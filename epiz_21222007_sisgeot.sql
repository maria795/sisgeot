-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql107.epizy.com
-- Generation Time: Apr 16, 2018 at 11:44 PM
-- Server version: 5.6.35-81.0
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `epiz_21222007_sisgeot`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch_office`
--

CREATE TABLE IF NOT EXISTS `branch_office` (
  `branch_office_id` int(5) NOT NULL AUTO_INCREMENT,
  `format_id` int(5) NOT NULL,
  `branch_office_code` varchar(7) NOT NULL,
  `branch_office_nro` varchar(4) NOT NULL,
  `branch_office_cost_center` varchar(60) NOT NULL,
  `branch_office_gerent_name` varchar(60) NOT NULL,
  `branch_office_gerent_phone` varchar(15) NOT NULL,
  `branch_office_gerent_email` varchar(60) NOT NULL,
  `branch_office_boss_name` varchar(60) NOT NULL,
  `branch_office_boss_phone` varchar(15) NOT NULL,
  `branch_office_boss_email` varchar(60) NOT NULL,
  PRIMARY KEY (`branch_office_id`),
  UNIQUE KEY `branch_office_code` (`branch_office_code`),
  KEY `format_id` (`format_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `branch_office`
--

INSERT INTO `branch_office` (`branch_office_id`, `format_id`, `branch_office_code`, `branch_office_nro`, `branch_office_cost_center`, `branch_office_gerent_name`, `branch_office_gerent_phone`, `branch_office_gerent_email`, `branch_office_boss_name`, `branch_office_boss_phone`, `branch_office_boss_email`) VALUES
(4, 3, 'LO09', '09', 'LOS LLANOS', 'ANGEL PEREZ', '04121212123', 'ANGEL@GMAIL.COM', 'PEPE PEREZ', '0412121212', 'PEPPE@GMAIL.COM'),
(6, 5, 'CONTROL', '354', 'PREVENCION DE PERDIDAS', 'SAMUEL GAETE', '2-60156211', 'ANTHONYMEDINACH@GMAIL.COM', 'JAQUELINE PARADA', '25656612', 'ANTHONYMEDINACH@GMAIL.COM');

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE IF NOT EXISTS `business` (
  `business_id` int(4) NOT NULL AUTO_INCREMENT,
  `business_name` varchar(100) NOT NULL,
  `business_rut` varchar(50) NOT NULL,
  `business_direction` varchar(100) NOT NULL,
  `business_phone` varchar(50) NOT NULL,
  `business_website` varchar(50) NOT NULL,
  `business_description` text NOT NULL,
  `business_logo` text NOT NULL,
  PRIMARY KEY (`business_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`business_id`, `business_name`, `business_rut`, `business_direction`, `business_phone`, `business_website`, `business_description`, `business_logo`) VALUES
(0, 'TECNOSEC', '76.020.371-8', 'TEMUCO,CHILE', '+569 924 90207', 'WWW.TECNOSEC.CL', 'COMERCIALIZADORA DE TECNOLOGIAS LTDA.', 'tecnosec.png'),
(1, 'INFRAGANTI', '', 'GENERAL DEL CANTO 105, OF. 1204, PROVIDENCIA', '+56 222611479', 'WWW.INFRAGANTI.CL', 'COMERCIALIZADORA DE TECNOLOGIAS LTDA.', 'infraganti.png'),
(2, 'GLOBAL BUSINESS', '0000', 'AV. EL ROSAL N 6331, MAIPU', '222 611 479', '222 611 479', 'COMERCIALIZADORA DE TECNOLOGIAS LTDA.', 'globalbusiness.png');

-- --------------------------------------------------------

--
-- Table structure for table `diagnostic`
--

CREATE TABLE IF NOT EXISTS `diagnostic` (
  `diagnostic_id` int(4) NOT NULL AUTO_INCREMENT,
  `diagnostic_name` varchar(120) NOT NULL,
  PRIMARY KEY (`diagnostic_id`),
  UNIQUE KEY `diagnosis_name` (`diagnostic_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `diagnostic`
--

INSERT INTO `diagnostic` (`diagnostic_id`, `diagnostic_name`) VALUES
(4, 'ACCESORIOS INCOMPLETOS'),
(8, 'CAMARA SIN VISUALIZAR'),
(6, 'LENTE SUELTO'),
(2, 'MANIPULACIÃ³N DE TERCEROS'),
(5, 'MUESTRA GOLPE'),
(3, 'MUESTRA HUMEDAD'),
(1, 'MUESTRA SUCIEDAD'),
(9, 'SISTEMA SIN VISUALIZAR'),
(7, 'SUPERFICIE RAYADA');

-- --------------------------------------------------------

--
-- Table structure for table `diagnostic_work`
--

CREATE TABLE IF NOT EXISTS `diagnostic_work` (
  `diagnostic_id` int(4) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  KEY `work_order_id` (`work_order_id`),
  KEY `diagnostic_id` (`diagnostic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diagnostic_work`
--

INSERT INTO `diagnostic_work` (`diagnostic_id`, `work_order_id`) VALUES
(1, 00001),
(2, 00002),
(2, 00003);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `document_destination` varchar(150) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `document_id` int(8) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`document_id`),
  KEY `document_ibfk_1` (`work_order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`document_destination`, `work_order_id`, `document_id`) VALUES
('nota de venta.pdf', 00001, 1);

-- --------------------------------------------------------

--
-- Table structure for table `formats`
--

CREATE TABLE IF NOT EXISTS `formats` (
  `format_id` int(5) NOT NULL AUTO_INCREMENT,
  `format_name` varchar(60) NOT NULL,
  `format_business_name` varchar(60) NOT NULL,
  `format_rut` varchar(15) NOT NULL,
  `format_direction` varchar(50) NOT NULL,
  `format_comuna` varchar(40) NOT NULL,
  `format_region` varchar(40) NOT NULL,
  `format_phone` varchar(15) NOT NULL,
  `format_gerent` varchar(60) NOT NULL,
  `format_gerent_email` varchar(50) NOT NULL,
  PRIMARY KEY (`format_id`),
  UNIQUE KEY `formats_rut` (`format_rut`),
  UNIQUE KEY `format_gerent_email` (`format_gerent_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `formats`
--

INSERT INTO `formats` (`format_id`, `format_name`, `format_business_name`, `format_rut`, `format_direction`, `format_comuna`, `format_region`, `format_phone`, `format_gerent`, `format_gerent_email`) VALUES
(0, 'ADMINISTRADOR', '0', 'admin', '0', '0', '0', '0', '0', '0'),
(3, 'UNIMAR', 'SUPERMERCADO', '123.1.1.1', 'CALLE NUEVA', '09', 'LOS ANDES', '+58323232', 'MARIA SILVA', 'MARIAJSILVAT@GMAIL.COM'),
(5, 'RENDIC HERMANOS S.A.', 'UNIMARC ', '9989116-5', 'CERRO EL PLOMO 5680', 'LAS CONDES', 'METROPOLITANA', '2-6014852', 'CHRISTIAN GODOY', 'ANTHONYMEDINACH@GMAIL.COM');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `photo_id` int(8) NOT NULL AUTO_INCREMENT,
  `photo_destination` varchar(150) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `work_order_id` (`work_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(4) NOT NULL AUTO_INCREMENT,
  `product_business` enum('TECNOSEC','INFRAGANTI','GLOBAL BUSINESS') NOT NULL COMMENT '0 = ''TECNOSEC'', 1=''INFRAGANTI'', 2=''GLOBAL BUSINESS''',
  `product_description` text NOT NULL,
  `product_cost` float NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_business`, `product_description`, `product_cost`) VALUES
(1, 'TECNOSEC', 'DISCO DURO, 320GB.', 100);

-- --------------------------------------------------------

--
-- Table structure for table `product_work`
--

CREATE TABLE IF NOT EXISTS `product_work` (
  `product_id` int(4) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  KEY `work_order_id` (`work_order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_work`
--

INSERT INTO `product_work` (`product_id`, `work_order_id`) VALUES
(1, 00001),
(1, 00002),
(1, 00003);

-- --------------------------------------------------------

--
-- Table structure for table `proyect`
--

CREATE TABLE IF NOT EXISTS `proyect` (
  `proyect_id` int(4) NOT NULL AUTO_INCREMENT,
  `proyect_code` varchar(7) DEFAULT NULL,
  `proyect_name` varchar(60) DEFAULT NULL,
  `proyect_description` text,
  `proyect_date` date DEFAULT NULL,
  `proyect_boss` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`proyect_id`),
  UNIQUE KEY `proyect_code` (`proyect_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `proyect`
--

INSERT INTO `proyect` (`proyect_id`, `proyect_code`, `proyect_name`, `proyect_description`, `proyect_date`, `proyect_boss`) VALUES
(1, 'PB19', 'PROYECTO BRECHA', 'PROYECTO BRECHA', '2918-12-19', 'JEAN CARLOS PEREZ'),
(2, 'PT10', 'PROYECTO TOTTEM', 'TIENE COMO OBJETIVO ORGANIZAR...', '2019-10-10', 'PEPE PEREZ');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE IF NOT EXISTS `receipt` (
  `receipt_id` int(8) NOT NULL AUTO_INCREMENT,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `receipt_detail_payment` enum('1','2','3','4','5') NOT NULL COMMENT '1=efectivo, 2=debito, 3=credito, 4=transferencia, 5=cheque',
  `receipt_amount` float NOT NULL,
  `receipt_issued` varchar(60) NOT NULL,
  `receipt_date` date NOT NULL,
  `receipt_observations` text NOT NULL,
  `business_id` int(4) NOT NULL,
  PRIMARY KEY (`receipt_id`),
  KEY `work_order_id` (`work_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `request_id` int(8) NOT NULL AUTO_INCREMENT,
  `branch_office_id` int(5) NOT NULL,
  `request_date` date NOT NULL,
  `request_department` varchar(50) NOT NULL,
  `request_medium` enum('LLAMADA TELEFONICA','EMAIL','SMS','WHATSAPP','PERSONAL') NOT NULL,
  `request_r_name` varchar(50) NOT NULL,
  `request_r_phone` varchar(15) NOT NULL,
  `request_r_email` varchar(60) NOT NULL,
  `request_status` set('0','1') NOT NULL,
  `request_app` text NOT NULL,
  `business_id` int(4) NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `branch_office_id` (`branch_office_id`),
  KEY `business_id` (`business_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `branch_office_id`, `request_date`, `request_department`, `request_medium`, `request_r_name`, `request_r_phone`, `request_r_email`, `request_status`, `request_app`, `business_id`) VALUES
(3, 6, '2018-01-13', 'MANTENCION', 'EMAIL', 'JUDTH ARANEDA', '4434562', 'ANTHONYMEDINACH@GMAIL.COM', '1', 'SOLICITA VISITAR Y SOLUCIONAR REQUERIMIENTO CCTV LOCAL IRRARRAZAVAL', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE IF NOT EXISTS `reset_password` (
  `reset_id` int(4) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reset_id`),
  UNIQUE KEY `id_user_2` (`id_user`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `services_id` int(4) NOT NULL AUTO_INCREMENT,
  `services_name` varchar(150) NOT NULL,
  PRIMARY KEY (`services_id`),
  UNIQUE KEY `services_name` (`services_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`services_id`, `services_name`) VALUES
(1, 'CAMBIO DE PASSWORD'),
(2, 'CAMBIOS DE EQUIPO GARANTIA/ NUEVO'),
(3, 'CANALIZADO DE CABLEADO'),
(9, 'CAPACITACIÃ³N PERSONAL'),
(7, 'CONFIGURACIÃ³N CCTV'),
(8, 'ENFOQUES DE CÃ¡MARAS'),
(10, 'INSTALACIÃ³N DE COMPUTADOR'),
(11, 'INSTALACIÃ³N DE DISCO DURO'),
(4, 'MANTENCIÃ³N DE HARDWARE'),
(5, 'MANTENCIÃ³N DE SOFWARE'),
(6, 'REUBICACIÃ³N DE CÃ¡MARAS'),
(12, 'TRASLADO DE CAMARA EN SALA ');

-- --------------------------------------------------------

--
-- Table structure for table `services_work`
--

CREATE TABLE IF NOT EXISTS `services_work` (
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `services_id` int(4) NOT NULL,
  KEY `work_order_id` (`work_order_id`),
  KEY `services_id` (`services_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_work`
--

INSERT INTO `services_work` (`work_order_id`, `services_id`) VALUES
(00001, 8),
(00001, 7),
(00002, 3),
(00003, 3);

-- --------------------------------------------------------

--
-- Table structure for table `technical`
--

CREATE TABLE IF NOT EXISTS `technical` (
  `technical_id` int(4) NOT NULL AUTO_INCREMENT,
  `technical_name` varchar(50) DEFAULT NULL,
  `technical_rut` varchar(12) DEFAULT NULL,
  `technical_position` varchar(60) DEFAULT NULL,
  `technical_phone` varchar(15) DEFAULT NULL,
  `technical_email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`technical_id`),
  UNIQUE KEY `technical_rut` (`technical_rut`,`technical_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `technical`
--

INSERT INTO `technical` (`technical_id`, `technical_name`, `technical_rut`, `technical_position`, `technical_phone`, `technical_email`) VALUES
(1, 'MIGUEL ANGEL', '1232323', 'GERENTE', '04121212121', 'GERENTE@GMAIL.COM');

-- --------------------------------------------------------

--
-- Table structure for table `technical_work`
--

CREATE TABLE IF NOT EXISTS `technical_work` (
  `technical_id` int(4) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  KEY `work_order_id` (`work_order_id`),
  KEY `technical_id` (`technical_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `technical_work`
--

INSERT INTO `technical_work` (`technical_id`, `work_order_id`) VALUES
(1, 00001),
(1, 00002),
(1, 00003);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(4) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(60) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_rut` varchar(20) NOT NULL,
  `user_type` enum('0','1','2') NOT NULL COMMENT '0=admin, 1=personal, 2=cliente',
  `user_business` enum('0','1','2') NOT NULL COMMENT '0=tecnosec, 1=infranganti, 2=global',
  `user_insert_date` datetime NOT NULL,
  `user_last_date` datetime NOT NULL,
  `format_id` int(5) NOT NULL,
  `user_active` enum('0','1') NOT NULL,
  `token` varchar(100) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `user_email` (`user_email`,`user_rut`),
  KEY `format_id` (`format_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `user_email`, `user_password`, `user_rut`, `user_type`, `user_business`, `user_insert_date`, `user_last_date`, `format_id`, `user_active`, `token`) VALUES
(0, 'ADMINISTRADOR', 'sif.cl17@gmail.com', 'f19a0a8fb7c81fded13129dca2651a89df498574', 'admin', '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1', ''),
(3, 'UNIMAR', 'MARIAJSILVAT@GMAIL.COM', '49fa572afd4b36847966f9002bf2e752d6f6dfb1', '123.1.1.1', '2', '', '2018-01-13 11:19:19', '0000-00-00 00:00:00', 3, '1', '46531c767078d0ea54e1132e08190902e53c3148'),
(4, 'A', 'MARIA_SILVA795@HOTMAIL.COM', '0a34e721a4d9556f203725c9f521a84500ba2001', 'A', '2', '', '2018-01-13 11:24:27', '0000-00-00 00:00:00', 4, '0', '4e4c3de56cd6b8bd9fa1ad99f5bb18bc405daac5'),
(5, 'RENDIC HERMANOS S.A.', 'ANTHONYMEDINACH@GMAIL.COM', '8fa6c0bfcd59ecf134e4c501a4acef4420a56b39', '9989116-5', '2', '', '2018-01-13 11:47:28', '0000-00-00 00:00:00', 5, '1', '08eea8897ec7bbe990ccb6bb64414f9093b55918');

-- --------------------------------------------------------

--
-- Table structure for table `verification_request`
--

CREATE TABLE IF NOT EXISTS `verification_request` (
  `verification_id` int(11) NOT NULL AUTO_INCREMENT,
  `v_request_id` int(11) NOT NULL,
  `v_request_status` enum('0','1') DEFAULT NULL,
  `v_request_date` date NOT NULL,
  `v_request_responsable` varchar(40) NOT NULL,
  `v_request_responsable_rut` int(11) NOT NULL,
  `v_token` varchar(150) NOT NULL,
  `v_format_id` int(8) NOT NULL,
  `business_id` int(4) NOT NULL,
  PRIMARY KEY (`verification_id`),
  UNIQUE KEY `v_request_id` (`v_request_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `verification_request`
--

INSERT INTO `verification_request` (`verification_id`, `v_request_id`, `v_request_status`, `v_request_date`, `v_request_responsable`, `v_request_responsable_rut`, `v_token`, `v_format_id`, `business_id`) VALUES
(2, 3, '1', '2018-01-13', 'RENDIC HERMANOS S.A.', 9989116, '05148b980e6adc9aaf3d9402fa1bd86814d5bf32', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `video_id` int(8) NOT NULL AUTO_INCREMENT,
  `video_destination` varchar(150) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY (`video_id`),
  KEY `work_order_id` (`work_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `work_order`
--

CREATE TABLE IF NOT EXISTS `work_order` (
  `work_order_id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `request_id` int(8) NOT NULL,
  `work_order_generation` date NOT NULL,
  `proyect_id` int(4) NOT NULL,
  `work_order_date` date NOT NULL,
  `time_input` varchar(15) NOT NULL,
  `time_output` varchar(15) NOT NULL,
  `work_order_observation` text NOT NULL,
  `work_order_status` enum('EN PROCESO','FINALIZADO') NOT NULL,
  PRIMARY KEY (`work_order_id`),
  KEY `request_id` (`request_id`),
  KEY `proyect_id` (`proyect_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `work_order`
--

INSERT INTO `work_order` (`work_order_id`, `request_id`, `work_order_generation`, `proyect_id`, `work_order_date`, `time_input`, `time_output`, `work_order_observation`, `work_order_status`) VALUES
(00001, 3, '2018-01-13', 2, '2018-01-15', '15:30', '16:30', '', 'FINALIZADO'),
(00002, 3, '2018-01-13', 0, '1969-12-31', '', '', '', 'EN PROCESO'),
(00003, 3, '2018-01-13', 2, '2018-02-01', '', '', '', 'EN PROCESO');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
