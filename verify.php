<?php
include("application/core/conexion.php");

$verification = new verification();
$request = new request();

$v_token= $_GET['token'];
$request_id = $_GET['x'];
$request_status = $_GET['y'];


$result0 = $request->ShowListAll($request_id);

$branch_office_cost_center = $result0[0]['branch_office_cost_center'];
$v_format_id = $result0[0]['format_id'];
$format_name = $result0[0]['format_name'];
$format_rut = $result0[0]['format_rut'];
$request_app = $result0[0]['request_app'];
$request_date = $result0[0]['request_date'];
$business_id = $result0[0]['business_id'];

$data = array(
		'v_request_id' => $request_id, 
		'v_request_status' => $request_status, 
		'v_request_date' => $request_date, 
		'v_request_responsable' => $format_name, 
		'v_request_responsable_rut' => $format_rut,
		'v_token' => $v_token,
		'v_format_id' => $v_format_id,
		'business_id' => $business_id
	);


if($verification->addVerification($data))
{
	$request->updateStatusRequest($request_id, $request_status);
	

	if ($request_status == '0') {include("verifyRequests.php");}
	elseif ($request_status == '1'){include("verifyRequest.php");}
	


	$result0 = $request->ShowListAll($request_id);

	$format_name = $result0[0]['format_name'];
	$request_status = $result0[0]['format_rut'];
	$request_date = $result0[0]['request_date'];
	$request_department = $result0[0]['request_department'];
	$branch_office_cost_center = $result0[0]['branch_office_cost_center'];
	$request_status = $result0[0]['request_status'];
	$request_app = $result0[0]['request_app'];
	$request_r_name = $result0[0]['request_r_name'];
	$request_r_phone = $result0[0]['request_r_phone'];
	$request_r_email = $result0[0]['request_r_email'];

					if ($request_status == '1') 
					{
					$request_status = 'VERIFICADO';

					$msj = "Nuestro equipo procedera a evaluar su requerimiento y de inmediato nos pondremos en contacto con usted, muchas gracias por preferirnos";
					}
					elseif($request_status == '0')
					{
						$request_status = 'RECHAZADA';
						$msj = "Le recomendamos que realice una nueva solicitud, y se ponga en contacto con su superior, muchas gracias por preferirnos";
					}

					$message = '<div id=":153" class="a3s aXjCH m16025006a7cf3d2d"><u></u>
							  <div>
							  
							    <table align="center" bgcolor="#eff2f1" border="0" cellpadding="0" cellspacing="0" class="m_5163378976771115696backgroundtable" width="100%" style="
							    background-color: #3e3c3c;">
							  
							    <tbody><tr>
							      <td style="padding-bottom:24px;padding-top:16px">
							      <center>
							        <div style="max-width:560px;display:block;clear:both;margin:0 auto;border-collapse:collapse">
							        
							        <table align="center" bgcolor="" border="0" cellpadding="0" cellspacing="0" width="100%">
							          <tbody><tr>
							          <td style="vertical-align:middle;text-align:center;padding-bottom:16px;padding-top:8px" height="28">
							           <a href="sif.cl" title="SIGAOT" target="_blank"><img src="'.$_SERVER['SERVER_NAME'].'/public/img/logo.png" style="margin:0 auto;display:block;height:100px" height="100"></a>
							          </td>
							          </tr>
							        </tbody></table>
							        
							        </div>
							        <div style="max-width:100%;display:block;clear:both;margin:0 auto">
							        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top-width:1px;border-bottom-width:1px;border-top-style:solid;border-bottom-style:solid;border-right-width:0;border-left-width:0;border-top-color:#e6e6e6;border-bottom-color:#e6e6e6">
							  
							          <tbody><tr>
							          <td style="padding-top:24px;padding-bottom:24px;padding-left:16px;padding-right:16px;text-align:left">
							            <div style="max-width:560px;display:block;clear:both;margin:0 auto">
							            
							            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px;line-height:1.5;border-collapse:collapse;text-align:left"><tbody><tr>
							    <td style="padding:16px" width="100%">
							      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px">
							        <tbody><tr>
							            
							          <td width="100%" style="line-height:1.3;padding-bottom:8px"> 
							          	<br>

							               <p style="font-size:24px;text-align:center;">SU REQUERIMIENTO FUE '.' '.$request_status.'</p>

							  				<strong><span style="text-transform:capitalize">Estimado cliente</span>,</strong>
							              <br>
							              <br>
							              <strong>Si usted no solicitó nuestros servicios, ignoré este correo electronico</strong>
							              <br><br>
							    
							              <strong>Se muestra a continuación la información relacionada con el requerimiento</strong><br><br>
							              <ul style="margin:0;padding:0;margin-left:24px">
							                  <li style="margin:0;padding:0"><strong>CLIENTE:</strong>'.' '.$format_name.'</li>
							                  <li style="margin:0;padding:0"><strong>CENTRO DE COSTO:</strong>'.' '.$branch_office_cost_center.'</li>
							                  <li style="margin:0;padding:0"><strong>FECHA:</strong>'.' '.$request_date.'</li>
							                  <li style="margin:0;padding:0"><strong>DEPARTAMENTO:</strong>'.' '.$request_department.'</li>
							                
							                  <li style="margin:0;padding:0"><strong>RESPONSABLE DE LA SOLICITUD:</strong>'.' '.$request_r_name.'</li>
							                  <li style="margin:0;padding:0"><strong>FONO:</strong>'.' '.$request_r_phone.'</li>
							              
							                  <li style="margin:0;padding:0"><strong>ESTADO:</strong>'.' '.$request_status.'</li>
							                  <li style="margin:0;padding:0"><strong>REPORTE:</strong>'.' '.$request_app.'</li>
							    
							                </ul>
							              <br>
							              <i>'.' '.$msj.'.</i>  
							              <br><br>
							              <div style="display:flex;justify-content: space-around">
							             <p></p>
							            </div>
							          </td>
							          </td>
							        </tr>
							      </tbody></table>
							    </td>
							  </tr></tbody></table>
							  
							  </div>
							  </td>
							  </tr>
							  </tbody></table>
							  </div>
							  
							  </center>
							  </td>
							  </tr>
							  </tbody></table>
							  
							  </div>';

				$subject = "RESPUESTA A SU SOLICITUD DE REQUERIMIENTO";
				$body = $message;
				$to_andress = $request_r_email;	

				if (sent_email($subject, $body, $to_andress))
				{
				   	$json = '{"success":"1","info":"send"}';
				}
				else
				{
  					$json = '{"success":"0","info":"errorSending"}';
				}

}
else
{
	$result1 = $verification->listVerificationRequest($request_id);
	$result_status = $result1[0]['request_status'];
	if ($result_status == 0) {include("verifyRequests.php");}
	elseif ($result_status == 1){include("verifyRequest.php");}


	//header("location: index.php");
}

?>