<?php
	require"application/core/conexion.php";
	if (empty($user)){
		header('location: index.php');
	}
	else if($user->getUserType()==2)
	{
		header('location: customer.php');
	}
?>
<!doctype html>
<html lang="es" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="public/css/reset.css"> 
		<link href="public/css/datatables.css" rel="stylesheet">
		
		
		<link href="public/css/main-0.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 0px) and (max-width: 479px)" >
		<link href="public/css/main-480.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 480px) and (max-width: 767px)" >
		<link href="public/css/main-768.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 768px) and (max-width: 1023px)" >
		<link href="public/css/main-1024.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 1024px)" >
		<link href="public/css/print.css" rel="stylesheet" type="text/css" media="print">
		<link href="public/css/font-awesome.min.css" rel="stylesheet">
		
		<!--<link href="public/css/datatables.css" rel="stylesheet">-->
		<link rel="apple-touch-icon" sizes="57x57" href="public/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="public/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="public/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="public/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="public/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="public/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="public/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="public/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="public/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="public/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="public/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="public/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="public/icons/favicon-16x16.png">
		<link rel="manifest" href="public/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="public/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
	
		<title>SISGEOT - Aplicación</title>
	</head>
	<body id="body">
		<div class="loader"></div>
		<div id="loading-div"></div>
		<div id="loading-div2"></div>
      
      		<div id="alertDiv"><div class="alertContainer"><div class="alertText"><p class="alertText1">¿Está seguro?</p>
		<p class="alertText2">El elemento no podra ser recuperado y sus datos asociados tambien se suprimirán</p></div>
		<div class="alertButtons"><button id="cancelDeleteRequest" class="cancelAlert">NO</button><button id="confirmDeleteRequest" class="confirmAlert">SI</button></div></div></div>
		<div id="statusDiv"><div class="alertContainer"><div class="alertText"><p class="alertText1">¿Está seguro?</p>
		<p class="alertText2">El estado del elemento sera cambiado</p></div>
		<div class="alertButtons"><button  class="cancelAlert">NO</button><button  class="confirmAlert">SI</button></div></div></div>
		
		<div id="userPanel">
			<div id="userPanelInput">
				<img src="public/img/user.png" id="userImg">
				<input type="text" id="username" class="userinfo" disabled value="<?php echo $_SESSION['name']?> ">
				<input type="text" id="position" class="userinfo" disabled value="<?php echo $_SESSION['email']?>">
			</div>
		</div>
		<header class="page-header">
			<a  class="page-header-trigger"><span></span></a>
			<!--<div id="logoo">
			<img src="img/capa1.png" id="capa1" class="logoCapas">												
			<img src="img/capa1.png" id="capa1" class="logoCapas">
			<img src="img/capa4.png" id="capa4" class="logoCapas">
			<img src="img/capa3.png" id="capa3" class="logoCapas">
			<img src="img/capa2.png" id="capa2" class="logoCapas"></div>-->
			<div class="logo"><img src="public/img/logo.png" alt="Logo" id="logoImg"></div>
			<div class="logo2" "><img src="public/img/logo-figure.png" alt="Logo" id="logo-figure"></div>
			<nav class="top-nav">
				<ul class="top-nav-list">
					<li  class="page-search icon">
						<!--<span class="fa fa-search fa-5x"></span>	-->
					
							<input type="search" placeholder="Buscar" id="search-input">
						
					</li>
					<li class="submenu account">
						<a class="userMenu">
							<img class="userMenu" src="public/img/user.png" id="avatar" alt="avatar">
							<p2 id="username2" class="userinfo2 userMenu" ><?php echo $_SESSION['name']?> </p2><!--24 caracteres-->
							<p2 id="position2" class="userinfo2 userMenu" ><?php echo $_SESSION['email']?></p2>
						</a>
						<ul >
							<li class="Password options icon options2">
								<span class="fa fa-pencil fa-5x topM"></span><a class="load-content" href="password.html">Contraseña</a>
							</li>
							<li class="Account icon options options2 onlyAdmin">
								<span class="fa fa-pencil fa-5x topM"></span><a class="load-content" href="account.html">Mi Cuenta</a>
							</li>
							<li class="icon options2 logout">
								<span class="fa fa-power-off fa-5x topM"></span><a href="application.php?logout=1">Cerrar  sesión</a>
							</li>
						</ul>
					</li>
				</ul>
			</nav>
		</header>
		<main class="main-content">
			
			<nav class="left-nav">
				<div id="separator">
				</div>
				<ul>
					<li class="Home icon options active">
						<span class="fa fa-home fa-5x"></span><a class="load-content"  href="" >¿Que hay de nuevo?</a>
					</li>
					<li class="label ">GESTIÓN DE OT's</li>
					<!--<li class="icon options gestionOT ">
						<span class="fa fa-file-text fa-5x"></span><a class="load-content"  href="request.html">Requerimientos</a>
					</li>-->

					<li class="Format icon options">
						<span class="fa fa-user fa-5x"></span>  <a class="load-content" href="format.html">Clientes</a>
					</li>

					<li class="BranchOffice icon options">
						<span class="fa fa-building fa-5x"></span>  <a class="load-content"  href="branchoffice.html">Sucursales</a>
					</li>

				

					<li class="Request icon options">
						<span class="fa fa-file-text fa-5x load-content" href="request.html" ></span><a class="load-content" href="request.html">Requerimientos</a>
					</li>
					<li class="WorkOrder icon options" >
						<span class="fa fa-list-alt fa-5x"></span><a class="load-content" href="WorkOrder.html">Órdenes de Trabajo</a>
					</li>
					<li class="Receipt icon options" >
						<span class="fa fa-money fa-5x"></span><a class="load-content" href="receipt.html">Emitir recibo</a>
					</li>	
					<li class="label ">DATOS ASOCIADOS</li>
					
					<li class="Proyect icon options">
						<span class="fa fa-tags fa-5x"></span><a class="load-content" href="proyect.html">Proyectos</a>
					</li>
					<li class="Service icon options">
						<span class="fa fa-tags fa-5x"></span><a class="load-content"  href="service.html">Servicios</a>
					</li>
					<li class="Technical icon options">
						<span class="fa fa-tags fa-5x"></span><a class="load-content" href="technical.html">Tecnicos</a>
					</li>




					<li class="Product icon options ">
						<span class="fa fa-tags fa-5x"></span><a class="load-content" href="product.html">Productos</a>
					</li>



					<li class="Diagnostic icon options">
						<span class="fa fa-tags fa-5x"></span><a class="load-content" href="diagnostic.html">Diagnostico</a>
					</li>

					<li class="Business icon options onlyAdmin">
						<span class="fa fa-tags fa-5x"></span><a class="load-content" href="Business.html">Empresas</a>
					</li>
				</ul>
				
				<ul class="onlyAdmin">
					<li class="label">AJUSTES DEL SISTEMA</li>
					<li class="User icon options">
						<span class="fa fa-group fa-5x"></span><a class="load-content" href="user.html">Usuarios</a>
					</li>
					<!--<li class="options overview">
						<a href="#0">OPCIÓN 2</a>
					</li>
					<li class="options overview">
						<a href="#0">OPCIÓN 3</a>
					</li>-->
					</ul>
				<ul class="acc">	
					<li class="label acc">MI CUENTA</li>
					<li class="Account icon options acc">
						<span class="fa fa-pencil fa-5x"></span><a class="load-content" href="account.html">Mi Cuenta</a>
					</li>
					<li class="logout icon options acc">
					<span class="fa fa-power-off fa-5x topM"></span><a href="application.php?logout=1">Cerrar  sesión</a>
					</li>
				</ul>
			</nav>

			<div class="content" id="content">
			


			</div> <!-- .content-wrapper -->
			<div class="imgFullBack"></div>
			<div class="imgFull"><div class="closeFull">x</div></div>
			<div class="vidFullBack"></div>
			<div class="vidFull">
				<video class="playVid" width="90%" height="90%" controls>
    <source src="" type="video/mp4">
    <source src="" type="video/ogg">
    <source src="" type="video/webm">
    <object data="video.mp4" width="470" height="255">
    <embed src="video.swf" width="470" height="255">
    </object>
</video>

				<div class="closeFull">x</div></div>
			<div class="message-box"><p class="message"></p></div>
			<div class="scriptBox"></div>
		</main>
		
		<script src="application/library/jquery.min.js"></script>
		
		<script src="application/library/modernizr.js"></script> <!-- Modernizr -->
		<script src="application/script/frontendController.js"></script>
		<script src="application/script/dataController.js"></script>
		<script src="application/script/fieldController.js"></script>
		<script src="application/script/receipt.js"></script>

		<script src="application/library/jquery.dataTables.min.js"></script>
		<script src="application/library/dataTables.fixedHeader.min.js"></script>
		<script src="application/library/dataTables.responsive.min.js"></script>
		
		<script>changeSection('Home')</script>
		

	</body>
</html>	
