<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();


$product_id = (int) $_POST['product_id'];
$product = new product();

if($user->getUserType() == 0 OR $user->getUserType() == 1)
{
	if(empty($product_id))
	{
		$product_business = $_POST['product_business'];
		$product_description = strtoupper($_POST['product_description']);
		$product_cost = strtoupper($_POST['product_cost']);

		if (empty($product_description) and empty($product_description) and empty($product_cost)) 
		{
				echo "<script>showMsg('Error','Existen campos vacios');</script>";
		}
		else
		{
			if($product->addProduct($product_business, $product_description, $product_cost))
			{
				echo "<script>showMsg('Exito','Producto registrado');$('form')[0].reset();</script>";
			}
			else
			{
				echo "<script>showMsg('Error','Producto no registrado');</script>";	
			}
		}
	}
	else
	{
		$product_business = $_POST['product_business'];
		$product_description = strtoupper($_POST['product_description']);
		$product_cost = strtoupper($_POST['product_cost']);

		if (empty($product_description) and empty($product_business) and empty($product_cost)) 
		{
				echo "<script>showMsg('Error','Existen campos vacios');</script>";
		}
		else
		{
			if($product->updateProduct($product_id, $product_business, $product_description, $product_cost))
			{
				echo "<script>showMsg('Exito','Producto actualizado');</script>";
			}
			else
			{
				echo "<script>showMsg('Error','Producto no actualizado');</script>";	
			}
		}
	}
}
else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 

?>