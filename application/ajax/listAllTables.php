<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$request = new orderWork();

if($user->getUserType() == 0 OR $user->getUserType() == 1) 
	{
		if ($user->getUserBusiness() == '')
		{
			echo json_encode($request->listRequestTable());
		}
		else if($user->getUserBusiness() == '0' OR $user->getUserBusiness() == '1' OR $user->getUserBusiness() == '2' )
		{	
			echo json_encode($request->listRequestTableBusiness($user->getUserBusiness()));
		}
		else
		{
			echo "User no encontrado";
		}
	}
	else if ($user->getUserType()==2) 
	{
		 $format_id = $user->IsFormat();
		 echo json_encode($request->showListAllRequest($format_id));

	}
	else
	{
		echo "Error fatal";
	}
?>