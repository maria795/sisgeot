<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$proyect_id = (int) $_POST['proyect_id'];
$proyect = new proyect();
echo json_encode($proyect->showProyectListByCode($proyect_id));
?>