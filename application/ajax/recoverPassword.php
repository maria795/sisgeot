<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$change = new recoverPassword();
$password1 =  sha1($_POST['password-1']);
$password2 =  sha1($_POST['password-2']);
$id    = $_POST['id_user'];
$token = $_POST['token'];

if($result = $change->showLink($token))
	{
		$id_user = $result['id_user'];
		if(sha1($id_user) == $id)
		{
			if($password1 == $password2)
			{
						if($change->updateUsers($id_user, $password1)) 
						{		
							echo  "<script>showMsg('Exito', 'Contraseña actualizada');</script>";


									if($change->deleteToken($token))
									{
										return true;
									    header('Location:index.php');

									}
									else
									{
										return false;
									}
						}
						else
						{
							echo  "<script>showMsg('Error', 'Contraseña no actualizada');</script>";
						}
			
			}
			else
			{
				echo  "<script>showMsg('Error', 'Contraseñas no coinciden');</script>";

			}
		}
		else
		{
			echo  "<script>showMsg('Error', 'Correo electronico no existe');</script>";
		}
	  
	  }
else
	{
		echo  "<script>showMsg('Error', 'Token no valido');</script>";
	}

?>