<?php
include("../core/conexion.php");

if ($user->isAccess() === false) die();

$id_user = $_POST['id_user'];
$user_p = new user();
if($user->getUserType() == 0)
{

if (empty($id_user)) 
{
	$user_name = strtoupper($_POST['user_name']);
	$user_email = strtoupper($_POST['user_email']);
	$user_business = strtoupper($_POST['user_business']);
	$user_rut = $_POST['user_rut'];
	$user_type = (int) 1;
	$format_id = (int) 0;
	$user_active = (int) 0;

	if(empty($user_name) and empty($user_email) and empty($user_rut) and empty($user_business))
	{
		echo "<script>showMsg('Error','Existen campos vacios');</script>";
	}
	else
	{
		if ($user_p->addUser($user_name, $user_email, $user_rut, $user_type, $user_business, $format_id, $user_active))
		{

			echo "<script>showMsg('Exito','Usuario registrado');$('form')[0].reset();</script>";
		}
		else
		{
			echo "<script>showMsg('Error','Usuario no registrado');</script>";	
		}
	}
}

else
{
	$user_name = strtoupper($_POST['user_name']);
	$user_email = strtoupper($_POST['user_email']);
	$user_business = strtoupper($_POST['user_business']);
	$user_rut = $_POST['user_rut'];
	$user_type = 2;
	$format_id = (int) 0;
//---------------------
	$result = $user->showUserListByCode($id_user);
	$lastEmail = $result[0]['user_email'];
	$firstPassword = $user->generatePassword();

	$cadena = $user_rut.$user_name.rand(1,99999).date('Y-m-d');
	$token  = sha1($cadena);			
	$subject = "Usted ha sido registrado en SISGEOT";	   
	$links = $_SERVER['SERVER_NAME'].'/emailVerify.php?id='.$id_user."&token=".$token;

	if(("$lastEmail" != "$user_email"))  
	{

			$body ='<div id=":153" class="a3s aXjCH m16025006a7cf3d2d"><u></u>
							  	<div>
							    <table align="center" bgcolor="#eff2f1" border="0" cellpadding="0" cellspacing="0" width="100%" style="
							    background-color: #3e3c3c;">
							  
							    <tbody><tr>
							      <td style="padding-bottom:24px;padding-top:16px">
							      <center>
							        <div style="max-width:560px;display:block;clear:both;margin:0 auto;border-collapse:collapse">
							        
							        <table align="center" bgcolor="" border="0" cellpadding="0" cellspacing="0" width="100%">
							          <tbody><tr>
							          <td style="vertical-align:middle;text-align:center;padding-bottom:16px;padding-top:8px" height="28">
							            <a href="sif.cl" title="SIGAOT" target="_blank"><img class="m_5163378976771115696logo CToWUd" style="margin:0 auto;display:block;height:100px" alt="" src="'.$_SERVER['SERVER_NAME'].'/public/img/logo.png" height="100"></a>
							          </td>
							          </tr>
							        </tbody></table>
							        
							        </div>
							        <div style="max-width:100%;display:block;clear:both;margin:0 auto">
							        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top-width:1px;border-bottom-width:1px;border-top-style:solid;border-bottom-style:solid;border-right-width:0;border-left-width:0;border-top-color:#e6e6e6;border-bottom-color:#e6e6e6">
							  
							          <tbody><tr>
							          <td style="padding-top:24px;padding-bottom:24px;padding-left:16px;padding-right:16px;text-align:left">
							            <div style="max-width:560px;display:block;clear:both;margin:0 auto">
							            
							            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px;line-height:1.5;border-collapse:collapse;text-align:left"><tbody><tr>
							    <td style="padding:16px" width="100%">
							      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px">
							        <tbody><tr>
							          <td width="100%" style="line-height:1.3;padding-bottom:8px"> 
							              <p style="font-size:24px;text-align:center;">APERTURA DE CUENTA DE USUARIO</p>
							              <br><br>
							  <strong>Hola <span style="text-transform:capitalize">'.$user_name.'</span>,</strong>
							              <br> Ha sido registrado en nuestra base de datos una cuenta con su información.
							              <br>
							              <br>
							              <strong>Si usted no solicitó obtener una cuenta en nuestro sistema, ignoré este correo electronico</strong>
							              <br>
							              <br>Para comenzar a disfrutar de nuestros servicios,  <strong> valide sus datos: </strong> 
							              <br><br>
							              <strong>Datos de su cuenta de usuario:</strong><br><br>
							              <ul style="margin:0;padding:0;margin-left:24px">
							                  <li style="margin:0;padding:0"><strong>RUT:</strong>'.' '.$user_rut.'</li>
							                  <li style="margin:0;padding:0"><strong>CONTRASEÑA:</strong>'.' '.$firstPassword.'</li>              
							                </ul>
							              <br>
							              <i>Usted sera redirecconado al presionar click en el siguiente boton</i>
							              <br><br>
							              <a href="'.$links.'" style="background: #4284ad;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">VERIFICAR CUENTA</a>
							         
							              <br> <span style="font-style:italic">Sistema de Gestión y Administración de Órdenes de Trabajo <br> (Tecnosec, Infraganti, Global Business)</span>
							          </td>
							          </td>
							        </tr>
							      </tbody></table>
							    </td>
							  </tr></tbody></table>
							  </div>
							  </td>
							  </tr>
							  </tbody></table>
							  </div>
							  </center>
							  </td>
							  </tr>';
							  $to_andress = $user_email;	
							  echo $links;	
							 //sent_email ($subject, $body, $to_andress);
	

	}	

//---------------------
	if(empty($user_name) and empty($user_email) and empty($user_rut) and empty($user_business))
	{
		echo "<script>showMsg('Error','Existen campos vacios');</script>";
	}
	else
	{
		$firstPassword = $user->encrypt($firstPassword);
		if ($user_p->updateUser($id_user, $user_name, $user_email, $user_rut, $user_type, $user_business, $format_id, $firstPassword, $token ))
		{

			echo "<script>showMsg('Exito','Usuario actualizado');</script>";
		}
		else
		{
			echo "<script>showMsg('Error','Usuario no actualizado');</script>";	
		}
	}
}
}
else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 