<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$product_id = (int) $_POST['product_id'];
$product = new product();
echo json_encode($product->showProductListByCode($product_id));
