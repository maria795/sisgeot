<?php
ini_set("display_errors", 1);

include("../core/conexion.php");

@$work_order_id = $_POST['work_order_id'];
@$workOrder = new workOrder();
@$diagnosticWork = new diagnosticWork();
@$technicalWork = new technicalWork();
@$productWork = new productWork();
@$serviceWork = new servicesWork();
@$photoWork=new photo();
@$videoWork=new video();
@$documentWork=new document();

@$request_id = $_POST['request_id']; 
@$proyect_id = strtoupper($_POST['proyect_id']); 
@$work_order_generation= $_POST['work_order_generation'];  
@$work_order_date= $_POST['work_order_date'];  
@$time_input = $_POST['time_input']; 
@$time_output = $_POST['time_output']; 
@$work_order_observation = strtoupper($_POST['work_order_observation']); 

@$fecha1=$work_order_generation;

@$work_order_generation=date("Y-m-d",strtotime($work_order_generation));
@$work_order_date=date("Y-m-d",strtotime($work_order_date));


@$video = $_FILES['videos']['name'];
@$photo = $_FILES['images']['name'];
@$document = $_FILES['documentos']['name'];

@$tmpFilePath = $_FILES['images']['tmp_name'];
@$tmpFilePaths = $_FILES['videos']['tmp_name'];
@$tmpFilePathss = $_FILES['documentos']['tmp_name'];


@$diagnostic = $_POST['diagnostic'];
@$service = $_POST['service'];
@$technical = $_POST['technical'];
@$product = $_POST['product'];

//print_r($_POST['nonDeleteImg']);

//echo str_pad("$work_order_id", 5, "0", STR_PAD_LEFT);

//delete order_work photo :D

if(empty($work_order_id))
 {
    if (empty($time_output)) 
    {
        $work_order_status = 'EN PROCESO';
    }
    else
    {
        $work_order_status = 'FINALIZADO';
    }

    if (empty($diagnostic) OR empty($service) OR empty($product) OR empty($technical)) {

        echo "<script>showMsg('Error','Existen campos vacios');</script>";
    }
    else
    {
    if($workOrder->addWorkOrder($request_id, $work_order_generation, $proyect_id, $work_order_date, $time_input, $time_output, $work_order_observation, $work_order_status))
    {
        $result = $workOrder->lastId();
        $id_work = $result[0]['work_order_id'];

        foreach ($technical as $key => $value) {
            $technicalWork->addTechnicalWork($id_work, $value);
        }

        foreach ($diagnostic as $key => $value) {
            $diagnosticWork->addDiagnosticWork($id_work, $value);
        }

        foreach ($service as $key => $value) {
            $serviceWork->addserviceWork($id_work, $value);
        }

        foreach ($product as $key => $value) {
            $productWork->addProductWork($id_work, $value);
        }

    for($i=0; $i<count($document); $i++){
        $tmpFile = $tmpFilePathss[$i];
          if($tmpFile!=""){
                $shortname = $document[$i];
                $filePath = "../../public/ot/doc/".$document[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $document_destination = $shortname;
                        $numero = $document_destination;
                        $count = count($numero);
                        $documentWork->addDocument($id_work, $numero);
                    
                }
            }
         }
    for($i=0; $i<count($video); $i++){
        $tmpFile = $tmpFilePaths[$i];
          if($tmpFile!=""){
                $shortname = $video[$i];
                $filePath = "../../public/ot/vid/".$video[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $video_destination = $shortname;
                        $numero = $video_destination;
                        $count = count($numero);
                        $videoWork->addVideo($id_work, $numero); 
                }
            }
         }
                    
    for($i=0; $i < count($photo); $i++){
        $tmpFile = $tmpFilePath[$i];
          if($tmpFile!=""){
                $shortname = $photo[$i];
                $filePath = "../../public/ot/img/".$photo[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $photo_destination = $shortname;
                        $numero = $photo_destination;
                        $count = count($numero);
                        $photoWork->addPhoto($id_work, $numero);
                        
                }
            }
         }

echo "<script> $('#serviceListAdded').html(''); $('#technicalListAdded').html(''); $('#diagnosticListAdded').html(''); $('#productListAdded').html(''); $('form')[0].reset();</script>";

echo "<script>showMsg('Exito','Orden de trabajo, registrado');</script>";

}
}
}
else
{
     if (empty($time_output)) 
    {
        $work_order_status = 'EN PROCESO';
    }
    else
    {
        $work_order_status = 'FINALIZADO';
    }

    if($workOrder->updateWorkOrder($work_order_id, $work_order_generation, $proyect_id, $work_order_date, $time_input, $time_output, $work_order_observation, $work_order_status))
    {
        if (!empty($technical)) {
            $technicalWork->deleteTechnicalWork($work_order_id);
            foreach ($technical as $key => $value) {
                $technicalWork->addTechnicalWork($work_order_id, $value);
            }
        }
        else
        {}
        if (!empty($diagnostic)) {
        $diagnosticWork->deleteDiagnosticWork($work_order_id);
        foreach ($diagnostic as $key => $value) {
            $diagnosticWork->addDiagnosticWork($work_order_id, $value);
        }}

        if (!empty($service)) {
        $serviceWork->deleteServicesWork($work_order_id);
        foreach ($service as $key => $value) {
            $serviceWork->addserviceWork($work_order_id, $value);
        }}
        
        if (!empty($product)) {
        $productWork->deleteProductWork($work_order_id);
        foreach ($product as $key => $value) {
            $productWork->addProductWork($work_order_id, $value);
        }}


if (!empty($_POST['DeleteImg'])){
    foreach ($_POST['DeleteImg'] as $key => $value) {
        $photoWork->deletePhoto($value);
        //unlink("../../public/ot/img/".$value);

    }
}

if (!empty($_POST['DeleteVideo'])){
    foreach ($_POST['DeleteVideo'] as $key => $value) {
        $videoWork->deleteVideo($value);
        
        //unlink("../../public/ot/vid/".$value);

    }
}
if (!empty($_POST['DeleteDocument'])){
    foreach ($_POST['DeleteDocument'] as $key => $value) {
        $document = $documentWork->deleteDocument($value);
        
        //unlink("../../public/ot/doc/".$value);

    }
}


for($i=0; $i<count($document); $i++){
        $tmpFile = $tmpFilePathss[$i];
          if($tmpFile!=""){
                $shortname = $document[$i];
                $filePath = "../../public/ot/doc/".$document[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $document_destination = $shortname;
                        $numero = $document_destination;
                        $count = count($numero);
                        $documentWork->addDocument($work_order_id, $numero);
                    
                }
            }
         }

for($i=0; $i<count($video); $i++){
        $tmpFile = $tmpFilePaths[$i];
          if($tmpFile!=""){
                $shortname = $video[$i];
                $filePath = "../../public/ot/vid/".$video[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $video_destination = $shortname;
                        $numero = $video_destination;
                        $count = count($numero);
                        $videoWork->addVideo($work_order_id, $numero); 
                }
            }
         }





    for ($i=0; $i < count($photo); $i++){
        $tmpFile = $tmpFilePath[$i];
          if($tmpFile!=""){
                $shortname = $photo[$i];
                $filePath = "../../public/ot/img/".$photo[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $photo_destination = $shortname;
                        $numero = $photo_destination;
                        $count = count($numero);
                        $photoWork->addPhoto($work_order_id, $numero);
         
                        
                }
            }
         }

echo "<script>showMsg('Exito','Orden de trabajo, actualizado'); $('#serviceListAdded').html(''); $('#technicalListAdded').html(''); $('#diagnosticListAdded').html(''); $('#productListAdded').html(''); $('form')[0].reset();</script>";


}
}
?> 
