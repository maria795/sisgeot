<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$format_id = (int) $_POST["format_id"];
$format = new format();
echo json_encode($format->showFormatListByCode($format_id));

?>