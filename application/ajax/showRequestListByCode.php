<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$request_id = (int) $_POST['request_id'];
$request = new request;
echo json_encode($request->showRequestListByCode($request_id));
?>
