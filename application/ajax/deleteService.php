 <?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$services_id =(int) $_POST['id'];
$services = new services;
if (empty($services_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{ 
		if($user->getUserType() == 0) 
	 	{
			if($services->deleteServices($services_id))
			{
				echo "<script>showMsg('Exito','Servicio eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Servicio no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}

?>