<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$business_id = $user->getUserBusiness();
$business = new business();
echo json_encode($business->showBusinessListByCode($business_id));

?>