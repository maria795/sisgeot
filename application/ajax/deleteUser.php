<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$id_user = (int) $_POST['id'];
$user_p = new user();
if (empty($id_user))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{
	if($user->getUserType() == 0) 
	 { 
			if($user_p->deleteUser($id_user))
			{
				echo "<script>showMsg('Exito','Requerimiento eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Requerimiento no eliminado');</script>";	
			}
		}
	else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
	else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}
?>