<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$branch_office_id = (int) $_POST['branch_office_id'];
$branch_office = new branchOffice;
$format_id = $_POST['format_id'];

if($user->getUserType() == 0 OR $user->getUserType() == 1)
{
		if(empty($branch_office_id))
		{
			if($format_id == 0 OR empty($format_id)) {
				echo "<script>showMsg('Error','Seleccione cliente');</script>";			
			}
			else
			{
				$format_id = $_POST['format_id'];
				$branch_office_code = strtoupper($_POST['branch_office_code']); 
				$branch_office_nro = strtoupper($_POST['branch_office_nro']) ;
				$branch_office_cost_center = strtoupper($_POST['branch_office_cost_center']); 
				$branch_office_gerent_name = strtoupper($_POST['branch_office_gerent_name']); 
				$branch_office_gerent_phone = strtoupper($_POST['branch_office_gerent_phone']);
				$branch_office_gerent_email = strtoupper($_POST['branch_office_gerent_email']) ;
				$branch_office_boss_name = strtoupper($_POST['branch_office_boss_name']); 
				$branch_office_boss_phone = strtoupper($_POST['branch_office_boss_phone']); 
				$branch_office_boss_email = strtoupper($_POST['branch_office_boss_email']);
				if (empty($format_id) and format_id != 0 and empty($branch_office_code)) 
				{
					echo "<script>showMsg('Error','Existen campos vacios');</script>";	
				}
				else
				{
					if ($branch_office->addBranchOffice($format_id, $branch_office_code, $branch_office_nro, $branch_office_cost_center, $branch_office_gerent_name, $branch_office_gerent_phone, $branch_office_gerent_email, $branch_office_boss_name, $branch_office_boss_phone, $branch_office_boss_email))
					{
							echo "<script>showMsg('Exito','Sucursal registrado');$('form')[0].reset();</script>";
					}
					else
					{
							echo "<script>showMsg('Error','Sucursal no registrado');</script>";	
					}
				}
			}
		}
		else
		{
			    $branch_office_id = $_POST['branch_office_id']; 
				$branch_office_code = strtoupper($_POST['branch_office_code']); 
				$branch_office_nro = strtoupper($_POST['branch_office_nro']) ;
				$branch_office_cost_center = strtoupper($_POST['branch_office_cost_center']); 
				$branch_office_gerent_name = strtoupper($_POST['branch_office_gerent_name']); 
				$branch_office_gerent_phone = strtoupper($_POST['branch_office_gerent_phone']);
				$branch_office_gerent_email = strtoupper($_POST['branch_office_gerent_email']) ;
				$branch_office_boss_name = strtoupper($_POST['branch_office_boss_name']); 
				$branch_office_boss_phone = strtoupper($_POST['branch_office_boss_phone']); 
				$branch_office_boss_email = strtoupper($_POST['branch_office_boss_email']);
				if(empty($branch_office_id)) 
				{
					echo "<script>showMsg('Error','Existen campos vacios');</script>";	
				}
				else
				{
					if ($branch_office->updateBranchOffice($branch_office_id, $branch_office_code, $branch_office_nro, $branch_office_cost_center, $branch_office_gerent_name, $branch_office_gerent_phone, $branch_office_gerent_email, $branch_office_boss_name, $branch_office_boss_phone, $branch_office_boss_email))
					{
							echo "<script>showMsg('Exito','Sucursal actualizado');</script>";
					}
					else
					{
							echo "<script>showMsg('Error','Sucursal no actualizado');</script>";	
					}
				}
		}
}
else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 
?>