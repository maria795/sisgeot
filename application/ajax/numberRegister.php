<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$request = new request();
$branchOffice = new branchOffice();
$orderWork = new orderWork();

$format_id = $user->IsFormat();
$request_statusT = '1';
$request_statusF = '0';
$work_order_statusT = 'EN PROCESO';
$work_order_statusF = 'FINALIZADO';

$count = $branchOffice->showBranchOfficeListByFormat($format_id);
$totalBranchOffice = (empty($count[0]))? 0: count($count);


$count = $request->ListAllFormatByRequest($format_id);
$numberRequest = (empty($count[0]))? 0: count($count);


$count = $request->ShowListAllFormat($format_id, $request_statusT);
$numberRequestTrue = (empty($count[0]))? 0: count($count);

$count = $request->ShowListAllFormat($format_id, $request_statusF);
$numberRequestFalse = (empty($count[0]))? 0: count($count);

$count = $orderWork->queryFormat($format_id);
$numberOT = (empty($count[0]))? 0: count($count);

$count = $orderWork->queryFormatStatus($format_id, $work_order_statusT);
$numberOTenproceso = (empty($count[0]))? 0: count($count);


$count = $orderWork->queryFormatStatus($format_id, $work_order_statusF);
$numberOTfinalizado = (empty($count[0]))? 0: count($count);


$data = array( 
	'totalBranchOffice' => $totalBranchOffice,
	'numberRequest'=> $numberRequest,
	'numberRequestTrue'=> $numberRequestTrue,
	'numberRequestFalse'=> $numberRequestFalse,
	'numberOT'=> $numberOT,
	'numberOTenproceso' => $numberOTenproceso,
	'numberOTfinalizado' => $numberOTfinalizado
	);

$data = json_encode($data);
echo $data;
//numero de requerimiento verificados no verificados 
?>