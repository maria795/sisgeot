<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$business = new  business();
$business_id = $_POST['business_id'];
$business_rut= $_POST['business_rut'];
$business_direction =strtoupper($_POST['business_direction']);
$business_phone=$_POST['business_phone'];
$business_website= strtoupper($_POST['business_website']);
$business_description= strtoupper($_POST['business_description']);

$photo = $_FILES['images']['name'];
$tmpFilePath = $_FILES['images']['tmp_name'];

$numero = null;
    for ($i=0; $i < count($photo); $i++){
        $tmpFile = $tmpFilePath[$i];
          if($tmpFile!=""){
                $shortname = $photo[$i];
                $filePath = "../../public/img/business/".$photo[$i];
                    if(move_uploaded_file($tmpFile, $filePath)){
                        $photo_destination = $shortname;
                        $numero = $photo_destination;                      
                }
            }
         }
$data =  array(
	'business_id' => $business_id, 
	'business_rut' => $business_rut,
	'business_direction' => $business_direction, 
	'business_phone' => $business_phone, 
	'business_website' => $business_phone, 
    'business_logo' => $numero,
    'business_description' => $business_description
	);

if($business->updateBusiness($data, $business_id))
{
	echo "<script>showMsg('Exito','Empresa actualizada');</script>";
}
else
{
	echo "<script>showMsg('Error','Empresa no actualizada');</script>";	
}

?>