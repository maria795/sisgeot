<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();


$diagnostic_id = (int) $_POST['diagnostic_id'];
$diagnostic = new diagnostic();
echo json_encode($diagnostic->showDiagnosticListByCode($diagnostic_id));
?>