<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$services_id =(int) $_POST['services_id'];
$services = new services();

if($user->getUserType() == 0 OR $user->getUserType() == 1)
{
if (empty($services_id))
{

	echo $services_name = strtoupper($_POST['services_name']);
	if(empty($services_name)) 
	{
		echo "<script>showMsg('Error','Existen campos vacios');</script>";
	}
	else{
		if ($services->addServices($services_name)) 
		{
			echo "<script>showMsg('Exito','Servicio registrado');$('form')[0].reset();</script>";
		}
		else
		{
			echo "<script>showMsg('Error','Servicio no registrado');</script>";	
		}
	}
}
else
{
    $services_name = strtoupper($_POST['services_name']);
	if(empty($services_name)) 
	{
		echo "<script>showMsg('Error','Existen campos vacios');</script>";
	}
	else{
		if ($services->updateServices($services_id, $services_name)) 
		{
			echo "<script>showMsg('Exito','Servicio actualizado');</script>";
		}
		else
		{
			echo "<script>showMsg('Error','Servicio no actualizado');</script>";
		}
	}
}
}
else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 
?>