<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$format_id = (int) $_POST["format_id"];
$format = new format();
$user = new user();
$result = $format->showFormatListByCode($format_id);
$lastEmail = $result[0]['format_gerent_email'];
$lastRUT = $result[0]['format_rut'];

if($user->getUserType() == 0 OR $user->getUserType() == 1)
{
	if(empty($format_id))
	{
			$format_name  = strtoupper($_POST['format_name']);
			$format_business_name  = strtoupper($_POST['format_business_name']);
			$format_rut  = strtoupper($_POST['format_rut']);
			$format_direction  = strtoupper($_POST['format_direction']);
			$format_comuna  = strtoupper($_POST['format_comuna']);
			$format_region  = strtoupper($_POST['format_region']);
			$format_phone = strtoupper($_POST['format_phone']);
			$format_gerent  = strtoupper($_POST['format_gerent']);
			$format_gerent_email = strtoupper($_POST['format_gerent_email']);

			if (empty($format_name) and empty($format_rut) and empty($format_business_name)) 
			{
				echo "<script>showMsg('Error','Existen campos vacios');</script>";
			}
			else
			{
			
					if($format->addFormat($format_name, $format_business_name, $format_rut, $format_direction, $format_comuna, $format_region, $format_phone, $format_gerent, $format_gerent_email)) 
						{
						 	echo "<script>showMsg('Exito','Empresa registrado');$('form')[0].reset();</script>";
						 	$user_type = (int) 2;
							$user_active = (int) 0;
							$user_business = "3";
							$result = $format->lastId();
							$id_format = $result[0]['format_id'];			 	
							$useer =  $user->addUser($format_name, $format_gerent_email, $format_rut, $user_type, $user_business, $id_format, $user_active);
						}
					else
						{
							echo "<script>showMsg('Error','Empresa no registrado');</script>";	
						}
			}
	}
	else
	{
			$format_name  = strtoupper($_POST['format_name']);
			$format_business_name  = strtoupper($_POST['format_business_name']);
			$format_rut  = strtoupper($_POST['format_rut']);
			$format_direction  = strtoupper($_POST['format_direction']);
			$format_comuna  = strtoupper($_POST['format_comuna']);
			$format_region  = strtoupper($_POST['format_region']);
			$format_phone = strtoupper($_POST['format_phone']);
			$format_gerent  = strtoupper($_POST['format_gerent']);
			$format_gerent_email = strtoupper($_POST['format_gerent_email']);
			$firstPassword = $user->generatePassword();


			if(("$lastEmail" != "$format_gerent_email"))  
			{				
				$cadena = $format_rut.$format_name.rand(1,99999).date('Y-m-d');
				$token  = sha1($cadena);			
			    $subject = "Usted ha sido registrado en SISGEOT";	   
				$links = $_SERVER['SERVER_NAME'].'/emailVerify.php?l3ja='.$format_id."&token=".$token;
				$body ='<div id=":153" class="a3s aXjCH m16025006a7cf3d2d"><u></u>
			  	<div>
			    <table align="center" bgcolor="#eff2f1" border="0" cellpadding="0" cellspacing="0" width="100%" style="
			    background-color: #3e3c3c;">
			  
			    <tbody><tr>
			      <td style="padding-bottom:24px;padding-top:16px">
			      <center>
			        <div style="max-width:560px;display:block;clear:both;margin:0 auto;border-collapse:collapse">
			        
			        <table align="center" bgcolor="" border="0" cellpadding="0" cellspacing="0" width="100%">
			          <tbody><tr>
			          <td style="vertical-align:middle;text-align:center;padding-bottom:16px;padding-top:8px" height="28">
			            <a href="sif.cl" title="SIGAOT" target="_blank"><img class="m_5163378976771115696logo CToWUd" style="margin:0 auto;display:block;height:100px" alt="" src="'.$_SERVER['SERVER_NAME'].'/public/img/logo.png" height="100"></a>
			          </td>
			          </tr>
			        </tbody></table>
			        
			        </div>
			        <div style="max-width:100%;display:block;clear:both;margin:0 auto">
			        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top-width:1px;border-bottom-width:1px;border-top-style:solid;border-bottom-style:solid;border-right-width:0;border-left-width:0;border-top-color:#e6e6e6;border-bottom-color:#e6e6e6">
			  
			          <tbody><tr>
			          <td style="padding-top:24px;padding-bottom:24px;padding-left:16px;padding-right:16px;text-align:left">
			            <div style="max-width:560px;display:block;clear:both;margin:0 auto">
			            
			            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px;line-height:1.5;border-collapse:collapse;text-align:left"><tbody><tr>
			    <td style="padding:16px" width="100%">
			      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px">
			        <tbody><tr>
			          <td width="100%" style="line-height:1.3;padding-bottom:8px"> 
			              <p style="font-size:24px;text-align:center;">APERTURA DE CUENTA DE USUARIO</p>
			              <br><br>
			  <strong>Hola <span style="text-transform:capitalize">'.$format_name.'</span>,</strong>
			              <br> Ha sido registrado en nuestra base de datos una cuenta con su información.
			              <br>
			              <br>
			              <strong>Si usted no solicitó obtener una cuenta en nuestro sistema, ignoré este correo electronico</strong>
			              <br>
			              <br>Para comenzar a disfrutar de nuestros servicios,  <strong> valide sus datos: </strong> 
			              <br><br>
			              <strong>Datos de su cuenta de usuario:</strong><br><br>
			              <ul style="margin:0;padding:0;margin-left:24px">
			                  <li style="margin:0;padding:0"><strong>RUT:</strong>'.' '.$format_rut.'</li>
			                  <li style="margin:0;padding:0"><strong>CONTRASEÑA:</strong>'.' '.$firstPassword.'</li>              
			                </ul>
			              <br>
			              <i>Usted sera redirecconado al presionar click en el siguiente boton</i>
			              <br><br>
			              <a href="'.$links.'" style="background: #4284ad;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">VERIFICAR CUENTA</a>
			         
			              <br> <span style="font-style:italic">Sistema de Gestión y Administración de Órdenes de Trabajo <br> (Tecnosec, Infraganti, Global Business)</span>
			          </td>
			          </td>
			        </tr>
			      </tbody></table>
			    </td>
			  </tr></tbody></table>
			  </div>
			  </td>
			  </tr>
			  </tbody></table>
			  </div>
			  </center>
			  </td>
			  </tr>';
			   $to_andress = $format_gerent_email;		
			   sent_email ($subject, $body, $to_andress);
			}
		
			if (empty($format_name) and empty($format_rut) and empty($format_business_name)) 
			{
				echo "<script>showMsg('Error','Existen campos vacios');</script>";
			}
			else
			{
				$firstPassword = $user->encrypt($firstPassword);
				if($format->updateFormat($format_id, $format_name, $format_business_name, $format_rut, $format_direction, $format_comuna, $format_region, $format_phone, $format_gerent, $format_gerent_email, $firstPassword)) 
				{
				 	echo "<script>showMsg('Exito','Empresa actualizado');</script>";	
				}
				else
				{
					echo "<script>showMsg('Error','Empresa no actualizado');</script>";	
				}
			}
	}
}

else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 
?>
