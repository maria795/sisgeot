<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$getPassword = $user->getUserPassword();
$passwordActual = $user->encrypt($_POST['password_actual']);

if($getPassword == $passwordActual) 
{
$user_p = new user();
$user_name = strtoupper($_POST['user_name']);
$user_email = strtoupper($_POST['user_email']);
$user_rut = strtoupper($_POST['user_rut']);

$id_user = $user->getUserId();
$id_user = "\"$id_user\"" ;
$user_type = "\"0\"" ;

		$data = array(
			'user_name'=> $user_name,  
			'user_rut'=> $user_rut,
			'user_email'=>$user_email 
			);

		if ($user_p->updateAdmin($data, $id_user, $user_type))
		{
			echo $db->getLastQuery();
			$_SESSION['rut'] = $user_rut;
			echo "<script>showMsg('Exito','Usuario actualizado');$('form')[0].reset();</script>";
		}
		else
		{
			echo "<script>showMsg('Error','Usuario no actualizado');</script>";	
		}
}
else{
		echo "<script>showMsg('Error','Tu contraseña actual es incorrecta');</script>";
 }

?>