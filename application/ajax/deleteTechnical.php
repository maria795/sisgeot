<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$technical_id = (int) $_POST['id'];
$technical = new technical();

if (empty($technical_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{ 
		
		if($user->getUserType() == 0) 
	 	{
			if($technical->deleteTechnical($technical_id))
			{
				echo "<script>showMsg('Exito','Tecnico eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Tecnico no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}

	}
	
?>