<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$request_id = (int) $_POST['request_id'];
$branch_office_id = $_POST['branch_office_id'];

$request = new request();
$business = new business();
$verification = new verification();

if(empty($branch_office_id) OR $branch_office_id === 0)
{
	echo "<script>showMsg('Error','Sucursal no seleccionada');</script>";				
}
else
{
	if(empty($request_id)) 
	{
			$request_department  = strtoupper($_POST['request_department']);
			$request_r_name = strtoupper($_POST['request_r_name']); 
			$request_r_email = strtoupper($_POST['request_r_email']); 
			$request_app = strtoupper($_POST['request_app']);
			$request_medium = $_POST['request_medium'];
			$request_r_phone = $_POST['request_r_phone'];
			$request_status = "0";	
			$request_date = date('Y-m-d');	
	

			if (empty($branch_office_id) and empty($request_department) and empty($request_app) and empty($request_date)) 
			{
					echo "<script>showMsg('Error','Existen campos vacios');</script>";
			}
			else
			{

	 				if(($user->getUserBusiness() == '0' OR $user->getUserBusiness() == '1' OR $user->getUserBusiness() == '2') AND $user->getUserType() == '1')
					{
						$business_id =  $user->getUserBusiness();
						$request_date;
						if ($request->addRequest($branch_office_id, $request_date, $request_department, $request_medium, $request_r_name, $request_r_phone, $request_r_email, $request_status, $request_app, $business_id)) 
						{
							echo "<script>showMsg('Exito','Requerimiento registrado');$('form')[0].reset();</script>";

							if ($request_status == '0') {
									$request_status = 'NO VERIFICADO';
								}
								//get request last data
								$result1 = $request->listRequestLast();
								$lastRequest = $result1[0]['request_id'];
								$lastBusiness = $result1[0]['business_id'];
								//get business data 

								$result2= $business->showBusinessListByCode($lastBusiness);
								$business_name = $result2[0]['business_name'];
								//Get data last request
								$result0 = $request->ShowListAll($lastRequest);
								$branch_office_cost_center = $result0[0]['branch_office_cost_center'];
								$format_name = $result0[0]['format_name'];
								$format_rut = $result0[0]['format_rut'];
								$email = $result0[0]['format_gerent_email'];

								//get token
								$cadena = $lastRequest.rand(1,99999).date('Y-m-d');
								$token  = sha1($cadena);
								$creado = date('Y-m-d');
	//get status
								$true = '1';
								$false = '0';
								//Links
								$linkV = $_SERVER['SERVER_NAME'].'/siaget/verify.php?x='.$lastRequest.'&y='.$true.'&token='.$token;

								$linkF = $_SERVER['SERVER_NAME'].'/siaget/verify.php?x='.$lastRequest.'&y='.$false.'&token='.$token;

					
							$message = '<div id=":153" class="a3s aXjCH m16025006a7cf3d2d"><u></u>
							  <div>
							  
							    <table align="center" bgcolor="#eff2f1" border="0" cellpadding="0" cellspacing="0" class="m_5163378976771115696backgroundtable" width="100%" style="
							    background-color: #3e3c3c;">
							  
							    <tbody><tr>
							      <td style="padding-bottom:24px;padding-top:16px">
							      <center>
							        <div style="max-width:560px;display:block;clear:both;margin:0 auto;border-collapse:collapse">
							        
							        <table align="center" bgcolor="" border="0" cellpadding="0" cellspacing="0" width="100%">
							          <tbody><tr>
							          <td style="vertical-align:middle;text-align:center;padding-bottom:16px;padding-top:8px" height="28">
							            <a href="sif.cl" title="SIGAOT" target="_blank"><img class="m_5163378976771115696logo CToWUd" style="margin:0 auto;display:block;height:100px" height="100"></a>
							          </td>
							          </tr>
							        </tbody></table>
							        
							        </div>
							        <div style="max-width:100%;display:block;clear:both;margin:0 auto">
							        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top-width:1px;border-bottom-width:1px;border-top-style:solid;border-bottom-style:solid;border-right-width:0;border-left-width:0;border-top-color:#e6e6e6;border-bottom-color:#e6e6e6">
							  
							          <tbody><tr>
							          <td style="padding-top:24px;padding-bottom:24px;padding-left:16px;padding-right:16px;text-align:left">
							            <div style="max-width:560px;display:block;clear:both;margin:0 auto">
							            
							            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px;line-height:1.5;border-collapse:collapse;text-align:left"><tbody><tr>
							    <td style="padding:16px" width="100%">
							      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px">
							        <tbody><tr>
							            
							          <td width="100%" style="line-height:1.3;padding-bottom:8px"> 


							              <p style="font-size:24px;text-align:center;">VERIFICACION DE REQUERIMIENTO</p>
							              <br><br>
							  <strong><span style="text-transform:capitalize">Estimado cliente</span>,</strong>
							              
							            
							              <br> Se ha registrado un nuevo requerimiento a su nombre en nuestro sistema de información
							              <br>
							              <br>
							              <strong>Si usted no solicitó nuestros servicios, ignoré este correo electronico</strong>
							              <br><br>
							    
							              <strong>Se muestra a continuación la información relacionada con el requerimiento</strong><br><br>
							              <ul style="margin:0;padding:0;margin-left:24px">
							                  <li style="margin:0;padding:0"><strong>CLIENTE:</strong>'.' '.$format_name.'</li>
							                  <li style="margin:0;padding:0"><strong>CENTRO DE COSTO:</strong>'.' '.$branch_office_cost_center.'</li>
							                  <li style="margin:0;padding:0"><strong>FECHA:</strong>'.' '.$request_date.'</li>
							                  <li style="margin:0;padding:0"><strong>DEPARTAMENTO:</strong>'.' '.$request_department.'</li>
							                
							                  <li style="margin:0;padding:0"><strong>RESPONSABLE DE LA SOLICITUD:</strong>'.' '.$request_r_name.'</li>
							                  <li style="margin:0;padding:0"><strong>FONO:</strong>'.' '.$request_r_phone.'</li>
							              
							                  <li style="margin:0;padding:0"><strong>ESTADO:</strong>'.' '.$request_status.'</li>
							                  <li style="margin:0;padding:0"><strong>REPORTE:</strong>'.' '.$request_app.'</li>
							                 
							                </ul>
							              <br>
							              <i>Por favor, si esta conforme, verifique la solicitud de requerimiento, de no ser asi, rechacela y solicitela nuevamente</i>
							    
							    
							              <br><br>
							              <div style="display:flex;justify-content: space-around">
							              <a href="'.$linkF.'" style="width: 40%;background: #e9685e;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">RECHAZAR</a>
							            
							              <a href="'.$linkV.'" style="    width: 40%;background: #4284ad;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">VERIFICAR</a>
							            </div>
							          </td>
							          </td>
							        </tr>
							      </tbody></table>
							    </td>
							  </tr></tbody></table>
							  
							  </div>
							  </td>
							  </tr>
							  </tbody></table>
							  </div>
							  
							  </center>
							  </td>
							  </tr>
							  </tbody></table>
							  
							  </div>';

								//Send email
								$subject = "Verificacion de requerimiento";
								$body = $message;;
								$to_andress = $email;	

								if (sent_email($subject, $body, $to_andress))
								{
								   	$json = '{"success":"1","info":"send"}';
									echo $json;
								}
								else
								{
				  					$json = '{"success":"0","info":"errorSending"}';
									echo $json;				
								}

						}
						else
						{
							echo "<script>showMsg('Error','Requerimiento no registrado');</script>";	
						}
					}
					else if($user->getUserBusiness() == '' and $user->getUserType() == '0')
					{
						$business_id = $_POST["business_id"];

						if ($request->addRequest($branch_office_id, $request_date, $request_department, $request_medium, $request_r_name, $request_r_phone, $request_r_email, $request_status, $request_app, $business_id)) 
						{
							echo "<script>showMsg('Exito','Requerimiento registrado');$('form')[0].reset();</script>";

								if ($request_status == '0') {
									$request_status = 'NO VERIFICADO';
								}
								//get request last data
								$result1 = $request->listRequestLast();
								$lastRequest = $result1[0]['request_id'];
								$lastBusiness = $result1[0]['business_id'];
								//get business data 

								$result2= $business->showBusinessListByCode($lastBusiness);
								$business_name = $result2[0]['business_name'];
								//Get data last request
								$result0 = $request->ShowListAll($lastRequest);
								$branch_office_cost_center = $result0[0]['branch_office_cost_center'];
								$format_name = $result0[0]['format_name'];
								$format_rut = $result0[0]['format_rut'];
								$email = $result0[0]['format_gerent_email'];

								//get token
								$cadena = $lastRequest.rand(1,99999).date('Y-m-d');
								$token  = sha1($cadena);
								$creado = date('Y-m-d');
								$true = '1';
								$false = '0';
								//Links
								$linkV = $_SERVER['SERVER_NAME'].'/verify.php?x='.$lastRequest.'&y='.$true.'&token='.$token;

								$linkF = $_SERVER['SERVER_NAME'].'/verify.php?x='.$lastRequest.'&y='.$false.'&token='.$token;

					
							$message = '<div id=":153" class="a3s aXjCH m16025006a7cf3d2d"><u></u>
							  <div>
							  
							    <table align="center" bgcolor="#eff2f1" border="0" cellpadding="0" cellspacing="0" class="m_5163378976771115696backgroundtable" width="100%" style="
							    background-color: #3e3c3c;">
							  
							    <tbody><tr>
							      <td style="padding-bottom:24px;padding-top:16px">
							      <center>
							        <div style="max-width:560px;display:block;clear:both;margin:0 auto;border-collapse:collapse">
							        
							        <table align="center" bgcolor="" border="0" cellpadding="0" cellspacing="0" width="100%">
							          <tbody><tr>
							          <td style="vertical-align:middle;text-align:center;padding-bottom:16px;padding-top:8px" height="28">
							            <a href="sif.cl" title="SIGAOT" target="_blank"><img class="m_5163378976771115696logo CToWUd" style="margin:0 auto;display:block;height:100px" src="'.$_SERVER['SERVER_NAME'].'/public/img/logo.png" height="100"></a>
							          </td>
							          </tr>
							        </tbody></table>
							        
							        </div>
							        <div style="max-width:100%;display:block;clear:both;margin:0 auto">
							        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top-width:1px;border-bottom-width:1px;border-top-style:solid;border-bottom-style:solid;border-right-width:0;border-left-width:0;border-top-color:#e6e6e6;border-bottom-color:#e6e6e6">
							  
							          <tbody><tr>
							          <td style="padding-top:24px;padding-bottom:24px;padding-left:16px;padding-right:16px;text-align:left">
							            <div style="max-width:560px;display:block;clear:both;margin:0 auto">
							            
							            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px;line-height:1.5;border-collapse:collapse;text-align:left"><tbody><tr>
							    <td style="padding:16px" width="100%">
							      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px">
							        <tbody><tr>
							            
							          <td width="100%" style="line-height:1.3;padding-bottom:8px"> 


							              <p style="font-size:24px;text-align:center;">VERIFICACION DE REQUERIMIENTO</p>
							              <br><br>
							  <strong><span style="text-transform:capitalize">Estimado cliente</span>,</strong>
							              
							            
							              <br> Se ha registrado un nuevo requerimiento a su nombre en nuestro sistema de información
							              <br>
							              <br>
							              <strong>Si usted no solicitó nuestros servicios, ignoré este correo electronico</strong>
							              <br><br>
							    
							              <strong>Se muestra a continuación la información relacionada con el requerimiento</strong><br><br>
							              <ul style="margin:0;padding:0;margin-left:24px">
							                  <li style="margin:0;padding:0"><strong>CLIENTE:</strong>'.' '.$format_name.'</li>
							                  <li style="margin:0;padding:0"><strong>CENTRO DE COSTO:</strong>'.' '.$branch_office_cost_center.'</li>
							                  <li style="margin:0;padding:0"><strong>FECHA:</strong>'.' '.$request_date.'</li>
							                  <li style="margin:0;padding:0"><strong>DEPARTAMENTO:</strong>'.' '.$request_department.'</li>
							                
							                  <li style="margin:0;padding:0"><strong>RESPONSABLE DE LA SOLICITUD:</strong>'.' '.$request_r_name.'</li>
							                  <li style="margin:0;padding:0"><strong>FONO:</strong>'.' '.$request_r_phone.'</li>
							              
							                  <li style="margin:0;padding:0"><strong>ESTADO:</strong>'.' '.$request_status.'</li>
							                  <li style="margin:0;padding:0"><strong>REPORTE:</strong>'.' '.$request_app.'</li>
							                 
							                </ul>
							              <br>
							              <i>Por favor, si esta conforme, verifique la solicitud de requerimiento, de no ser asi, rechacela y solicitela nuevamente</i>
							    
							    
							              <br><br>
							              <div style="display:flex;justify-content: space-around">
							              <a href="'.$linkF.'" style="width: 40%;background: #e9685e;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">RECHAZAR</a>
							            
							              <a href="'.$linkV.'" style="    width: 40%;background: #4284ad;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">VERIFICAR</a>
							            </div>
							          </td>
							          </td>
							        </tr>
							      </tbody></table>
							    </td>
							  </tr></tbody></table>
							  
							  </div>
							  </td>
							  </tr>
							  </tbody></table>
							  </div>
							  
							  </center>
							  </td>
							  </tr>
							  </tbody></table>
							  
							  </div>';

								//Send email
								$subject = "Verificacion de requerimiento";
								$body = $message;;
								$to_andress = $email;	

								if (sent_email($subject, $body, $to_andress))
								{
								   	$json = '{"success":"1","info":"send"}';
									echo $json;
								}
								else
								{
				  					$json = '{"success":"0","info":"errorSending"}';
									echo $json;				
								}
						}
						else
						{
							echo "<script>showMsg('Error','Requerimiento no registrado');</script>";	
						}

					}

			}
	}
	else
	{
			$request_department  = strtoupper($_POST['request_department']);
			$request_r_name = strtoupper($_POST['request_r_name']); 
			$request_r_email = strtoupper($_POST['request_r_email']); 
			$request_app = strtoupper($_POST['request_app']);
			$request_medium = $_POST['request_medium'];
			$request_r_phone = $_POST['request_r_phone'];
			$business_id = $_POST["business_id"];

			if (empty($branch_office_id) and empty($request_department) and empty($request_app) and empty($request_date)) 
			{
					echo "<script>showMsg('Error','Existen campos vacios');</script>";
			}
			else
			{
				if ($request->updateRequest($request_id, $request_department, $request_medium, $request_r_name, $request_r_phone, $request_r_email, $request_app, $business_id)) 
				{
					echo "<script>showMsg('Exito','Requerimiento actualizado');$('form')[0].reset();</script>";

				}
				else

				{
					echo "<script>showMsg('Error','Requerimiento no actualizado');</script>";	
				}
			}
	}
}
?>