<?php
include("../core/conexion.php");
if($user->isAccess() === false) die();
$branch_office_id = (int) $_POST['id'];
$branchOffice = new branchOffice();
if(empty($branch_office_id))
{
	echo "<script>showMsg('Error','Campo vacio');</script>";
}
else
{
		if($user->getUserType() == 0) 
	 	{
			if($branchOffice->deleteBranchOffice($branch_office_id))
			{
				echo "<script>showMsg('Exito','Sucursal eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Sucursal no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
}
?>