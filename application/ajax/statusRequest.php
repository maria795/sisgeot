<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$request = new request();
$data=$_POST["data"];
$a = json_decode($data, true);
$request_id = $a['request_id'];
$request_status = $a['request_status'];

if($user->getUserType() == 0 OR $user->getUserType() == 2) 
{
	$request->updateStatusRequest($request_id, $request_status);
	echo "<script>showMsg('Exito','Requerimiento verificado');</script>";
}
else if($user->getUserType() == 1) 
{
	echo "<script>showMsg('Error','Permiso denegado');</script>";
}


?>