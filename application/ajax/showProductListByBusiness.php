<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$business_id = $user->getUserBusiness();
$product = new product();
echo json_encode($product->showProductListByBusiness($business_id));
?>