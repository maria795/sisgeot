<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$branch_office_id = (int) $_POST['branch_office_id'];
$branchOffice = new branchOffice();

echo json_encode($branchOffice->showBranchOfficeListByCode($branch_office_id));
?>