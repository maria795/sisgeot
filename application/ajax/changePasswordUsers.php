 <?php
include("../core/conexion.php");    
if ($user->isAccess() === false) die();

$changes = new recoverPassword();

$getPassword = $user->getUserPassword();
$passwordActual = $user->encrypt($_POST['password_0']);
$password1 = $_POST['password_1'];
$password2 = $_POST['password_2'];

$id_user = $user->getUserId();
$id_user = "\"$id_user\"" ;

if($getPassword == $passwordActual) 
{
	if($changes->comparePassword($password1, $password2))
	{
		if ($changes->updatePasswordUser($password1, $id_user))
		{
				$_SESSION['password'] = $password1;
				echo "<script>showMsg('Exito','Contraseña cambiada');</script>";
 		}
		else
		{
			 	echo "<script>showMsg('Error','Las contraseñas no cambiadas');</script>";

		}
	}
	else
	{
		echo "<script>showMsg('Error','Las contraseñas no coinciden');</script>";

	}
}
else
{
	echo "<script>showMsg('Error','Tu contraseña actual es incorrecta');</script>";

}

?>