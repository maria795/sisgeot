<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$product_id = (int) $_POST['id'];
$product = new product();
if (empty($product_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{ 
	if($user->getUserType() == 0) 
	 	{
			if($product->deleteProduct($product_id))
			{
				echo "<script>showMsg('Exito','Producto eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Producto no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}

?>

