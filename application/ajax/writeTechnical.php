<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$technical_id = $_POST['technical_id'];

$technical = new technical();
if($user->getUserType() == 0 OR $user->getUserType() == 1)
{
if (empty($technical_id)) 
{
		$technical_name=strtoupper($_POST['technical_name']); 
		$technical_rut=strtoupper($_POST['technical_rut']);
		$technical_phone=strtoupper($_POST['technical_phone']);
		$technical_position=strtoupper($_POST['technical_position']);
		$technical_email=strtoupper($_POST['technical_email']);

		if (empty($technical_name) and empty($technical_rut) and empty($technical_email)) {
		
			echo "<script>showMsg('Error','Existen campos vacios');</script>";
		}
		else
		{
			if($technical->addTechnical($technical_name, $technical_rut, $technical_position, $technical_phone, $technical_email))
			{
				echo "<script>showMsg('Exito','Técnico registrado');$('form')[0].reset();</script>";
			}
			else
			{
				echo "<script>showMsg('Error','Técnico no registrado');</script>";	
			}
		}
}

else
{
		$technical_name=strtoupper($_POST['technical_name']); 
		$technical_rut=strtoupper($_POST['technical_rut']);
		$technical_position=strtoupper($_POST['technical_position']);
		$technical_phone=strtoupper($_POST['technical_phone']);
		$technical_email=strtoupper($_POST['technical_email']);

		if (empty($technical_name) and empty($technical_rut) and empty($technical_email)) {
		
			echo "<script>showMsg('Error','Existen campos vacios');</script>";
		}
		else
		{
			if($technical->updateTechnical($technical_id, $technical_name, $technical_rut, $technical_position, $technical_phone, $technical_email))
			{
				echo "<script>showMsg('Exito','Técnico actualizado');</script>";
			}
			else
			{
				echo "<script>showMsg('Error','Técnico no actualizado');</script>";	
			}
		}
}
}
else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 
?>