<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$format_id = (int) $_POST["id"];
$format = new format();
if (empty($format_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{ 
	if($user->getUserType() == 0) 
	 	{ 
			if($format->deleteFormat($format_id))
			{
				echo "<script>showMsg('Exito','Cliente eliminado');</script>";	

			}
			else{
				echo "<script>showMsg('Error','Cliente no eliminado');</script>";	
			}
		}
	else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
	else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}

?>