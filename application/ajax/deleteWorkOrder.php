<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$work_order_id = (int) $_POST['id'];
$work_order = new workOrder();

if (empty($work_order_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{
		if($user->getUserType() == 0) 
	 	{
			if($work_order->deleteOrderWork($work_order_id))
			{
				echo "<script>showMsg('Exito','Orden de trabajo eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Orden de trabajo no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
		{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
		} 
		else
		{
			echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
		}
	}
?>