<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();


$request_id = (int) $_POST['id'];
$request = new request;
if (empty($request_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{
	if($user->getUserType() == 0) 
	 { 
			if($request->deleteRequest($request_id))
			{
				echo "<script>showMsg('Exito','Requerimiento eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Requerimiento no eliminado');</script>";	
			}
		}
	else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
	else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}
