<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$diagnostic_id = (int) $_POST['id'];
$diagnostic = new diagnostic;
if (empty($diagnostic_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{ 
	if($user->getUserType() == 0) 
	 	{
			if($diagnostic->deleteDiagnostic($diagnostic_id))
			{
				echo "<script>showMsg('Exito','Diagnostico eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Diagnostico  no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}
?>