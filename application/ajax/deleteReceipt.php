<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();
$receipt_id = (int) $_POST['id'];
$receipt = new receipt();
if (empty($receipt_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}
else
	{ 
	if($user->getUserType() == 0) 
	 	{
			if($receipt->deleteReceipt($receipt_id))
			{
				echo "<script>showMsg('Exito','receipto eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','receipto no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}
	}

?>