 <?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();


$diagnostic_id = (int) $_POST['diagnostic_id'];
$diagnostic = new diagnostic();

if($user->getUserType() == 0 OR $user->getUserType() == 1)
{
	if (empty($diagnostic_id)) 
	{
		$diagnostic_name = strtoupper($_POST['diagnostic_name']);

		if (empty($diagnostic_name))
		{
			echo "<script>showMsg('Error','Existen campos vacios');</script>";
		}
		else
		{
			if($diagnostic->addDiagnostic($diagnostic_name)) 
			{
				echo "<script>showMsg('Exito','Diagnóstico registrado');$('form')[0].reset();</script>";
			}
			else
			{
				echo "<script>showMsg('Error','Diagnóstico no registrado');</script>";	
			}
		}
	}
	else
	{
		$diagnostic_name = strtoupper($_POST['diagnostic_name']);
		echo "a".$diagnostic_name;
		if (empty($diagnostic_name)) 
		{
			echo "<script>showMsg('Error','Existen campos vacios');</script>";
		}
		else
		{
			if($diagnostic->updateDiagnostic($diagnostic_id, $diagnostic_name)) 
			{
				echo "<script>showMsg('Exito','Diagnóstico actualizado');</script>";
			}
			else
			{
				echo "<script>showMsg('Error','Diagnóstico no actualizado');</script>";	
			}
		}
	}
}
else{
	echo "<script>showMsg('Error','Permiso denegado');</script>";	
} 

?>