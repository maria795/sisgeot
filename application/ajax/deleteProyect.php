 <?php
include("../core/conexion.php");

if ($user->isAccess() === false) die();


$proyect_id = (int) $_POST['id'];
$proyect = new proyect();

if (empty($proyect_id))
	{ 
		echo "<script>showMsg('Error','Campo vacio');</script>";
	}

else
	{ 
		if($user->getUserType() == 0) 
	 	{
			if($proyect->deleteProyect($proyect_id))
			{
				echo "<script>showMsg('Exito','Proyecto eliminado');</script>";	
			}
			else
			{
				echo "<script>showMsg('Error','Proyecto no eliminado');</script>";	
			}
		}
		else if($user->getUserType() == 1 OR $user->getUserType() == 2)
			{
				echo "<script>showMsg('Error','Permiso denegado');</script>";	
			} 
		else
			{
			 	echo "<script>showMsg('Error','No posee acceso al sistema');</script>";	
			}		
	}
?>