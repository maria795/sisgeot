<?php
include("../core/conexion.php");
if ($user->isAccess() === false) die();

$format_name =  $_POST["format_name"];

$format = new format();

echo json_encode($format->showFormatListByName($format_name));

?>