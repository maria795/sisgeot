function login()
{
  $(".loginError").removeClass("error");
   $(".login-form").removeClass("is-visible");
  var url = 'application/ajax/login.php';
  var data =$('#login-box').serialize();
  $.ajax(
  {
    type:'POST',
    url:url,
    data:data,
    success: function(response)
    {
      var data = JSON.parse(response);
      if(data.access==1)
      {
        console.log(data.url);
        $("#redir").html("<script>window.location.href = '"+data.url+"'</script>");
      }
      else if(data.access==0)
      {
        if(data.info=="inactive")
        {
          $("#inputUser").addClass("error");
          $("#userError").addClass("is-visible").text("El usuario ha sido desactivado");

        }
        else if(data.info=="error")
        {
          $("#inputUser").addClass("error");
          $("#userError").addClass("is-visible").text("Error al identificar usuario");
        }
        else if(data.info=="invalid")
        {
          $("#inputUser").addClass("error");
          $("#inputPass").addClass("error");
          $("#passError").addClass("is-visible").text("Usuario o contraseña incorrecta");

        }
      }
      
    }
  });
  return false;
}

  $('#inputUser, #inputPass').click(function() {
    if($('#inputPass, #inputUser').val(''))
    {
          $("#inputUser").removeClass("error");
          $("#inputPass").removeClass("error");
          $("#userError").removeClass("is-visible");
          $("#passError").removeClass("is-visible");
          console.log("aca");
    }
  });


function requestNewPassword()
{
  $("#inputUser").removeClass("error");
   $(".loginError").removeClass("is-visible").removeClass("green");
  var url = 'application/ajax/setEmail.php';
  $.ajax(
  {
    type:'POST',
    url:url,
    data:$('#recover').serialize(),
    success: function(response)
    {
      var data = JSON.parse(response);
      if(data.success==1)
      {
        $(".loginError").addClass("is-visible").addClass("green").text("La informacion para recuperar su contraseña ha sido enviada");
      }
      if(data.success==0)
      {
        if(data.info=="errorSending")
        {
        $(".loginError").addClass("is-visible").text("Ha ocurrido un error");
        $("#inputUser").addClass("error");
      }

      if(data.success==0)
      {
        if(data.info=="errorSend")
        {
        $(".loginError").addClass("is-visible").text("Ya fue enviado un email con las instrucciones");
        $("#inputUser").addClass("error");
        }

        else if(data.info=="invalid")
        {
        $(".loginError").addClass("is-visible").text("El correo electronico no es valido");
      }
      }
    }
  }
  });
  return false;
}



function changePassword()
{
  $(".loginError").removeClass("is-visible").removeClass("green");
  $("#password-1").removeClass("error");
            $("#password-2").removeClass("error");
  var pass1 = $("#password-1").val();
  var pass2 = $("#password-2").val();
  if(pass1 != pass2)
  {
    $("#passError").addClass("is-visible").html("Las contraseñas no coinciden");
    return false;
  }
  else
  {
    var url = 'application/ajax/changePassword.php';
    $.ajax(
    {
      type:'POST',
      url:url,
      data:$('#login-box').serialize(),
      success: function(response)
      {
        var data = JSON.parse(response);
        if(data.success==1)
        {
          $("#passError").addClass("is-visible").addClass("green").html("Contraseña actualizada satisfactoriamente");
        }
        else if(data.success==0)
        {
          if(data.info=="error")
          {
            $("#passError").addClass("is-visible").html("Ha ocurrido un error");
            $("#password-1").addClass("error");
            $("#password-2").addClass("error");
          }
          if(data.info=="diferent")
          {
             $("#passError").addClass("is-visible").html("Las contraseñas no coinciden");
             $("#password-1").addClass("error");
            $("#password-2").addClass("error");
          }
        }

        
      }
    });
    return false;
  }
}