window.preview = function (input) 
{
	$(input.files).each(function (key,val) 
        {
        	console.log(val);
        });
    if (input.files && input.files[0]) 
    {
        $(input.files).each(function (key,val) 
        {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            reader.onload = function (e) 
            {
            	
                $("#previewImg").append("<img class='thumb' src='" + e.target.result + "'>");
                $("body").find(".fileContainer").append('<input type="file" id="images" onchange="preview(this);" onClick=" this.value='+"almacenado"+';" name="images[]" multiple "/>');
            }
        });
    }
}


$(document).on({
    ajaxStart: function() { $('#loading-div').addClass("active");    },
     ajaxStop: function() { 
    window.setTimeout(function()
	{
     	$('#loading-div').removeClass("active"); 
     }, 0);
     }    
});

function hideBtn()
{
	$("body").find(".breadcumb").append('<li><a href="#">Editar</a></li>');
	$(".print-p").addClass("hidden");
	$(".save-reg-p").removeClass("hidden");
	$(".new-customer-trigger-p").addClass("hidden");
}


function userType()
{
	checkUser2().done(function()
	{
		console.log(user);
	});
}

function checkUser2()
{
var dfrd1 = $.Deferred();
$.get("application/ajax/checkUser.php", function (data) {
	user = data;
	dfrd1.resolve();
});
return $.when(dfrd1).done().promise();
}


$(window).on('load',function() {

	$(function()
	{
		checkUser().done(function()
		{
			selectContentByUser().done();
		});
	});

function checkUser()
{
	var dfrd1 = $.Deferred();
	$.get("application/ajax/checkUser.php", function (data) {
		user = data;
		dfrd1.resolve();
	});
	return $.when(dfrd1).done().promise();
}

function selectContentByUser()
{
	var dfrd1 = $.Deferred();
	$(".hideForCustommer").hide();
	
	$(".loader").fadeOut("fast");
	console.log(user)
	dfrd1.resolve();
	return dfrd1.promise();
}

});


function changeSection(section)
{
	
	var sect = section;
	if(sect=="logout")
	{
		window.location.href = "application.php?logout=1";
	}
	var url = "public/views/"+sect.toLowerCase()+".html";
	
	var tableFunc = window["table"+sect];
	$.when(
		$.get(url, function(data) {
			tablesDestroyer();
			$('#content').html(data);
		})
	).done(function() {
		tableFunc();
		if(sect!="")
		{
		$("body").find(".breadcumb").html('<li><a href="index.php">Home</a></li><li class="'+sect+' optionsDir"><a class="load-content"  href="'+sect.toLowerCase()+'.html">'+$(".left-nav ul .active").text()+'</a></li>');
		}
		if(user==2)
		{$(".new-customer-trigger").hide();}
	});
}


jQuery(document).ready(function()
{
	
	
$('.options,.optionsDir').on("click",function(event)
	{
		event.preventDefault();

		$('.options').removeClass("active");
		$('.options2').removeClass("active");
    	
    	$(".left-nav").removeClass("visible");
		$(".page-header").removeClass("right");
		$(".page-header-trigger").removeClass("clicked")
		$(".content").removeClass("moved");
		$("body").scrollTop(0);
		$('body').css({overflow: 'auto',});


		var classes = $(this).attr("class").split(/\s/);
		var section = classes[0];
		$("."+section).addClass("active");
		changeSection(section);
	});
 });





function tablesDestroyer()
{
	$('#main-tbody-table').DataTable().destroy();
	$('#branchOffice-content-table').DataTable().destroy();
	$('#req-content-table').DataTable().destroy();
	$('#ots-content-table').DataTable().destroy();
	$('#proyect-content-table').DataTable().destroy();
	$('#services-content-table').DataTable().destroy();
	$('#technical-content-table').DataTable().destroy();
	$('#product-content-table').DataTable().destroy();
	$('#diagnostic-content-table').DataTable().destroy();
	$('#client-content-table').empty();
	$('#branchOffice-content-table').empty();
	$('#req-content-table').empty();
	$('#ots-content-table').empty();
	$('#proyect-content-table').empty();
	$('#services-content-table').empty();
	$('#technical-content-table').empty();
	$('#product-content-table').empty();
	$('#diagnostic-content-table').empty();
}

var idioma={
  "sProcessing":"Procesando...",
  "sZeroRecords":"No se encontraron resultados",
  "sEmptyTable":"Ningún dato disponible en esta tabla",
  "sInfo": "",
  "lengthMenu":"Mostrar _MENU_ registros por página",
  "infoFiltered":"(Filtrado de _MAX_ total entradas)",
  "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
  "sSearch":"Buscar: ",
  "paginate":{
    "first":"Primero",
    "last":"Ultimo",
    "next":"Siguiente",
    "previous":"Anterior"
  },
};

function createDataTable()
{
	var oTable = $('.main-table').DataTable({
		
	"paging": false,
	"lengthChange": true,
	"fixedHeader": false,
	"language": idioma,
}
		
	);
	$("body").find("#search-input").on("keyup", function()
	{
	  oTable.search($(this).val()).draw();
	})

		$('#fini').keyup( function() 
		{ 
			$.fn.dataTableExt.afnFiltering.push(function( oSettings, aData, iDataIndex ) 
			{
				var iFini = document.getElementById('fini').value;
				var iFfin = document.getElementById('ffin').value;
				var iStartDateCol = 0;
				var iEndDateCol = 0;
				iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
				iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

				var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
				var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

				if ( iFini === "" && iFfin === "" ){return true;}
				else if ( iFini <= datofini && iFfin === ""){return true;}
				else if ( iFfin >= datoffin && iFini === ""){return true;}
				else if (iFini <= datofini && iFfin >= datoffin){return true;}
				return false;
			});
			console.log("hola");
			oTable.draw(); 
		});

		$('#ffin').keyup( function() 
		{ 
			$.fn.dataTableExt.afnFiltering.push(
			function( oSettings, aData, iDataIndex ) 
			{
				var iFini = document.getElementById('fini').value;
				var iFfin = document.getElementById('ffin').value;
				var iStartDateCol = 0;
				var iEndDateCol = 0;

				iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
				iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

				var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
				var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

				if ( iFini === "" && iFfin === "" ){return true;}
				else if ( iFini <= datofini && iFfin === ""){return true;}
				else if ( iFfin >= datoffin && iFini === ""){return true;}
				else if (iFini <= datofini && iFfin >= datoffin){return true;}
				return false;
			});
			oTable.draw(); 
		});
}

/* BUSINESS -----------------------------------------------------------------------------*/

function tableFormat()
{
	
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 

	$.getJSON('application/ajax/listFormat.php', function(response)
	{
		var table;
		$('#client-content-table-tbody').empty();
		$.each(response, function(key, val) 
		{
			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Nombre">'+val.format_name+'</td>';
			table += 	'<td data-label="Razon social">' +val.format_business_name+'</td>';
			table += 	'<td data-label="RUT">'+val.format_rut+'</td>';
			table += 	'<td data-label="Direccion">'+val.format_direction+'</td>';
			table += 	'<td data-label="Comuna">'+val.format_comuna+'</td>';
			table += 	'<td data-label="Region">'+val.format_region+'</td>';
			table += 	'<td data-label="Fono">'+val.format_phone+'</td>';
			table += 	'<td data-label="Gerente">'+val.format_gerent+'</td>';
			table += 	'<td data-label="Email Gerente">'+val.format_gerent_email+'</td>';
			table += 	'<td class="no-print">';
			table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
			table += 		'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editBusiness('+val.format_id+')"><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.format_id+',\'Format\')"><a href="#0">Eliminar</a></li>';
			table += 		'</ul>';
			table += 	'</td>';
			table += '</tr>';
					 
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});
}

function insertBusiness()
{
	var url='application/ajax/writeFormat.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-customer-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}

function editBusiness(format_id)
{
	 hideBtn();
	var id=format_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showFormatListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'format_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#format_id").val(data[0]["format_id"]);
			$("body").find("#format_name").val(data[0]["format_name"]);
			$("body").find("#format_business_name").val(data[0]['format_business_name']);
			$("body").find("#format_rut").val(data[0]['format_rut']);
			$("body").find("#format_direction").val(data[0]['format_direction']);
			$("body").find("#format_comuna").val(data[0]['format_comuna']);
			$("body").find("#format_region").val(data[0]['format_region']);
			$("body").find("#format_phone").val(data[0]['format_phone']);
			$("body").find("#format_gerent").val(data[0]['format_gerent']);
			$("body").find("#format_gerent_email").val(data[0]['format_gerent_email']);
  		}
 	});
}






//-------------------------------------------------------------------------------


function tableProyect()
{
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 
	$.getJSON('application/ajax/listProyect.php', function(response)
	{
		var table;
		$('#proyect-content-table-tbody').empty();
		$.each(response, function(key, val) 
		{	
			val.proyect_date = val.proyect_date.split('-');
			val.proyect_date = val.proyect_date[2]+'-'+val.proyect_date[1]+'-'+val.proyect_date[0];
			
			table +='<tr>';
			table +=	'<td class="t-trigger" data-label="Codigo">'+val.proyect_code+'</td>';
			table +=	'<td data-label="Nombre">'+val.proyect_name+'</td>';
			table +=	'<td data-label="Descripción técnica">' +val.proyect_name+'</td>';
			table +=	'<td data-label="Jefe">'+val.proyect_boss+'</td>';
			table +=	'<td data-label="Fecha">'+val.proyect_date+'</td>';
			table +=	'<td data-label="Acciones" class="no-print">';
			table +=		'<button class="tableDropdown dropdowns">Aciones</button>';
			table += 		'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editProyect('+val.proyect_id+')  "><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.proyect_id+',\'Proyect\')"><a href="#0">Eliminar</a></li>';
			
			table += 		'</ul>';
			table +=	'</td>';
			table +='</tr>';
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});
}




function insertProyect()
{
	if(isValidDate($("body").find("#proyect_date").val()) == "error")
	{
		
		return false;
	}
	else
	{
	var url='application/ajax/writeProyect.php';
	//console.log($('#new-proyect-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-proyect-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
	 return false;
}
}

function showProyectListByCode(proyect_id)
{
	var id=proyect_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showProyectListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'proyect_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
			
			$("body").find("#proyect_code").val(data['']);
			$("body").find("#proyect_name").val(data['']);
			$("body").find("#product_description").val(data['']);
			$("body").find("#proyect_boss").val(data['']);
			$("body").find("#proyect_date").val(data['']);

		
  		}
 	});
}



function editProyect(proyect_id)
{ hideBtn();
	var id=proyect_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	$('#new-proyect-form')[0].reset();
	var url='application/ajax/showProyectListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'proyect_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
			$("body").find("#proyect_id").val(data[0]["proyect_id"]);
			$("body").find("#proyect_name").val(data[0]["proyect_name"]);
			$("body").find("#proyect_description").val(data[0]['proyect_name']);
			$("body").find("#proyect_boss").val(data[0]['proyect_boss']);
			$("body").find("#proyect_date").val(data[0]['proyect_date']);
			$("body").find("#proyect_code").val(data[0]['proyect_code']);
  		}
 	});
}



function tableWorkOrder()
{userType();
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		   $("body").find(".save-div.advanced").removeClass('hidden');
		}
	   }, 100); 
	   $("body").find(".rangos").addClass("active");
	$.getJSON('application/ajax/listWorkOrder.php', function(response)
	{
		
		var table;
		$('#ots-content-table-tbody').empty();
		$.each(response, function(key, value)
		{	
			var servicesAcum="";
			var diagnosticAcum="";
			var productsAcum="";
			var technicalAcum="";
			$.each(response[key].services, function(key2, value2)
			{	
				if(value2!=false)
				{	
				servicesAcum += (response[key].services[key2].services_name);
				servicesAcum += ", ";
				}
				else
				{
				servicesAcum = "";
				}

			});

			$.each(response[key].technical, function(key2, value2)
			{
				if(value2!=false)
				{
				technicalAcum += (response[key].technical[key2].technical_name);
				technicalAcum += ", ";
				}
				else
				{
				technicalAcum = "";
				}
			});
		

			$.each(response[key].product, function(key2, value2)
			{
				if(value2!=false)
				{
					productsAcum += (response[key].product[key2].product_description);
					productsAcum += ", ";
				}
				else
				{
					productsAcum = "";
				}
			});

			$.each(response[key].diagnostic, function(key2, value2)
			{
				if(value2!=false)
				{
				diagnosticAcum += (response[key].diagnostic[key2].diagnostic_name);
				diagnosticAcum += ", ";
				}
				else
				{
				diagnosticAcum ="";
				}
			});

			
			servicesAcum = servicesAcum.slice(0,-2);
			diagnosticAcum = diagnosticAcum.slice(0,-2);
			productsAcum = productsAcum.slice(0,-2);
			technicalAcum = technicalAcum.slice(0,-2);
			
			response[key].work_order_generation = response[key].work_order_generation.split('-');
			response[key].work_order_generation = response[key].work_order_generation[2]+'-'+response[key].work_order_generation[1]+'-'+response[key].work_order_generation[0];
			

			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Fecha">'+response[key].work_order_generation+'</td>';

			if(response[key].work_order_status=="EN PROCESO")
			{
			table +=    '<td data-label="Estado"><div class="tableDropdown2">'+response[key].work_order_status+'</div></td>';}
			else if(response[key].work_order_status=="FINALIZADO")
			{
			table +=    '<td data-label="Estado"><div class="tableDropdown3">'+response[key].work_order_status+'</div></td>';}

			table += 	'<td data-label="Servicios">'+servicesAcum+'</td>';
			table += 	'<td data-label="Diagnosticos">'+diagnosticAcum+'</td>';
			table += 	'<td data-label="Productos">'+productsAcum+'</td>';
			table += 	'<td data-label="Tecnicos">'+technicalAcum+'</td>';
			table += 	'<td data-label="Proyecto">'+response[key].proyect_name+'</td>';
			table += 	'<td class="no-print" data-label="Acciones" >';
			table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
			table +=    	'<ul class="dropdownButtonList">';
			table += 			'<li onclick="consultWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Ver</a></li>';
			if(user!=2)
			{
			table += 			'<li onclick="receiptWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Recibo</a></li>';
			table += 			'<li onclick="editWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+parseInt(response[key].work_order_id, 10)+',\'WorkOrder\')"><a href="#0">Eliminar</a></li>';
			}
			table += 		'</ul>';
			table += 	'</td>';
			table += '</tr>';

		});
		
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});

}


function consultWorkOrder(work_order_id)
{
	
	$("body").find(".save-div.advanced").addClass('hidden');
	var id=work_order_id;
	$("body").find(".breadcumb").append('<li><a href="#">Ver órden de trabajo</a></li>');
	$("body").find(".rangos").removeClass("active");

	$("body").find('section').each(function() {
    $(this).addClass('hidden');
    $("body").find(".fooTable").empty();
  });

	$("body").find(".regConsult").removeClass("hidden");

	var url='application/ajax/listWorkOrder.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'work_order_id='+id,
		success: function(json)
		{
			

			var response = JSON.parse(json);
			$.each(response, function(key, value)
			{

				if(response[key].work_order_id==id)
				{
					
					$("body").find("#busInfo").html("<p>Fono: "+response["0"].business_phone+"</p><p>"+response["0"].business_direction+"</p><p>"+response["0"].business_website+"</p>");
					$("body").find("#newReceiptImg").attr('src','public/img/business/'+response["0"].business_logo);
					var tablePt1="";
					var tablePt2="";
					var tablePt3 ="";
					var tablePt4 ="";
					var tablePt5 ="";
					

					var tablePt1= "<tr><th><p>OT N° "+value.work_order_id+"</p></th><th><p>Fecha: "+value.work_order_generation+"</p></th></tr>";

					
					tablePt2 += "<tr>";
					tablePt2 += 		"<td>Informacion del Cliente</td>";
					tablePt2 += 		"<td>";
					tablePt2 += 			"<p>NOMBRE: "+value.format_business_name+"</p>";
					tablePt2 +=				"<p>RUT: "+value.format_rut+"</p>";
					tablePt2 +=					"<p>MEDIO: "+value.format_business_name+"</p>";
					tablePt2 +=					"<p>DEPARTAMENTO: "+value.format_rut+"</p>";
					tablePt2 +=					"<p>CONTACTO: "+value.branch_office_cost_center+"</p>";
					tablePt2 +=					"<p>FONO: "+value.branch_office_nro+"</p>";
					tablePt2 +=					"<p>EMAIL: "+value.format_direction+"</p>";
					tablePt2 +=					"<p>SOLICITUD: "+value.format_comuna+"</p>";
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += "<tr>";
					tablePt2 += 		"<td>Informacion del Requerimiento</td>";
					tablePt2 += 		"<td>";
					tablePt2 += 			"<p>PRESONA - CONTACTO: "+value.request_r_name+"</p>";
					tablePt2 +=				"<p>FONO: "+value.request_r_phone+"</p>";
					tablePt2 +=				"<p>DEPARTAMENTO: "+value.request_department+"</p>";
					tablePt2 +=				"<p>EMAIL: "+value.request_r_email+"</p>";
					tablePt2 +=				"<p>MEDIO: "+value.request_medium+"</p>";
					tablePt2 +=				"<p>SOLICITUD: "+value.request_app+"</p>";
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += "<tr>";
					tablePt2 += 	"<td>Guia de Entrega</td>";
					tablePt2 += 		"<td>";
					tablePt2 += 			"<p>ENTRADA: "+value.time_input+"</p>";
					tablePt2 +=				"<p>SALIDA: "+value.time_output+"</p>";
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += "<tr>";
					tablePt2 += 	"<td>Proyecto</td>";
					tablePt2 += 		"<td>";
					tablePt2 += 			"<p>"+value.proyect_name+"</p>";
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += 	"<td>Servicios</td>";
					tablePt2 += 		"<td>";
					$.each(response[key].services, function(key2, value2)
					{	
					if(value2!=false)
						{	
							tablePt2 += 			"<p>"+value2.services_name+"</p>";
						}
					else
						{
							tablePt2 += 			"";							
						}
 					});
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += 	"<td>Tecnicos</td>";
					tablePt2 += 		"<td>";
					$.each(response[key].technical, function(key2, value2)
					{		
						if(value2!=false)
						{
							tablePt2 += 			"<p>"+value2.technical_name+"</p>";
						}
						else
						{
							tablePt2 += 			"";							
						}
 					});
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += 	"<td>Productos</td>";
					tablePt2 += 		"<td>";
					$.each(response[key].product, function(key2, value2)
					{	if(value2!=false)
						{	
							tablePt2 += 			"<p>"+value2.product_description+"</p>";
				
 						}
 						else
						{
							tablePt2 += 			"";							
						}
 					});
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += 	"<td>Diagnostico</td>";
					tablePt2 += 		"<td>";
					$.each(response[key].diagnostic, function(key2, value2)
					{	
					if(value2!=false)
						{	
							tablePt2 += 			"<p>"+value2.diagnostic_name+"</p>";
						}
					else
						{
							tablePt2 += 			"";							
						}
 					});
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";

					tablePt2 += "<tr>";
					tablePt2 += 	"<td>Observaciones</td>";
					tablePt2 += 		"<td>";
					tablePt2 += 			"<p>"+value.work_order_observation+"</p>";
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					tablePt2 += "<tr>";
					tablePt2 += 	"<td>CONFORMIDAD</td>";
					tablePt2 += 		"<td>";
					tablePt2 += 			"<p>NOMBRE DEL ENCARGADO:</p>";
					tablePt2 += 			"<p>FIRMA:</p>";
					tablePt2 += 			"<p>RUT:</p>";
					tablePt2 += 			"<p>TIMBRE:</p>";
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";


					tablePt2 += 	"<td>Documentos</td>";
					tablePt2 += 		"<td>";

					
			

						$.each(response[key].document, function(key2, value2)
						{		
								if(value2!=false)
								{
									tablePt2 += "<a target='_blank' href='public/ot/doc/"+value2.document_destination+"'>"+value2.document_destination+"</a>";
								}
								else
								{
									tablePt2 += "<p>No disponibles</p>";
								}
	
					
 						});

										
					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";

					
					tablePt2 += 		"<td colspan='2'>"; 	
					tablePt2 += 	"Imagenes<br><br>";				
 					$.each(response[key].photo, function(key2, value2)
					{		
						if(value2!=false)
						{
							tablePt2 += "<img id='imgPreview2' src='public/ot/img/"+value2.photo_destination+"'>";
						}
						else
						{
									tablePt2 += "<p>No disponibles</p>";
						}
							
 					});
 					tablePt2 += 		"</td>";
					tablePt2 += "</tr>";
					
 					$.each(response[key].video, function(key2, value2)
					{	
						if(value2!=false)
						{
								tablePt4 += '<div class="thumbVideoContainer"><video class="videoThumb"><source src="public/ot/vid/'+value2.video_destination+'#t=15'+'" type="video/mp4"><source src="public/ot/vid/'+value2.video_destination+'#t=15'+'" type="video/ogg"><source src="public/ot/vid/'+value2.video_destination+'#t=15'+'" type="video/webm"><object data="video.mp4" width="470" height="255"><embed src="video.swf" width="470" height="255"></object></video><div class="thumbVideoTitle">'+value2.video_destination+'</div></div>';								//tablePt4 += '<video width="320" height="240" controls><source src="public/ot/vid/"'+value2.video_destination+'" type="video/mp4">';
								//tablePt4 += 'Your browser does not support the video tag.</video>';
							
						}
							
 					});

					
					
					$("body").find("#ot-main-thead").html(tablePt1);
					$("body").find(".regOverview-tbody-table").html(tablePt2);
					
					$("body").find(".otImages").html(tablePt3);
					$("body").find(".otVideos").html(tablePt4);
			
					

					$("body").find("#work_order_generation").val(response[key].work_order_generation);
					$("body").find("#request_r_name").val(response[key].request_r_name);
					$("body").find("#request_r_email").val(response[key].request_r_email);
					$("body").find("#projectList").val(response[key].project_id);
					$("body").find("#work_order_date").val(response[key].work_order_date);
					$("body").find("#time-in").val(response[key].time_input);
					$("body").find("#time-out").val(response[key].time_output);
					$("body").find("#work_order_observation").val(response[key].work_order_observation);
					
				
					$.each(response[key].video, function(key2, value2)
					{
						$("body").find("#previewVideo").append("<a href='public/ot/vid"+value2.video_destination+"'>"+value2.video_destination+"</a>");
					});
					$.each(response[key].photo, function(key2, value2)
					{
						$("body").find("#previewImg").append("<a href='public/ot/img"+value2.photo_destination+"'>"+value2.photo_destination+"</a>");
					});
					$.each(response[key].document, function(key2, value2)
					{
						$("body").find("#previewDoc").append("<a href='public/ot/doc"+value2.document_destination+"'>"+value2.document_destination+"</a>");
					});
					
				}
			});
		}
  	
 	});
}




function editWorkOrder(work_order_id)
{ hideBtn();
	
	$("body").find(".rangos").removeClass("active");
	var id=work_order_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/listWorkOrder.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'work_order_id='+id,
		success: function(json)
		{
			var response = JSON.parse(json);
			$.each(response, function(key, value)
			{
				if(response[key].work_order_id==id)
				{
					$("body").find("#work_order_id").val(id);				
					$("body").find("#work_order_generation").val(response[key].work_order_generation);
					$("body").find("#request_r_name").val(response[key].format_name);
					$("body").find("#request_office_cost_center").val(response[key].branch_office_cost_center);
					//$("body").find("#request_app").val(response[key].request_app);

					$("body").find("#projectList").val(response[key].project_id);

					$.getJSON('application/ajax/listProyect.php', function(response2)
					{	
						$("body").find("#projectList").empty();

						$.each(response2, function(key2, val2) 
						{
							if(response[key].proyect_id == val2.proyect_id)
							{
								$("body").find("#projectList").append(' <option value="'+val2.proyect_id+'" selected>'+val2.proyect_name+'</option>');
							}
							else
							{
								$("body").find("#projectList").append(' <option value="'+val2.proyect_id+'" >'+val2.proyect_name+'</option>');
							}
						});
 					});

					/*$.each(response[key].services, function(key2, value2)
					{
           					var secti="service";
           					$("body").find("#"+secti+"Wrapper").empty();
           					var newEl="";
           					$.getJSON("application/ajax/listServices.php", function(response2)
							{

								newEl +='<div class="dynField">';
           						newEl += 	'<select class="'+secti+'List" name="'+secti+'[]">';
								$.each(response2, function(key3, val3) 
								{
									
									if(value2.services_id==val3.services_id)
									{
										query='<option value="'+val3.services_id+'" selected>'+val3.services_name+'</option>';
									}
									else
									{
										query='<option value="'+val3.services_id+'">'+val3.services_name+'</option>';
									}
									newEl += query;

								});
								newEl +=	'</select>';
           						newEl +=	'<a href="#" class="addField dynButton '+secti+'">Agregar</a>';
           						newEl +='</div>';
								$("body").find("#"+secti+"Wrapper").append(newEl);
 							});
					});*/

					$("#serviceListAdded").empty();
					$.each(response[key].services, function(key2, value2)
					{
						
           					var secti="service";
           					var options="";
           					$.getJSON("application/ajax/listServices.php", function(response2)
							{
					
								$.each(response2, function(key3, val3) 
								{
									options +='<option value="'+val3.services_id+'">'+val3.services_name+'</option>';
								});
								
								$("body").find("#"+secti+"List").html(options);
							 });
if (parseInt(value2.services_id) > 0){$("#"+secti+"ListAdded").append('<div class="dinamycListAddedDiv"><button class="dinamycListAdded" value="'+value2.services_id+'"><input type="hidden" name="'+secti+'[]" value="'+value2.services_id+'"> '+value2.services_name+'</button><span class="deleteDinamicallyListEl"></span></div>');}

					});

					$("#productListAdded").empty();
					$.each(response[key].product, function(key2, value2)
					{
           					var secti="product";
           					var options="";
           					$.getJSON("application/ajax/listProduct.php", function(response2)
							{
								
								$.each(response2, function(key3, val3) 
								{
									options +='<option value="'+val3.product_id+'">'+val3.product_description+'</option>';
								});
								
								$("body").find("#"+secti+"List").html(options);
							 });
if (parseInt(value2.product_id) > 0){$("#"+secti+"ListAdded").append('<div class="dinamycListAddedDiv"><button class="dinamycListAdded" value="'+value2.product_id+'"><input type="hidden" name="'+secti+'[]" value="'+value2.product_id+'"> '+value2.product_description+'</button><span class="deleteDinamicallyListEl"></span></div>');}
					});

					$("#diagnosticListAdded").empty();
					$.each(response[key].diagnostic, function(key2, value2)
					{
						
           					var secti="diagnostic";
           					var options="";
           					$.getJSON("application/ajax/listDiagnostic.php", function(response2)
							{
								
								$.each(response2, function(key3, val3) 
								{
									options +='<option value="'+val3.diagnostic_id+'">'+val3.diagnostic_name+'</option>';
								});
								
								$("body").find("#"+secti+"List").html(options);
							 });
if (parseInt(value2.diagnostic_id) > 0){ $("#"+secti+"ListAdded").append('<div class="dinamycListAddedDiv"><button class="dinamycListAdded" value="'+value2.diagnostic_id+'"><input type="hidden" name="'+secti+'[]" value="'+value2.diagnostic_id+'"> '+value2.diagnostic_name+'</button><span class="deleteDinamicallyListEl"></span></div>');}

					});


					$.each(response[key].technical, function(key2, value2)
					{
           					var secti="technical";
           					var options="";
           					$.getJSON("application/ajax/listTechnical.php", function(response2)
							{
								
								$.each(response2, function(key3, val3) 
								{
									options +='<option value="'+val3.technical_id+'">'+val3.technical_name+'</option>';
									
									
								});
								
								$("body").find("#"+secti+"List").html(options);
							 });
if (parseInt(value2.technical_id) > 0) {$("#"+secti+"ListAdded").append('<div class="dinamycListAddedDiv"><button class="dinamycListAdded" value="'+value2.technical_id+'"><input type="hidden" name="'+secti+'[]" value="'+value2.technical_id+'"> '+value2.technical_name+'</button><span class="deleteDinamicallyListEl"></span></div>');}
				});
					
					$("body").find("#work_order_date").val(response[key].work_order_date);
					$("body").find("#time-in").val(response[key].time_input);
					$("body").find("#time-out").val(response[key].time_output);
					$("body").find("#work_order_observation").val(response[key].work_order_observation);
					
					$.each(response[key].video, function(key2, value2)
					{
						if(value2!=false)
						{				
							$("body").find(".videoWrapper").append('<div class="thumbVideoContainer"><video class="videoThumb"><source src="public/ot/vid/'+value2.video_destination+'#t=15'+'" type="video/mp4"><source src="public/ot/vid/'+value2.video_destination+'#t=15'+'" type="video/ogg"><source src="public/ot/vid/'+value2.video_destination+'#t=15'+'" type="video/webm"><object data="video.mp4" width="470" height="255"><embed src="video.swf" width="470" height="255"></object></video><div class="thumbVideoTitle">'+value2.video_destination+'</div><input type="hidden" id="nonDeleteVideo" name="nonDeleteVideo[]" value="'+value2.video_id+'"><div class="removeVid">x</div></div>');	
						}
					});
					$.each(response[key].photo, function(key2, value2)
					{
						if(value2!=false)
						{
						$("body").find(".imgWrapper").prepend('<div class="imgDiv"><p class="newImg">+</p><img id="imgPreview" style="z-index: 0; object-fit: cover; width: 100%; height: 100%;" src="public/ot/img/'+value2.photo_destination+'"><div class="removeImg active">x</div><input type="hidden" name="nonDeleteImg[]" value="'+value2.photo_id+'"></></div>');
						}
					});
					$.each(response[key].document, function(key2, value2)
					{
						if (parseInt(value2.document_id) > 0) $("body").find(".documentWrapper").append("<div class='documentEL'><a class='documentAdded' href='public/ot/doc/"+value2.document_destination+"'>"+value2.document_destination+"&nbsp&nbsp&nbsp</a><button class='deleteDocument xButton' value='x'><span class='xSymbol'></span></button><input type='hidden' name='nonDeleteDocument[]' value='"+value2.document_id+"'></div>");
					});
					
				}
			});
			$("body").find(".save-div.advanced").addClass('hidden');
		}
  	
 	});
}

function receiptWorkOrder(id)
{
	var id=id;

	$(function()
	{
		loadView().done(function()
		{
			loadData().done();
		});
	});



function loadView()
{
	var dfrd1 = $.Deferred();
	setTimeout(() => {
		$( "#content" ).load( "public/views/receipt.html" );
		$(".options").removeClass("active");
		$(".Receipt").addClass("active");
		dfrd1.resolve();
	}, 0);
	
	return $.when(dfrd1).done().promise();
}
function loadData()
{
	var dfrd1 = $.Deferred();
	
/*		$(".fooTable").toggleClass("hidden");
		$(".add").toggleClass("hidden");*/
		var url='application/ajax/showWorkOrderByCode.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'work_order_id='+id,
			success: function(response)
			{
				var serviceListPay="";
				var data = JSON.parse(response);
				console.log(data["0"].request_r_name);
				console.log(data);
				$("body").find("#receiptName").val(data["0"].request_r_name);
				$("body").find("#receiptBus").val(data["0"].business_id);
				$("body").find("#newReceiptImg").attr("src", 'public/img/business/'+data["0"].business_logo);
				$("body").find("#receiptObs").val(data["0"].work_order_observation);
				$.each(data.services, function(key2, value2)
				{		
					serviceListPay += value2.services_name;
					serviceListPay += ", ";
				});
				serviceListPay = serviceListPay.substring(0,serviceListPay.length - 2);
				$("body").find("#receiptService").val(serviceListPay);
  			}
		 });
	dfrd1.resolve();
	return dfrd1.promise();
}

}
			
function tableService()
{
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 

	$('#loading-div2').addClass("active"); 
	$.getJSON('application/ajax/listServices.php', function(response)
	{
		var table;
		$('#services-content-table-tbody').empty();
		$.each(response, function(key, val) 
		{
			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Nombre">'+val.services_name+'</td>';
			table += 	'<td class="no-print" data-label="Acciones" >';
			table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
			table +=    	'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editServices('+val.services_id+')"><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.services_id+',\'Service\')"><a href="#0">Eliminar</a></li>';
			table += 		'</ul>';
			table += 	'</td>';
			table += '</tr>';
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});
}


function insertService()
{
	var url='application/ajax/writeService.php';
	console.log($('#new-serv-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-serv-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}



function showServicesListByCode(services_id)
{
	var id=services_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showServicesListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'services_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
		
  		}
 	});
}



function editServices(services_id)
{ hideBtn();
	var id=services_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showServicesListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'services_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#services_id").val(data[0]["services_id"]);
			$("body").find("#services_name").val(data[0]["services_name"]);

  		}
 	});
}

/*------------------------------------------------------------------*/


function insertDiagnostic()
{
	var url='application/ajax/writeDiagnostic.php';
	console.log($('#new-diag-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-diag-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}

function tableDiagnostic()
{
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 
	$.getJSON('application/ajax/listDiagnostic.php', function(response)
	{
		var table;
		$('#diagnostic-content-table-tbody').empty();
		$.each(response, function(key, val) 
		{
			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Nombre">'+val.diagnostic_name+'</td>';
			table += 	'<td class="no-print" data-label="Acciones" >';
			table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
			table +=    	'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editDiagnostic('+val.diagnostic_id+')  "><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.diagnostic_id+',\'Diagnostic\')"><a href="#0">Eliminar</a></li>';
			table += 		'</ul>';
			table += 	'</td>';
			table += '</tr>';
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});
	
}

function showDiagnosticListByCode(diagnostic_id)
{
	var id=diagnostic_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showDiagnosticListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'diagnostic_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
		
  		}
 	});
}


function editDiagnostic(diagnostic_id)
{ hideBtn();
	var id=diagnostic_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showDiagnosticListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'diagnostic_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#diagnostic_id").val(data[0]["diagnostic_id"]);
			$("body").find("#diagnostic_name").val(data[0]["diagnostic_name"]);
  		}
 	});
}


function waitForTableCreate()
{
	$('#loading-div2').addClass("active"); 
	$(".main-tbody-table").bind("DOMNodeInserted",function(){
		if($(".main-tbody-table > *").attr('class') =="odd")
		{
			
			setTimeout(() => {$('#loading-div2').removeClass("active"); }, 0);
		}
	});
}



/*--------------------------------------------------------------------------*/
function tableTechnical()
{
        waitForTableCreate();

	
	$.getJSON('application/ajax/listTechnical.php', function(response)
	{
		$('#technical-content-table-tbody').empty();
		var table;
		$.each(response, function(key, val) 
		{

				table += '<tr>';
				table += 	'<td class="t-trigger" data-label="Nombre">'+val.technical_name+'</td>';
				table += 	'<td data-label="RUT" >'+val.technical_rut+'</td>';
				table += 	'<td data-label="Cargo" >'+val.technical_position+'</td>';
				table += 	'<td data-label="Fono" >'+val.technical_phone+'</td>';
				table += 	'<td data-label="Email" >'+val.technical_email+'</td>';
				table += 	'<td class="no-print">';
				table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
				table += 			'<ul class="dropdownButtonList">';
				table += 				'<li onclick="editTechnical('+val.technical_id+')  "><a href="#0">Editar</a></li>';
				table += 				'<li onclick="deleteElement('+val.technical_id+',\'Technical\')"><a href="#0">Eliminar</a></li>';
				table += 			'</ul>';
				table += 	'</td>';
				table += '</tr>';
		});
	
			$.when($('.main-tbody-table').append(table)).done(createDataTable());
			return false;

		
	});
}

function insertTechnical()
{
	var url='application/ajax/writeTechnical.php';
	console.log($('#new-tec-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-tec-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}

function showTechnicalListByCode(technical_id)
{
	var id=technical_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showTechnicalListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'technical_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
		
  		}
 	});
}



function editTechnical(technical_id)
{ hideBtn();
	var id=technical_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showTechnicalListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'technical_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#technical_id").val(data[0]["technical_id"]);
			$("body").find("#technical_name").val(data[0]["technical_name"]);
			$("body").find("#technical_rut").val(data[0]['technical_rut']);
			$("body").find("#technical_position").val(data[0]['technical_position']);
			$("body").find("#technical_phone").val(data[0]['technical_phone']);
			$("body").find("#technical_email").val(data[0]['technical_email']);
  		}
 	});
}



/*----------------------------------------------------------------------------------*/

function insertProduct()
{
	var url='application/ajax/writeProduct.php';
	console.log($('#new-product-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-product-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}



function tableProduct()
{
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 
	var table="";
	$.getJSON('application/ajax/listProduct.php', function(response)
	{
		$('#product-content-table-tbody').empty();
		
		
		$.each(response, function(key, val) 
		{
			
			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Empresa">'+val.product_business+'</td>';
			table += 	'<td data-label="Descripcion" >'+val.product_description+'</td>';
			table += 	'<td data-label="Costo" >'+val.product_cost+'</td>';
			table +=	'<td data-label="Acciones" class="no-print">';
			table +=		'<button class="tableDropdown dropdowns">Aciones</button>';
			table += 		'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editProduct('+val.product_id+')"  ><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.product_id+',\'Product\')"><a href="#0">Eliminar</a></li>';
			table += 		'</ul>';
			table +=	'</td>';
			table += '</tr>';
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	})
	
	
}


function showProductListByCode(product_id)
{
	var id=product_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showProductListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'product_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
		
  		}
 	});
}



function editProduct(product_id)
{ hideBtn();
	var id=product_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showProductListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'product_id='+id,
		success: function(response)
		{
			console.log(response);
			var data = JSON.parse(response);
			var business;
			if(data[0]["product_business"]=="TECNOSEC"){business=1};
			if(data[0]["product_business"]=="INFRAGANTI"){business=2};
			if(data[0]["product_business"]=="GLOBAL BUSINESS"){business=3};
			$("body").find("#product_id").val(data[0]["product_id"]);
			$("body").find("#product_business").val(business);
			$("body").find("#product_description").val(data[0]['product_description']);
			$("body").find("#product_cost").val(data[0]['product_cost']);
  		}
 	});
}



/*------------------------------------------------------------*/




function tableBranchOffice()
{
	userType();
	$("body").find('#format_name_list').empty();
	var url='application/ajax/listFormat.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'',
		dataType:'json',
		success: function(response)
		{
			$.each(response, function(key, val) 
			{
				$('#format_name_list').append(' <option value="'+val.format_name+'"></option>');
			});
			return false;
		  }
	 });
	

	 $('#loading-div2').addClass("active"); 
	 var element=$("body").find('.main-tbody-table');
	 var existCondition = setInterval(function() {
		 if (element.children().length > 0) {
			clearInterval(existCondition);
			$('#loading-div2').removeClass("active"); 
		 }
		}, 100); 
	
	$.getJSON('application/ajax/listBranchOffice.php', function(response)
	{
		$('#product-content-table-tbody').empty();
		var table;
		$.each(response, function(key, val) 
		{
			table +='<tr><td class="t-trigger" data-label="CODIGO">'+val.branch_office_code+'</td>';
			table +=	'<td data-label="CENTRO DE COSTO">'+val.branch_office_cost_center+'</td>';
			table +=	'<td data-label="Nº LOCAL">'+val.branch_office_nro+'</td>';
			table +=	'<td data-label="GERENTE DE TIENDA">'+val.branch_office_gerent_name+'</td>';
			table +=	'<td data-label="EMAIL">'+val.branch_office_gerent_email+'</td>';
			table +=	'<td data-label="FONO">'+val.branch_office_gerent_phone+'</td>';
			table +=	'<td data-label="JEFE DE PREVENCIÓN DE PÉRDIDAS">'+val.branch_office_boss_name+'</td>';
			table +=	'<td data-label="EMAIL">'+val.branch_office_boss_email+'</td>';
			table +=	'<td data-label="FONO">'+val.branch_office_boss_phone+'</td>';
		
			if(user!=2)
			{
			table +=	'<td data-label="Acciones" class="no-print">';
			table +=		'<button class="tableDropdown dropdowns">Aciones</button>';
			table += 		'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editBranchOffice('+val.branch_office_id+')  "><a href="#0">Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.branch_office_id+',\'BranchOffice\')"><a href="#0">Eliminar</a></li>';
			table += 		'</ul>';
			table +=	'</td>';
			}
			else{
			table +=	'<td data-label="Acciones">No disponibles</td>';
			}
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});
}



function editBranchOffice(branch_office_id)
{ hideBtn();
  $("#format_name_search").attr("disabled","disabled");
	var id=branch_office_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showBranchOfficeListByCode.php';
	var url2='application/ajax/showFormatListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'branch_office_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			var format_id=data[0]["format_id"];
			var format_name;
			var format_rut;
			$.ajax(
			{
				type:'POST',
				url:url2,
				data:'format_id='+format_id,
				success: function(response2)
				{
					var data2 = JSON.parse(response2);
					$("body").find("#branch_office_id").val(data[0]["branch_office_id"]);
					$("body").find("#format_name_search").val(data2[0]["format_name"]);
					$("body").find("#format_rut_new").val(data2[0]['format_rut']);
					$("body").find("#branch_office_id").val(data[0]["branch_office_id"]);
					$("body").find("#branch_office_cost_center").val(data[0]['branch_office_cost_center']);
					$("body").find("#branch_office_nro").val(data[0]['branch_office_nro']);
					$("body").find("#branch_office_gerent_name").val(data[0]['branch_office_gerent_name']);
					$("body").find("#branch_office_gerent_email").val(data[0]['branch_office_gerent_email']);
					$("body").find("#branch_office_code").val(data[0]['branch_office_code']);
					$("body").find("#branch_office_gerent_phone").val(data[0]['branch_office_gerent_phone']);
					$("body").find("#branch_office_boss_name").val(data[0]['branch_office_boss_name']);
					$("body").find("#branch_office_boss_email").val(data[0]['branch_office_boss_email']);
					$("body").find("#branch_office_boss_phone").val(data[0]['branch_office_boss_phone']);
  				}
 			});
  		}
 	});
}



function insertBranchOffice()
{
	var url='application/ajax/writeBranchOffice.php';
	console.log($('#new-branch_office-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-branch_office-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}

function showBranchOfficeListByCode(branch_office_id)
{
	var id=branch_office_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showBranchOfficeListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'branch_office_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
  		}
 	});
}


/*------------------------------------------------------------------*/


function insertRequest()
{
	var url='application/ajax/writeRequest.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-req-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}


/*---------------------------------------------------------------------*/

$("body").on("click",".req_status",function(e)
{
	var request_id=$(this).attr("id");
	var url='application/ajax/statusRequest.php';
	$(this).attr("title","");
	$(this).text("VERIFICADO");
	$(this).removeClass("tableDropdown22").addClass("tableDropdown33");
	//var url='application/ajax/cambiaraabb.php';
	var request_status=$(this).attr("role");
	var data = {
   				"request_id": request_id,
   				"request_status": request_status
					};

					console.log(data);
	$.ajax(
	{
		type:'POST',
		dataType:"json",
		url:url,
		data: { data: JSON.stringify(data) },
		success: function(response)
		{
			
  		}
	 });
	
	
 	return false;
})


function tableRequest()
{
	userType();
	$("body").find('#branch_office_code_list').empty();
	var url='application/ajax/listBranchOffice.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'',
		dataType:'json',
		success: function(response)
		{
			$.each(response, function(key, val) 
			{
				$('#branch_office_code_list').append(' <option value="'+val.branch_office_code+'"></option>');
			});
			return false;
		  }
	 });
	 
	$.getJSON('application/ajax/checkUser.php', function(response) 
		{
			if(response==0)
			{		
				var s="";
				$.getJSON('application/ajax/listBusiness.php', function(response2)
				{
					
					$.each(response2, function(key2, val2) 
					{						
						s += '<option value="'+val2.business_id+'">'+val2.business_name+'</option>';
					});
					$("body").find("#business_id").html(s);
				});
				
				
				$("body").find("#business_id").attr("disabled",false);
				$("body").find("#business_id").show();
				$("body").find(".is-admin").show();
				

				$("body").find("#business_din_cont").attr("disabled",true);
				$("body").find("#business_din_cont").hide()
				
			}
			else
			{
				$("body").find("#business_id").attr("disabled",true);
				$("body").find("#business_id").hide();
				$("body").find(".is-admin").hide();

				$("body").find("#business_din_cont").attr("disabled",false);
				$("body").find("#business_din_cont").show();

			}
			
		});

		$('#loading-div2').addClass("active"); 
		var element=$("body").find('.main-tbody-table');
		var existCondition = setInterval(function() {
			if (element.children().length > 0) {
			   clearInterval(existCondition);
			   $('#loading-div2').removeClass("active"); 
			}
		   }, 100); 

	$.getJSON('application/ajax/listAllTables.php', function(response) 
	{    
		var table; 
        $.each(response, function(key, val) 
		{
			if(val.request_id>0)
			{
			console.log(val.request_status);
			if(val.request_status==0)
			{
				var selectedAct="selected";
				var selectedInac="";
			}
			if(val.request_status==1)
			{
				var selectedAct="";
				var selectedInac="selected";
			}

			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="REFERENCIA">'+val.branch_office_code+'</td>'
			table +=	'<td nowrap" data-label="FECHA">'+val.request_date+'</td>';
			table += 	'<td data-label="DEPARTAMENTO">'+val.request_department+'</td>'
			table +=	'<td data-label="MEDIO">'+val.request_medium+'</td>';
			table +=	'<td data-label="NOMBRE">'+val.request_r_name+'</td>';
			table +=	'<td data-label="FONO">'+val.request_r_phone+'</td>';
			table +=	'<td data-label="EMAIL">'+val.request_r_email+'</td>';


			var verifiButtonClass="";
			if(user==0){verifiButtonClass='req_status" title="Click para verificar';}
			else{verifiButtonClass="";}

			if(val.request_status==0)
			{
			table +=    '<td data-label="Estado"><div role="1" id="'+val.request_id+'" class="tableDropdown22 '+verifiButtonClass+'">NO VERIFICADO</div></td>';}
			else if(val.request_status==1)
			{
			table +=    '<td data-label="Estado"><div class="tableDropdown33">VERIFICADO</div></td>';}


			table +=	'<td data-label="SOLICITUD">'+val.request_app+'</td>';
			table += 	'<td class="no-print">';
			table += 		'<button class="tableDropdown dropdowns">Órdenes</button>';
			table += 		'<ul class="dropdownButtonList">';
			if(user!=2)
			{
			table += 			'<li  onclick="newOT('+val.request_id+')  "><a href="#0">Agregar OT</a></li>';
			}
			table += 			'<li  onclick="showOT('+val.request_id+')"><a href="#0">Mostrar OTs</a></li>';
			table += 		'</ul>';
			table += 	'</td>';
			table += 	'<td class="no-print">';
			if(user!=2)
			{
				table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
				table += 		'<ul class="dropdownButtonList">';
				table += 			'<li  onclick="editRequest('+val.request_id+')  "><a href="#0">Editar</a></li>';
				table += 			'<li onclick="deleteElement('+val.request_id+',\'Request\')"><a href="#0">Eliminar</a></li>';
				table += 		'</ul>';
			}
			else
			{
				table += "No disponibles"
			}
			table += 	'</td>';
			table += '</tr>';
			}	

		});
		$('#request-content-table-tbody').append(table);
		var oTable = $('#req-content-table').DataTable();

		$("body").find("#search-input").on("keyup", function(){
			  oTable.search($(this).val()).draw() ;
		})
		
    }); 

 }
function showOT()
{
	$("body").find(".rangos").removeClass("active");
}


function editRequest(request_id)
{ hideBtn();
  $("#branch_office_code_search").attr("disabled","disabled");
	var id=request_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url2='application/ajax/showBranchOfficeListByCode.php';
	var url='application/ajax/showRequestListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'request_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			var branch_office_id=data[0]["branch_office_id"];
			$.ajax(
			{
				type:'POST',
				url:url2,
				data:'branch_office_id='+branch_office_id,
				success: function(response2)
				{
					var data2 = JSON.parse(response2);
					$("body").find("#request_id").val(request_id);

					$("body").find("#branch_office_code_search").val(data2[0]["branch_office_code"]);
					$("body").find("#branch_office_cost_center").val(data2[0]['branch_office_cost_center']);
					
					$("body").find("#branch_office_id").val(data2[0]['branch_office_id']);

					$("body").find("#business_id").val(data["0"].business_id);

					$("body").find("#request_medium").val($("#request_medium" + " option").filter(function() { return this.text == data[0]["request_medium"] }).val());
					
					$("body").find("#request_department").val(data[0]['request_department']);
					$("body").find("#request_r_name").val(data[0]['request_r_name']);
					$("body").find("#request_r_phone").val(data[0]['request_r_phone']);
					$("body").find("#request_r_email").val(data[0]['request_r_email']);
					$("body").find("#request_app").val(data[0]['request_app']);
  				}
 			});
  		}
 	});
}


function showRequestListByCode(request_id)
{
	var id=request_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/ShowListAll.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'request_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			console.log(data);
  		}
 	});
}






/*-------------------------------USER-------------------------------------------*/


$("body").on("change",".user_active",function(e)
{
	var id_user=$(this).attr("id");
	var url='application/ajax/statusUser.php';
	var user_active=$(this).val();
	var data = {
   				"id_user": id_user,
   				"user_active": user_active
					};
	$.ajax(
	{
		type:'POST',
		dataType:"json",
		url:url,
		data: { data: JSON.stringify(data) },
		success: function(response)
		{
  		}
 	});
 	return false;
})



function tableUser()
{
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 
	$.getJSON('application/ajax/listUser.php', function(response)
	{
		console.log(response);
		var table;
		$('#user-content-table-tbody').empty();
		$.each(response, function(key, val) 
		{
			console.log(val.user_active);
			if(val.user_active==1)
			{
				var selectedAct="selected";
				var selectedInac="";
			}
			if(val.user_active==0)
			{
				var selectedAct="";
				var selectedInac="selected";
			}
			var bs="";
			if(val.user_business==0)bs="TECNOSEC";
			if(val.user_business==1)bs="INFRAGANTI";
			if(val.user_business==2)bs="GLOBALBUSINESS";
			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Nombre">'+val.user_name+'</td>';
			table += 	'<td data-label="Email">'+val.user_email+'</td>';
			table += 	'<td data-label="RUT">'+val.user_rut+'</td>';
			table += 	'<td data-label="Empresa">'+bs+'</td>';
			table += 	'<td data-label="Estado">';
			table +=	 	'<select class="user_active" name="user_active" id="'+val.id_user+'">';
			table +=				'<option value="1" '+selectedAct+'>Activo</option>';
			table +=				'<option value="0" '+selectedInac+'>Inactivo</option>';
			table +=		'</select>';
			table +=	'</td>';
			table += 	'<td class="no-print">';
			table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
			table += 		'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editUser('+val.id_user+')"  ><a href="#0"   >Editar</a></li>';
			table += 			'<li onclick="deleteElement('+val.id_user+',\'User\')"><a href="#0">Eliminar</a></li>';
			table += 		'</ul>';
			table += 	'</td>';
			table += '</tr>';
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	});
}



function insertUser()
{
	var url='application/ajax/writeUser.php';
	//console.log($('#new-proyect-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-user-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}

function showUserListByCode(id_user)
{
	var id=id_user;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showUserListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'id_user='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
  		}
 	});
}

function editUser(id_user)
{ hideBtn();
	var id=id_user;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showUserListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'id_user='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#id_user").val(data[0]["id_user"]);
			$("body").find("#user_name").val(data[0]["user_name"]);
			$("body").find("#user_email").val(data[0]['user_email']);
			$("body").find("#user_rut").val(data[0]['user_rut']);

			$("body").find("#user_business").val(data[0]['user_business']);

			if(data[0]['user_business'] == "TECNOSEC")$("#user_business").val("1");
			if(data[0]['user_business'] == "INFRAGANTI")$("#user_business").val("2");
			if(data[0]['user_business'] == "GLOBAL BUSINESS")$("#user_business").val("3");
  		}
 	});
}






function showMsg(type,message)
{
	$("body").find(".message-box").addClass(type);
	$("body").find(".message").html("<strong>¡"+type+"!</strong> "+message);
	window.setTimeout(function()
	{
  		$("body").find(".message-box").removeClass(type);
	}, 2000);
	return false;
}









jQuery(document).ready(function()
{



	var url='application/ajax/checkUser.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'',
		success: function(response)
		{
			if(response==1)
			{
				console.log("yes");
				$("body").find(".onlyAdmin").hide();
			}
			
  		}
 	});


 

    $("body").on("click",".addField", function(e)
    {
        e.preventDefault();
        	$(this).removeClass("addField").addClass("rmField");
        	$(this).html("Eliminar");
        	if($(this).hasClass("diagnostic")){var secti="diagnostic";}
        	if($(this).hasClass("service")){var secti="service";}
        	if($(this).hasClass("technical")){var secti="technical";}
        	if($(this).hasClass("product")){var secti="product";}
        	console.log(secti);
        	var newEl = "";
        	var query;
        	if(secti=='service'){url="application/ajax/listServices.php";}
        	if(secti=='technical'){url="application/ajax/listTechnical.php";}
        	if(secti=='product'){url="application/ajax/listProduct.php";}
        	if(secti=='diagnostic'){url="application/ajax/listDiagnostic.php";}


           	$.getJSON(url, function(response)
			{
				newEl +='<div class="dynField">';
           		newEl += 	'<select class="'+secti+'List" name="'+secti+'[]">';
				$.each(response, function(key, val) 
				{
					if(secti=='service'){query='<option value="'+val.services_id+'">'+val.services_name+'</option>';}
					if(secti=='technical'){query='<option value="'+val.technical_id+'">'+val.technical_name+'</option>';}
					if(secti=='product'){query='<option value="'+val.product_id+'">'+val.product_description+'</option>';}
					if(secti=='diagnostic'){query='<option value="'+val.diagnostic_id+'">'+val.diagnostic_name+'</option>';}
					newEl += query;
				});
				newEl +=	'</select>';
           		newEl +=	'<a href="#" class="addField dynButton '+secti+'">Agregar</a>';
           		newEl +='</div>';
				$("#"+secti+"Wrapper").append(newEl);
 			});
        })

    $("body").on("click",".rmField", function(e)
    {
    	 e.preventDefault(); $(this).parent('div').remove();
     })

    







/*$("body").on("focus hover",".serviceList", function()
	{
		var thisEl = this;
		$(this).empty();
		var url='application/ajax/listServices.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			context: this,
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$(thisEl).append(' <option value="'+val.services_id+'">'+val.services_name+'</option>');
				});
				return false;
  			}
 		});
 		return false;

	});



$("body").on("focus hover",".technicalList", function()
	{
		var thisEl = this;
		$(this).empty();
		var url='application/ajax/listTechnical.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			context: this,
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$(thisEl).append(' <option value="'+val.technical_id+'">'+val.technical_name+'</option>');
				});
				return false;
  			}
 		});
 		return false;

	});




$("body").on("focus hover",".productList", function()
	{
		var thisEl = this;
		$(this).empty();
		var url='application/ajax/listProduct.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			context: this,
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$(thisEl).append(' <option value="'+val.product_id+'">'+val.product_description+'</option>');
				});
				return false;
  			}
 		});
 		return false;

	});


$("body").on("focus hover",".diagnosticList", function()
	{
		var thisEl = this;
		$(this).empty();
		var url='application/ajax/listDiagnostic.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			context: this,
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$(thisEl).append(' <option value="'+val.diagnostic_id+'">'+val.diagnostic_name+'</option>');
				});
				return false;
  			}
 		});
 		return false;
 		
	});


$("body").on("focus hover",".projectList", function()
	{
		var thisEl = this;
		$(this).empty();
		var url='application/ajax/listProyect.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			context: this,
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$(thisEl).append(' <option value="'+val.proyect_id+'">'+val.proyect_description+'</option>');
				});
				return false;
  			}
 		});
 		return false;
 		
	});
*/

/*
	$("body").on("focus","#format_name_search", function()
	{	
		$("body").find('#format_name_list').empty();
		var url='application/ajax/listFormat.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$('#format_name_list').append(' <option value="'+val.format_name+'"></option>');
				});
				return false;
  			}
 		});
 		return false;
	});*/

/*
	$("body").on("focus","#branch_office_code_search", function()
	{	
		$("body").find('#branch_office_code_list').empty();
		var url='application/ajax/listBranchOffice.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'',
			dataType:'json',
			success: function(response)
			{
				$.each(response, function(key, val) 
				{
					$('#branch_office_code_list').append(' <option value="'+val.branch_office_code+'"></option>');
				});
				return false;
  			}
 		});
 		return false;
	});*/
});



function searchCostCenterName()
{

	var code = $("body").find("#branch_office_code_search").val();
	var url='application/ajax/showBranchOfficeListByName.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'branch_office_code='+code,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#branch_office_cost_center").val(data[0]['branch_office_cost_center']); 
			$("body").find("#branch_office_id").val(data[0]['branch_office_id']); 
			return false;
  		}
 	});
 	return false;
}

function searchFormatName()
{

	var format_name = $("body").find("#format_name_search").val();
	var url='application/ajax/showFormatListByName.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'format_name='+format_name,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#format_rut_new").val(data[0]['format_rut']); 
			$("body").find("#format_id_new").val(data[0]['format_id']); 
			return false;
  		}
 	});
 	return false;
}

function insertOrder()
{
	var form = $('#new-ot-form')[0];
	 var data = new FormData(form);
	var url='application/ajax/writeWorkOrder.php';
	$.ajax(
	{
		  	type: "POST",
            enctype: 'multipart/form-data',
            url: url,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
			success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}



function newOT(request_id)
{
	
$("body").find(".rangos").removeClass("active");
	var id=request_id;
	
var url='application/ajax/showRequestListByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'request_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			
			if (data[0]['request_status'] == "0") {

        showMsg("Error","El requerimiento no ha sido verificado");
        return;}
        else
        {
        	$('#content').load('public/views/workorder.html', function() {
$("body").find("#request_r_name").val(data[0]['request_r_name']); 
        	$("body").find(".fooTable").addClass("hidden");
	$("body").find(".add").removeClass("hidden");
	$("body").find("#new-ot-form").addClass("visible");
	$("body").scrollTop(0);
	$("body").find("#request_id").val(id);


var d = new Date();
console.log(d);
$("body").find("#work_order_generation").val(d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate());


			$("body").find("#request_id").val(data[0]['request_id']); 
			var id2=data[0]['branch_office_id'];
			var url='application/ajax/showBranchOfficeListByCode.php';
			$.ajax(
			{
				type:'POST',
				url:url,
				data:'branch_office_id='+id2,
				success: function(response2)
				{
					var data2 = JSON.parse(response2);
					console.log(data2);
					$("body").find("#request_office_cost_center").val(data2["0"].branch_office_cost_center); 
				}
			});

			$.getJSON('application/ajax/listProyect.php', function(response)
			{	
				$("body").find("#projectList").empty();
				$.each(response, function(key, val) 
				{
					$("body").find("#projectList").append(' <option value="'+val.proyect_id+'">'+val.proyect_name+'</option>');
				});
				$("body").find("#projectList").append(' <option value="0" disabled selected hidden>Seleccione</option>');
				return false;
 			});

			$.getJSON('application/ajax/listServices.php', function(response)
			{	
				$("body").find("#serviceList").empty();
				
				$.each(response, function(key, val) 
				{
					$("body").find("#serviceList").append(' <option value="'+val.services_id+'">'+val.services_name+'</option>');
				});
				$("body").find("#serviceList").append(' <option value="0" disabled selected hidden>Seleccione</option>');
				return false;
 			});

 			$.getJSON('application/ajax/listDiagnostic.php', function(response)
			{	
				$("body").find("#diagnosticList").empty();
				
				$.each(response, function(key, val) 
				{
					$("body").find("#diagnosticList").append(' <option value="'+val.diagnostic_id+'">'+val.diagnostic_name+'</option>');
				});
				$("body").find("#diagnosticList").append(' <option value="0" disabled selected hidden>Seleccione</option>');
				return false;
 			});
 			$.getJSON('application/ajax/listTechnical.php', function(response)
			{	
				$("body").find("#technicalList").empty();
				
				$.each(response, function(key, val) 
				{
					$("body").find("#technicalList").append(' <option value="'+val.technical_id+'">'+val.technical_name+'</option>');
				});
				$("body").find("#technicalList").append(' <option value="0" disabled selected hidden>Seleccione</option>');
				return false;
 			});
 			$.getJSON('application/ajax/listProduct.php', function(response)
			{	
				$("body").find("#productList").empty();
				
				$.each(response, function(key, val) 
				{
					$("body").find("#productList").append(' <option value="'+val.product_id+'">'+val.product_description+'</option>');
				});
				$("body").find("#productList").append(' <option value="0" disabled selected hidden>Seleccione</option>');
				return false;
 			});

			 $("body").find(".breadcumb").html('<li><a href="index.php">Home</a></li><li class="WorkOrder optionsDir"><a class="load-content" href="workorder.html">Órdenes de Trabajo</a></li><li><a href="index.php">Agregar</a></li>');
			 $(".options").removeClass("active");
			 $(".WorkOrder").addClass("active");
			 $("body").find(".print-p").addClass("hidden");
			 $("body").find(".save-reg-p").removeClass("hidden");
			 $("body").find(".save-div.advanced").addClass('hidden');
			return false;});
  		}}
 	});




	
	
}


function isValidDate(text)
{ 
	var comp = text.split('/');
	if(parseInt(comp[0], 10)>3000)
	{
		
		showMsg("Error","La fecha posee errores.")
		return "error";
	}

};





function insertOt()
{
	if(isValidDate($("body").find("#work_order_date").val()) == "error")
	{
		
		return false;
	}
	else
	{
	var url='application/ajax/writeWorkOrder.php';
	var form = $('#new-ot-form')[0];
	var data = new FormData(form);
	
	//console.log($('#new-proyect-form').serialize());
	$.ajax(
	{
		type:'POST',
		enctype: 'multipart/form-data',
		url:url,
		data:data,
		processData: false,
        contentType: false,
        cache: false,

		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
	 return false;
}
}


function changePassword()
{
	var url='application/ajax/changePasswordUsers.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#pass-form').serialize(),
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}


function tableBusiness()
{
	$('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 
	var table="";
	$.getJSON('application/ajax/listBusiness.php', function(response)
	{
		$('#company-content-table-tbody').empty();
		
		console.log(response);
		$.each(response, function(key, val) 
		{
			
			table += '<tr>';
			table += 	'<td class="t-trigger" data-label="Nombre">'+val.business_name+'</td>';
			table += 	'<td data-label="RUT" >'+val.business_rut+'</td>';
			table += 	'<td data-label="FONO" >'+val.business_phone+'</td>';
			table +=	'<td data-label="Acciones" class="no-print">';
			table +=		'<button class="tableDropdown dropdowns">Aciones</button>';
			table += 		'<ul class="dropdownButtonList">';
			table += 			'<li onclick="editCompany('+val.business_id+')"  ><a href="#0">Editar</a></li>';
			table += 		'</ul>';
			table +=	'</td>';
			table += '</tr>';
		});
		$.when($('.main-tbody-table').append(table)).done(createDataTable());
		return false;
	})
}


function editCompany(business_id)
{ hideBtn();
	var business_id=business_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showBusinessListCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'business_id='+business_id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#business_id").val(data[0]["business_id"]);
			$("body").find("#business_name").val(data[0]["business_name"]);
			$("body").find("#business_rut").val(data[0]['business_rut']);
			$("body").find("#business_direction").val(data[0]['business_direction']);
			$("body").find("#business_phone").val(data[0]['business_phone']);
			$("body").find("#business_website").val(data[0]['business_website']);
			$("body").find("#business_description").val(data[0]['business_description']);
			$("body").find("#company-logo-prv").attr("src","public/img/business/"+data[0]['business_logo']);

  		}
 	});
}



function tableHome()
{
  var table="";
	table += '<tr>';
			table += 	'<td colspan="6">No existen verificaciones</td>';
			table += '</tr>';
  $('#main-content-table-tbody').append(table);
  console.log("a");
	var element=$("body").find('#main-content-table-tbody');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
	   }, 100); 
	var table="";
   
	$.getJSON('application/ajax/listVerification.php', function(response)
	{
      console.log(response);
		if (response==false)
		{
			table += '<tr>';
			table += 	'<td colspan="6">No existen verificaciones</td>';
			table += '</tr>';
		}
		$('#main-content-table-tbody').empty();
		
		
		$.each(response, function(key, val) 
		{
	
			table += '<tr>';
			table += 	'<td class="collapseCa">'+val.v_request_date+'</td>';
			table += 	'<td class="collapseCa">'+val.v_request_responsable+'</td>';
			table += 	'<td class="collapseCa">'+val.v_request_responsable_rut+'</td>';
			table += 	'<td class="collapseCa">'+val.request_app+'</td>';
			if(val.request_status==0)
			{
			table += 	'<td class="collapseCa">NO VERIFICADO</td>';}
			else if(val.request_status==1)
			{
			table += 	'<td class="collapseCa">VERIFICADO</td>';
			}
			table += '</tr>';
			
		});
		
		$('#main-content-table-tbody').append(table);
		var number = $("body").find('#main-content-table-tbody tr').length;
		//$('#main-content-table-tbody tr').slice(3,number).hide();
		
		return false;
	})
}

function tableHome_c()
{
$('#loading-div2').addClass("active"); 
	
	var existCondition = setInterval(function() {
		if ($("#main-content-table-tbody").children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
		}
else
{
}
	   }, 100); 

var table="";
	var url='application/ajax/numberRegister.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'',
		dataType:'json',
		success: function(response)
		{
		
	table += '<tr><td rowspan="">Sucursales</td><td>Registros</td><td>'+response.totalBranchOffice+'</td></tr><tr>';
	table += '<td rowspan="2" class="green">Requerimientos</td><td class="green">Registros</td><td class="green">'+response.numberRequest+'</td></tr><tr ><td class="green">Verificados</td><td class="green">'+response.numberRequestTrue+'</td></tr>';
	table += '<tr ><td rowspan="3">Ordenes</td><td>Registros</td><td>'+response.numberOT+'</td></tr><tr><td>En proceso</td><td>'+response.numberOTenproceso+'</td></tr><tr><td>Finalizados</td><td>'+response.numberOTfinalizado+'</td></tr>';
			 $("body").find("#main-content-table-tbody").append(table);
			
	
		  }
	 });
	
	 return false;
}





function insertCompany()
{
	var url='application/ajax/updateBusiness.php';
	var form = $('#new-company-form')[0];
	var data = new FormData(form);
	
	//console.log($('#new-proyect-form').serialize());
	$.ajax(
	{
		type:'POST',
		enctype: 'multipart/form-data',
		url:url,
		data:data,
		processData: false,
        contentType: false,
        cache: false,
		success: function(response)
		{
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}


function updateAdmin()
{
	var url='application/ajax/writeAdmin.php';
	//console.log($('#new-proyect-form').serialize());
	$.ajax(
	{
		type:'POST',
		url:url,
		data:$('#new-admin-form').serialize(),
		success: function(response)
		{

			$(".scriptBox").html(response);
			
			return false;
  		}
 	});
 	return false;
}

function tableReceipt()
{
	$("body").find('#receiptBus').empty();
	var url='application/ajax/listBusiness.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'',
		dataType:'json',
		success: function(response)
		{
			console.log(response);
			$.each(response, function(key, val) 
			{
				console.log(val);
				$("body").find('#receiptBus').append(' <option value="'+val.business_id+'">'+val.business_name+'</option>');
				
			});
			return false;
		  }
	 });
	 return false;
}


function tableaHome_c()
{
	console.log("a0");
	var url='application/ajax/numberRegister.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'',
		dataType:'json',
		success: function(response)
		{
console.log("a1");
	table += '<tr><td rowspan="">Sucursales</td><td>Registros</td><td>23</td></tr><tr>';
	table += '<td rowspan="2" class="green">Requerimientos</td><td class="green">Registros</td><td class="green">12</td></tr><tr ><td class="green">Verificados</td><td class="green">2</td></tr>';
	  table += '<tr ><td rowspan="3">Ordenes</td><td>Registros</td><td>45</td></tr><tr><td>En proceso</td><td>23</td></tr><tr><td>Finalizados</td><td>1</td></tr>';
			
			console.log(response);
			
		  }
	 });
	 $("body").find("#main-content-table-tbody").append(table);
	 return false;
}









function showOT(id)
{
 
	var request_id=id;
	var url = "public/views/workorder.html";
	$.when(
		$.get(url, function(data) {
			tablesDestroyer();
			$('#content').html(data);
		})
	).done(function() {
		$("body").find(".fooTable").removeClass("hidden");
		$("body").find(".regConsult").addClass("hidden");
		$("body").find(".add").addClass("hidden");
		userType();
      $('#loading-div2').addClass("active"); 
	var element=$("body").find('.main-tbody-table');
	var existCondition = setInterval(function() {
		if (element.children().length > 0) {
		   clearInterval(existCondition);
		   $('#loading-div2').removeClass("active"); 
          console.log("sii");
		}
	   }, 100);
		var url2='application/ajax/listWorkOrder.php';
		$.ajax(
			{
				type:'POST',
				url:url2,
				data:'request_id='+request_id,
				dataType:'json',
				success: function(response)
				{
					var table="";
					$('#ots-content-table-tbody').empty();
					console.log(response)
					$.each(response, function(key, value)
					{
						
						if(response[key].request_id==request_id)
						{
							
						var servicesAcum="";
						var diagnosticAcum="";
						var productsAcum="";
						var technicalAcum="";
						$.each(response[key].services, function(key2, value2)
						{	
							if(value2!=false)
							{	
							servicesAcum += (response[key].services[key2].services_name);
							servicesAcum += ", ";
							}
							else
							{
							servicesAcum = "";
							}
			
						});
			
						$.each(response[key].technical, function(key2, value2)
						{
							if(value2!=false)
							{
							technicalAcum += (response[key].technical[key2].technical_name);
							technicalAcum += ", ";
							}
							else
							{
							technicalAcum = "";
							}
						});
					
			
						$.each(response[key].product, function(key2, value2)
						{
							if(value2!=false)
							{
								productsAcum += (response[key].product[key2].product_description);
								productsAcum += ", ";
							}
							else
							{
								productsAcum = "";
							}
						});
			
						$.each(response[key].diagnostic, function(key2, value2)
						{
							if(value2!=false)
							{
							diagnosticAcum += (response[key].diagnostic[key2].diagnostic_name);
							diagnosticAcum += ", ";
							}
							else
							{
							diagnosticAcum ="";
							}
						});
			
						
						servicesAcum = servicesAcum.slice(0,-2);
						diagnosticAcum = diagnosticAcum.slice(0,-2);
						productsAcum = productsAcum.slice(0,-2);
						technicalAcum = technicalAcum.slice(0,-2);
						
						table += '<tr>';
						table += 	'<td class="t-trigger" data-label="Fecha">'+response[key].work_order_generation+'</td>';
			
						if(response[key].work_order_status=="EN PROCESO")
						{
						table +=    '<td data-label="Estado"><div class="tableDropdown2">'+response[key].work_order_status+'</div></td>';}
						else if(response[key].work_order_status=="FINALIZADO")
						{
						table +=    '<td data-label="Estado"><div class="tableDropdown3">'+response[key].work_order_status+'</div></td>';}
			
						table += 	'<td data-label="Servicios">'+servicesAcum+'</td>';
						table += 	'<td data-label="Diagnosticos">'+diagnosticAcum+'</td>';
						table += 	'<td data-label="Productos">'+productsAcum+'</td>';
						table += 	'<td data-label="Tecnicos">'+technicalAcum+'</td>';
						table += 	'<td data-label="Proyecto">'+response[key].proyect_name+'</td>';
						table += 	'<td class="no-print" data-label="Acciones" >';
						table += 		'<button class="tableDropdown dropdowns">Aciones</button>';
						table +=    	'<ul class="dropdownButtonList">';
						table += 			'<li onclick="consultWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Ver</a></li>';
						if(user!=2)
						{
						table += 			'<li onclick="receiptWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Recibo</a></li>';
						table += 			'<li onclick="editWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Editar</a></li>';
						table += 			'<li onclick="deleteWorkOrder('+parseInt(response[key].work_order_id, 10)+')"><a href="#0">Eliminar</a></li>';
						}
						table += 		'</ul>';
						table += 	'</td>';
						table += '</tr>';	
						console.log(table);
						}
					});
					$.when($('.main-tbody-table').append(table)).done(createDataTable());
					return false;
	
				}
			});
		if(user==2)
		{$(".new-customer-trigger").hide();}
	});
}

















//DELETEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE

function deleteBusiness(format_id)
{
  	var id = format_id;

		var url='application/ajax/deleteFormat.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:"format_id="+id,
			success: function(response)
			{
				if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Business");}
				$(".scriptBox").html(response);
				return false;
  			}
 		});
 		return false;
    
}

function deleteBranchOffice(branch_office_id)
{
	var id = branch_office_id;
	var url='application/ajax/deleteBranchOffice.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"branch_office_id="+id,
		success: function(response)
		{
			$(".scriptBox").html(response);
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("BranchOffice");}
			return false;
  		}
 	});
 	return false;
}


function deleteRequest(request_id)
{
	var id = request_id;
	var url='application/ajax/deleteRequest.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"request_id="+id,
		success: function(response)
		{
			$(".scriptBox").html(response);
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Request");}
			return false;
  		}
 	});
 	return false;
}

function deleteWorkOrder(work_order_id)
{
	var id = work_order_id;
	var url='application/ajax/deleteWorkOrder.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"work_order_id="+id,
		success: function(response)
		{
			$(".scriptBox").html(response);
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Ot");}
			return false;
  		}
 	});
 	return false;
}

function deleteProyect(proyect_id)
{
    var id = proyect_id;
  	
		var url='application/ajax/deleteProyect.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:"proyect_id="+id,
			success: function(response)
			{
				if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Proyect");}
				$(".scriptBox").html(response);
				return false;
  			}
 		});
 		return false;
     
}



$("body").on("click",".deleteService",function(e)
{
  
    var id = $(this).attr("id");
var url='application/ajax/deleteServices.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"services_id="+id,
		success: function(response)
		{
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Service");}
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
 
});



function deleteTechnical(technical_id)
{
	var id = technical_id;
	var url='application/ajax/deleteTechnical.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"technical_id="+id,
		success: function(response)
		{
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Technical");}
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}


function deleteDiagnostic(diagnostic_id)
{
	var id = diagnostic_id;
	var url='application/ajax/deleteDiagnostic.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"diagnostic_id="+id,
		success: function(response)
		{
			$(".scriptBox").html(response);
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Diagnostic");}
			return false;
  		}
 	});
 	return false;
}



function deleteProduct(product_id)
{
	var id = product_id;
	var url='application/ajax/deleteProduct.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"product_id="+id,
		success: function(response)
		{
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("Product");}
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}




function deleteUser(user_id)
{
	var id = user_id;
	var url='application/ajax/deleteUser.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:"id_user="+id,
		success: function(response)
		{
			if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection("User");}
			$(".scriptBox").html(response);
			return false;
  		}
 	});
 	return false;
}























function deleteElement(id,section)
{
	$("#alertDiv").addClass("active");
	var url="application/ajax/delete"+section+'.php';
	var data = JSON.stringify({id:id,url:url,section:section});
	$('#confirmDeleteRequest').data('info', data);
}

function removefromTable()
{
	var info = JSON.parse($(this).data("info"));
    $("#alertDiv").removeClass("active");
		$.ajax(
		{
			type:'POST',
			url:info.url,
			data:"id="+info.id,
			success: function(response)
			{
				if(response.substr(response.indexOf("'") + 1,5)=="Exito"){changeSection(info.section);}
				$(".scriptBox").html(response);
				return false;
  			}
 		});
 		return false;
}

function closeAlert()
{
	$('#confirmDeleteRequest').data('info','');
   	$("body").find("#alertDiv").removeClass("active");
}

$("#confirmDeleteRequest").on("click", removefromTable);
$("#cancelDeleteRequest").on("click", closeAlert)


	











function tableAccount(format_id)
{
	
	
	var id=format_id;
	$(".fooTable").toggleClass("hidden");
	$(".add").toggleClass("hidden");
	var url='application/ajax/showAdminByCode.php';
	$.ajax(
	{
		type:'POST',
		url:url,
		data:'format_id='+id,
		success: function(response)
		{
			var data = JSON.parse(response);
			$("body").find("#user_name").val(data[0]["user_name"]);
			$("body").find("#user_rut").val(data[0]["user_rut"]);
			$("body").find("#user_email").val(data[0]['user_email']);
  		}
	 });
	 $("body").find(".add").removeClass("hidden");
}




