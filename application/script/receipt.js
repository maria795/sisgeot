jQuery(document).ready(function()
{
	$("body").on("click",".receipt",function(e)
	{
		loadView().done(function()
		{
			loadData().done();
		});
	});

	function loadView()
	{
		var dfrd1 = $.Deferred();
		$(".printOptions").removeClass("active");
		$(".print").removeClass("active");
		$(".save-div").removeClass("active");
		$(".icon-save").removeClass("active");
		$("body").find("#busInfo").empty();
		var d = new Date();
		var business_id = $("body").find("#receiptBus").val();
		var url='application/ajax/showBusinessListCode.php';
		$.ajax(
		{
			type:'POST',
			url:url,
			data:'business_id='+business_id,
			dataType:'json',
			success: function(response)
			{
				$("body").find("#busInfo").html("<p>Fono: "+response["0"].business_phone+"</p><p>"+response["0"].business_direction+"</p><p>"+response["0"].business_website+"</p>");
				$("body").find("#busDetails").text(response["0"].business_description+" R.U.T. "+response["0"].business_rut+" ha recibido pago emitido por: ");	
				$("body").find("#receiptDateInput").text((d.getMonth()+1)+"/"+(d.getDate())+"/"+d.getFullYear());
				$("body").find("#receiptServiceInput").text($("body").find(" "+"#receiptService").val());
				$("body").find("#receiptServiceInputName").text($("body").find("#receiptName").val());
				$("body").find("#receiptServiceInputMoney").text($("body").find("#receiptMoney").val());
				$("body").find("#receiptServiceInputBy").text($("body").find("#receiptBy").val());
				$("body").find("#receiptPaymentInput").text(" "+$("body").find("#payment_method").val());
				$("body").find("#receiptObsInput").text($("body").find("#receiptObs").val());
				$("body").find("#newReceiptImg").attr('src','public/img/business/'+response["0"].business_logo);
			}
		});
		$('#newReceiptImg,#busInfo,#busDetails').off().bind('load', function() {
			dfrd1.resolve();
		});
		return $.when(dfrd1).done().promise();
	}

	function loadData()
	{
		var dfrd1 = $.Deferred();

		var css = '@page { size: portrait; }',
		head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');
		style.type = 'text/css';
		style.media = 'print';
		if (style.styleSheet){style.styleSheet.cssText = css;} 
		else {style.appendChild(document.createTextNode(css));}
		head.appendChild(style);
		
		window.print();
		
		dfrd1.resolve();
		return dfrd1.promise();
	}
});




	
	











/*
jQuery(document).ready(function()
{
	$("body").on("click",".receipt",function(e)
	{
		function getBusinessDetails()
		{
			var business_id = $("body").find("#receiptBus").val();
			var url='application/ajax/showBusinessListCode.php';
			$.ajax(
			{
				type:'POST',
				url:url,
				data:'business_id='+business_id,
				dataType:'json',
				success: function(response)
				{
					$("body").find("#newReceiptImg").attr('src','public/img/business/'+response["0"].business_logo);
					$("body").find("#busInfo").html("<p>Fono: "+response["0"].business_phone+"</p><p>"+response["0"].business_direction+"</p><p>"+response["0"].business_website+"</p>");
					$("body").find("#busDetails").text(response["0"].business_description+" R.U.T. "+response["0"].business_rut+" ha recibido pago emitido por: ");	
				}
			});
			$("body").find("#busInfo").empty();
			var d = new Date();
			$("body").find("#receiptDateInput").text((d.getMonth()+1)+"/"+(d.getDate())+"/"+d.getFullYear());
			$("body").find("#receiptServiceInput").text($("body").find(" "+"#receiptService").val());
			$("body").find("#receiptServiceInputName").text($("body").find("#receiptName").val());
			$("body").find("#receiptServiceInputMoney").text($("body").find("#receiptMoney").val());
			$("body").find("#receiptServiceInputBy").text($("body").find("#receiptBy").val());
			$("body").find("#receiptPaymentInput").text(" "+$("body").find("#payment_method").val());
			$("body").find("#receiptObsInput").text($("body").find("#receiptObs").val());
		}

		function printReceipt()
		{
			var css = '@page { size: portrait; }',
    		head = document.head || document.getElementsByTagName('head')[0],
    		style = document.createElement('style');
			style.type = 'text/css';
			style.media = 'print';
			if (style.styleSheet){style.styleSheet.cssText = css;} 
			else {style.appendChild(document.createTextNode(css));}
			head.appendChild(style);
			window.print();
		}
		
		$.when(getBusinessDetails()).done(function() {printReceipt();});
	});
});
*/