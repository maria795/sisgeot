<?php

class services{
	private $table = "services";
	private $db;

	function __construct(){
		global $db;
		$this->db  = &$db;
	}


	function addServices($services_name)
	{
		$data = array(
			'services_name' =>  $services_name);
		return $this->db->insert($this->table, $data);
	}

	function updateServices($services_id, $services_name)
	{
		$data = array(
			'services_name' =>  $services_name);
		
		return $this->db->update($this->table, $data, "services_id={$services_id}");
	}

	function deleteServices($services_id)
	{
		return $this->db->delete($this->table, "services_id = {$services_id}");
	}

	function showServicesListByCode($services_id)
	{
		return $this->db->select('*', $this->table, "services_id = {$services_id}");
	}
	
	function listServices($limit=null)
	{
		return $this->db->select('*', $this->table, false, $limit);
	}

}
?>