<?php
class product{
	private $table = "product";
	private $db;

	function __construct(){
		global $db;
		$this->db= &$db;
	}

	function addProduct($product_business, $product_description, $product_cost){
		
		$data = array(
			'product_business' => $product_business,
			'product_description' => $product_description,
			'product_cost' => $product_cost
			);
		return $this->db->insert($this->table, $data, '');
	}

	function updateProduct($product_id, $product_business, $product_description, $product_cost){
		
		$product_description = strtoupper($_POST['product_description']);
		$product_cost = strtoupper($_POST['product_cost']);

		$data = array(
			'product_business' => $product_business,
			'product_description' => $product_description,
			'product_cost' => $product_cost
			);
		return $this->db->update($this->table, $data, "product_id = {$product_id}");
	}

	function showProductListByCode($product_id)
	{
		return $this->db->select('*', $this->table, "product_id = {$product_id}");
	}

	function showProductListByBusiness($business_id)
	{
		return $this->db->select('*', $this->table, "product_id = {$business_id}");
	}

	function listProduct($limit=null)
	{
	    return $this->db->select('*', $this->table, false, $limit);
	}
	function deleteProduct($product_id)
	{
		return $this->db->delete($this->table, "product_id = {$product_id}") ;
	}
}

?>