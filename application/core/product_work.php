  <?php

class productWork{

	private $table = "product_work";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addProductWork($work_order_id, $product_id)
	{
		$data = array(
			'work_order_id' => $work_order_id,
			'product_id' => $product_id
			);

		return $this->db->insert($this->table, $data);
	}

	function deleteProductWork($work_order_id)
	{
		return $this->db->delete($this->table, "work_order_id={$work_order_id}") ;
	}
	function showProductByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}

	function showProductWorkListByCode($work_order_id)
	{
		return $this->db->select("*", $this->table, "work_order_id={$work_order_id}");
	}
	
	function listProductWork($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}



}

?>
