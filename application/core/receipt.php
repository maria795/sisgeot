<?php

class receipt{
	private $table = "receipt";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addReceipt($data)
	{
		return $this->db->insert($this->table, $data);
	}
	function updateReceipt($receipt_id)
	{
		return $this->db->update($this->table, $data, "receipt_id={$receipt_id}");
	}

	function deleteReceipt($receipt_id)
	{
		return $this->db->delete($this->table, "receipt_id = {$receipt_id}");
	}

	function showReceiptListByCode($receipt_id)
	{
		return $this->db->select('*', $this->table, "receipt_id = {$receipt_id}");
	}
	
	function listReceipt($limit = null)
	{
		return $this->db->select('*', $this->table, $limit);
	}
}
?>