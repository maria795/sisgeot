<?php
class format{

	private $table = "formats";
 	private $db;

 	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addFormat($format_name, $format_business_name, $format_rut, $format_direction, $format_comuna, $format_region, $format_phone, $format_gerent, $format_gerent_email)
	{
		$data = array(
		'format_name'  => $format_name,
		'format_business_name' => $format_business_name,
		'format_rut'  => $format_rut,
		'format_direction'  => $format_direction,
		'format_comuna'  => $format_comuna,
		'format_region'  => $format_region,
		'format_phone' => $format_phone,
		'format_gerent'  => $format_gerent,
		'format_gerent_email'  => $format_gerent_email
		);

		return $this->db->insert($this->table, $data, '');
	}

	function  updateFormat($format_id, $format_name, $format_business_name, $format_rut, $format_direction, $format_comuna, $format_region, $format_phone, $format_gerent, $format_gerent_email,$user_password)
	{
			$data = array(
			'formats.format_name' => $format_name, 
			'formats.format_business_name' => $format_business_name, 
			'formats.format_rut' => $format_rut, 
			'formats.format_direction' => $format_direction, 
			'formats.format_comuna' => $format_comuna,  
			'formats.format_region' => $format_region,  
			'formats.format_phone '=> $format_phone, 
			'formats.format_gerent' => $format_gerent,  
			'formats.format_gerent_email' => $format_gerent_email, 
			'user.user_email' => $format_gerent_email,  
			'user.user_rut' => $format_rut,
			'user.user_password' => $user_password
			);
			
	return $this->db->update("formats JOIN user ON formats.format_id = user.format_id", $data, "user.format_id = '{$format_id}'");  
	}

	function showFormatListByCode($format_id)
	{
		return $this->db->select('*', $this->table, "format_id = {$format_id}");
	}
	
	function showFormatListByName($format_name)
	{
		return $this->db->select('formats.format_name, formats.format_id, user.id_user,formats.format_rut, user.user_active', " user INNER JOIN formats ON user.format_id = formats.format_id", "format_name = '{$format_name}'");
	}

	function countFormat()
	{
		return $this->db->select('format_id as row', $this->table, false);
	}
	
	function lastId($limit = null)
	{
		return  $this->db->select('format_id', "formats ORDER BY format_id DESC LIMIT 1", false, $limit);
	
	}

	function listFormat($limit = null)
	{
	    return $this->db->select('*', $this->table, "format_id != 0", $limit);
	}

	function deleteFormat($format_id)
	{
		$this->db->delete("user", "format_id = {$format_id}") ;
		return $this->db->delete($this->table, "format_id = {$format_id}") ;

	}


}



?>