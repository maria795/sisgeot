<?php

class orderWork{

function __construct(){
			global $db;
			$this->db = &$db;	
	}

	
function query($business_id)
	{
	$work_order = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id', "request.business_id = '{$business_id}'");
	return $work_order;
	}


function otherQuery(&$work_order, $work_order_id)
	{
		
		$work_order['services'] = $this->db->select('*', 'work_order INNER JOIN services_work ON services_work.work_order_id = work_order.work_order_id INNER JOIN services ON services_work.services_id = services.services_id', "services_work.work_order_id = '{$work_order_id}'");


		$work_order['diagnostic'] = $this->db->select('*', 'work_order INNER JOIN diagnostic_work ON diagnostic_work.work_order_id = work_order.work_order_id INNER JOIN diagnostic ON diagnostic_work.diagnostic_id = diagnostic.diagnostic_id', "diagnostic_work.work_order_id = '{$work_order_id}'");

		$work_order['technical'] = $this->db->select('*', 'work_order INNER JOIN technical_work ON technical_work.work_order_id = work_order.work_order_id INNER JOIN technical ON technical_work.technical_id = technical.technical_id ', "technical_work.work_order_id = '{$work_order_id}'");

		$work_order['product'] = $this->db->select('*', 'work_order INNER JOIN product_work ON product_work.work_order_id = work_order.work_order_id INNER JOIN product ON product_work.product_id = product.product_id', "product_work.work_order_id = '{$work_order_id}'");
		

		$work_order['photo'] = $this->db->select('*', "work_order INNER JOIN photo ON work_order.work_order_id = photo.work_order_id", "photo.work_order_id = '{$work_order_id}'");
		
		$work_order['video'] = $this->db->select('*', "work_order INNER JOIN video ON work_order.work_order_id = video.work_order_id", "video.work_order_id = '{$work_order_id}'");

		$work_order['document'] = $this->db->select('*', "work_order INNER JOIN document ON work_order.work_order_id = document.work_order_id", "document.work_order_id = '{$work_order_id}'");

		//$a = array_merge($services, $diagnostic_work);
   	}


	function query_ad()
	{
	$work_order_ad = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id');
		return $work_order_ad;
	}


function otherQuery_ad(&$work_order_ad, $work_order_id)
	{
		
		$work_order_ad['services'] = $this->db->select('*', 'work_order INNER JOIN services_work ON services_work.work_order_id = work_order.work_order_id INNER JOIN services ON services_work.services_id = services.services_id', "services_work.work_order_id = '{$work_order_id}'");


		$work_order_ad['diagnostic'] = $this->db->select('*', 'work_order INNER JOIN diagnostic_work ON diagnostic_work.work_order_id = work_order.work_order_id INNER JOIN diagnostic ON diagnostic_work.diagnostic_id = diagnostic.diagnostic_id', "diagnostic_work.work_order_id = '{$work_order_id}'");

		$work_order_ad['technical'] = $this->db->select('*', 'work_order INNER JOIN technical_work ON technical_work.work_order_id = work_order.work_order_id INNER JOIN technical ON technical_work.technical_id = technical.technical_id ', "technical_work.work_order_id = '{$work_order_id}'");

		$work_order_ad['product'] = $this->db->select('*', 'work_order INNER JOIN product_work ON product_work.work_order_id = work_order.work_order_id INNER JOIN product ON product_work.product_id = product.product_id', "product_work.work_order_id = '{$work_order_id}'");
		
		$work_order_ad['photo'] = $this->db->select('*', "work_order INNER JOIN photo ON work_order.work_order_id = photo.work_order_id", "photo.work_order_id = '{$work_order_id}'");
		
		$work_order_ad['video'] = $this->db->select('*', "work_order INNER JOIN video ON work_order.work_order_id = video.work_order_id", "video.work_order_id = '{$work_order_id}'");

		$work_order_ad['document'] = $this->db->select('*', "work_order INNER JOIN document ON work_order.work_order_id = document.work_order_id", "document.work_order_id = '{$work_order_id}'");

		//$a = array_merge($services, $diagnostic_work);

	}


	function work_work_order_request($format_id,$limit=null)
	{
		$work_order_request = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id', "formats.format_id = '{$format_id}'", $limit);
		return $work_order_request;
	}

        function work_order_client($format_id,$limit=null)
	{
		$order_client = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id', "formats.format_id = '{$format_id}'", $limit);
		return $order_client;
	}


	function otherQueryClient(&$work_order_request, $work_order_id)
	{
		
		$work_order_request['services'] = $this->db->select('*', 'work_order INNER JOIN services_work ON services_work.work_order_id = work_order.work_order_id INNER JOIN services ON services_work.services_id = services.services_id', "services_work.work_order_id = '{$work_order_id}'");


		$work_order_request['diagnostic'] = $this->db->select('*', 'work_order INNER JOIN diagnostic_work ON diagnostic_work.work_order_id = work_order.work_order_id INNER JOIN diagnostic ON diagnostic_work.diagnostic_id = diagnostic.diagnostic_id', "diagnostic_work.work_order_id = '{$work_order_id}'");

		$work_order_request['technical'] = $this->db->select('*', 'work_order INNER JOIN technical_work ON technical_work.work_order_id = work_order.work_order_id INNER JOIN technical ON technical_work.technical_id = technical.technical_id ', "technical_work.work_order_id = '{$work_order_id}'");

		$work_order_request['product'] = $this->db->select('*', 'work_order INNER JOIN product_work ON product_work.work_order_id = work_order.work_order_id INNER JOIN product ON product_work.product_id = product.product_id', "product_work.work_order_id = '{$work_order_id}'");
		

		$work_order_request['photo'] = $this->db->select('*', "work_order INNER JOIN photo ON work_order.work_order_id = photo.work_order_id", "photo.work_order_id = '{$work_order_id}'");
		
		$work_order_request['video'] = $this->db->select('*', "work_order INNER JOIN video ON work_order.work_order_id = video.work_order_id", "video.work_order_id = '{$work_order_id}'");

		$work_order_request['document'] = $this->db->select('*', "work_order INNER JOIN document ON work_order.work_order_id = document.work_order_id", "document.work_order_id = '{$work_order_id}'");

		//$a = array_merge($services, $diagnostic_work);

	}
	 function queryFormat($format_id)
	{
	$work_order0 = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id', "formats.format_id = '{$format_id}'");
	return $work_order0;
	}



	function ShowListAll($request_id)
	{
		return $this->db->select("*", "formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id", 
			"request.request_id = '{$request_id}'", false);
	}

	function showListAllRequest($format_id, $limit=null)
	{
		return $this->db->select("*", "formats  INNER JOIN branch_office ON formats.format_id = branch_office.format_id  INNER JOIN request  ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id", "formats.format_id = '{$format_id}'", $limit);
	}


	function listRequestTableBusiness($business_id, $limit=null)
	{
		return $this->db->select("*", "formats  INNER JOIN branch_office ON formats.format_id = branch_office.format_id  INNER JOIN request  ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id", "request.business_id = '{$business_id}'"
, $limit);
	}

	function listRequestTable($limit=null)
	{
		return $this->db->select("*", "formats  INNER JOIN branch_office ON formats.format_id = branch_office.format_id  INNER JOIN request  ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id", false, $limit);
	}


	function queryFormatStatus($format_id, $work_order_status)
	{
	$work_order = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id', "formats.format_id = '{$format_id}' and work_order.work_order_status = '{$work_order_status}'");
	return $work_order;
	}


	function queryFormatRequest($request_id)
	{
	$work_order_request = $this->db->select('*', 'formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id INNER JOIN business ON request.business_id = business.business_id INNER JOIN work_order ON request.request_id = work_order.request_id INNER JOIN proyect ON work_order.proyect_id = proyect.proyect_id', "request.request_id = '{$request_id}'");
	return $work_order_request;
	}

	function otherQueryRequest(&$work_order_request, $work_order_id)
	{
		
		$work_order_request['services'] = $this->db->select('*', 'work_order INNER JOIN services_work ON services_work.work_order_id = work_order.work_order_id INNER JOIN services ON services_work.services_id = services.services_id', "services_work.work_order_id = '{$work_order_id}'");


		$work_order_request['diagnostic'] = $this->db->select('*', 'work_order INNER JOIN diagnostic_work ON diagnostic_work.work_order_id = work_order.work_order_id INNER JOIN diagnostic ON diagnostic_work.diagnostic_id = diagnostic.diagnostic_id', "diagnostic_work.work_order_id = '{$work_order_id}'");

		$work_order_request['technical'] = $this->db->select('*', 'work_order INNER JOIN technical_work ON technical_work.work_order_id = work_order.work_order_id INNER JOIN technical ON technical_work.technical_id = technical.technical_id ', "technical_work.work_order_id = '{$work_order_id}'");

		$work_order_request['product'] = $this->db->select('*', 'work_order INNER JOIN product_work ON product_work.work_order_id = work_order.work_order_id INNER JOIN product ON product_work.product_id = product.product_id', "product_work.work_order_id = '{$work_order_id}'");
		
		$work_order_request['photo'] = $this->db->select('*', "work_order INNER JOIN photo ON work_order.work_order_id = photo.work_order_id", "photo.work_order_id = '{$work_order_id}'");
		
		$work_order_request['video'] = $this->db->select('*', "work_order INNER JOIN video ON work_order.work_order_id = video.work_order_id", "video.work_order_id = '{$work_order_id}'");

		$work_order_request['document'] = $this->db->select('*', "work_order INNER JOIN document ON work_order.work_order_id = document.work_order_id", "document.work_order_id = '{$work_order_id}'");

		//$a = array_merge($services, $diagnostic_work);

	}





}
?>			