 <?php

class document
{
	private $table = "document"; 
	private $db;

	function __construct(){
		global $db;
		$this->db  = &$db;
	}

	function addDocument($work_order_id, $document_destination)
	{
			$data = array(
			     	'document_destination'=> $document_destination,
			     	'work_order_id'=> $work_order_id
			     	);
			return  $this->db->insert($this->table, $data);                  
	}

	function deleteDocument($document_id)
	{
		return $this->db->delete($this->table, "document_id={$document_id}");
	}

	function showDocumentListByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}

}

?>