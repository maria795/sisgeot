<?php

class workOrder{

	private $table = "work_order";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}
	
	function addWorkOrder($request_id, $work_order_generation, $proyect_id, $work_order_date, $time_input, $time_output, $work_order_observation, $work_order_status){
		$data = array(
			'request_id' => $request_id, 
			'work_order_generation' => $work_order_generation, 
			'proyect_id' =>  $proyect_id, 
			'work_order_date' =>  $work_order_date, 
			'time_input' =>  $time_input, 
			'time_output' =>  $time_output,
			'work_order_observation' => $work_order_observation,
			'work_order_status' => $work_order_status
			);
			return $this->db->insert($this->table, $data);
	}

		function updateWorkOrder($work_order_id,$work_order_generation, $proyect_id, $work_order_date, $time_input, $time_output, $work_order_observation,$work_order_status)

		{
		$data = array(
			'work_order_generation' => $work_order_generation, 
			'proyect_id' =>  $proyect_id, 
			'work_order_date' =>  $work_order_date, 
			'time_input' =>  $time_input, 
			'time_output' =>  $time_output,
			'work_order_observation' => $work_order_observation,
			'work_order_status' => $work_order_status,

			);

			return $this->db->update($this->table, $data,"work_order_id={$work_order_id}");
		}

	function showWorkOrderListByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}

	function lastId($limit = null)
	{
		return  $this->db->select('work_order_id', "work_order ORDER BY work_order_id DESC LIMIT 1", false, $limit);
	}
	function deleteOrderWork($work_order_id)
	{
		return $this->db->delete($this->table, "work_order_id='{$work_order_id}'");
	}

}


/*
	function updateOrderWork{}
	function deleteOrderWork{}
	function showOrderWorkListByCode(){}
	function listOrderWork{}

}*/
?>