<?php

class technical{
	private $table = "technical";
	private $db;

	function __construct(){
		global $db;
		$this->db  = &$db;
	}

	function addTechnical($technical_name, $technical_rut, $technical_position, $technical_phone, $technical_email)
	{
		$data = array(
		'technical_name'    =>	$technical_name, 
		'technical_rut' 	=>	$technical_rut, 
 		'technical_position'=> 	$technical_position, 
		'technical_phone' 	=>	$technical_phone, 
		'technical_email'	=>	$technical_email
		);
		return $this->db->insert($this->table, $data, "");
	}


	function updateTechnical($technical_id, $technical_name, $technical_rut, $technical_position, $technical_phone, $technical_email)
	{
	
		$data = array(
		'technical_name'    =>	$technical_name, 
		'technical_rut' 	=>	$technical_rut, 
 		'technical_position'=> 	$technical_position, 
		'technical_phone' 	=>	$technical_phone, 
		'technical_email'	=>	$technical_email
		);

		return $this->db->update($this->table, $data, "technical_id={$technical_id}");
	}


	function deleteTechnical($technical_id)
	{
		return $this->db->delete($this->table, "technical_id = {$technical_id}") ;
	}
	

	function showTechnicalListByCode($technical_id)
	{
		return $this->db->select("*", $this->table, "technical_id = {$technical_id}");
	}
	
	function listTechnical($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}

}

?>