<?php
class business{
	private $table = "business";
	private $db;

	function __construct(){
		global $db;
		$this->db= &$db;
	}

	function updateBusiness($data, $business_id)
	{
		return $this->db->update($this->table, $data, "business_id = '{$business_id}'");
	}

	function showBusinessListByCode($business_id)
	{
		return $this->db->select('*', $this->table, "business_id = '{$business_id}'");
	}
	function listBusiness($limit = null)
	{
		return $this->db->select('*', $this->table, $limit);
	}

}