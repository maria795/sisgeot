 <?php

class video
{
	private $table = "video"; 
	private $db;

	function __construct(){
		global $db;
		$this->db  = &$db;
	}

	function addVideo($work_order_id, $video_destination)
	{
			$data = array(
			     	'work_order_id'=> $work_order_id,
			     	'video_destination'=> $video_destination
			     	);
			return  $this->db->insert($this->table, $data);                  
	}

	function deleteVideo($video_id)
	{
		return $this->db->delete($this->table, "video_id={$video_id}");
	}

	function showVideoListByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}

}

?>