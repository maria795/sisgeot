<?php

class request{

	private $table = "request";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addRequest($branch_office_id, $request_date, $request_department, $request_medium, $request_r_name, $request_r_phone, $request_r_email, $request_status, $request_app, $business_id)
	{
		
		$data = array(
		'branch_office_id'  => $branch_office_id,
		'request_date'  => $request_date,
		'request_department'  => $request_department,
		'request_medium'  => $request_medium,
		'request_r_name' => $request_r_name,
		'request_r_phone'  => $request_r_phone,
		'request_r_email' => $request_r_email,
		'request_status'=>$request_status,
		'request_app' => $request_app,
		'business_id' => $business_id
		);
		return $this->db->insert($this->table, $data);
	}

	function updateRequest($request_id, $request_department, $request_medium, $request_r_name, $request_r_phone, $request_r_email, $request_app, $business_id)
	{

		
		$data = array(
		'request_department'  => $request_department,
		'request_medium'  => $request_medium,
		'request_r_name' => $request_r_name,
		'request_r_phone'  => $request_r_phone,
		'request_r_email' => $request_r_email,
		'request_app' => $request_app,
		'business_id' => $business_id

		);
		return $this->db->update($this->table, $data, "request_id={$request_id}");
	}

	function updateStatusRequest($request_id, $request_status)
	{

		$data = array(
		'request_status' => $request_status
		);

		return $this->db->update($this->table, $data, "request_id='{$request_id}'");
	}

	function deleteRequest($request_id)
	{
		$this->db->delete("verification_request", "request_id = {$request_id}") ;		
		return $this->db->delete($this->table, "request_id = {$request_id}") ;
	}

	function showRequestListByCode($request_id)
	{
		return $this->db->select("*", $this->table, "request_id = {$request_id}");
	}
	
	function listRequest($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}

	function listRequestBusiness($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}

	function listRequestLast($limit=null)
	{
		return $this->db->select("*", $this->table ." order by request_id desc", false, $limit);
	}
	function ShowListAll($request_id)
	{
		return $this->db->select("*", "formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id", 
			"request.request_id = '{$request_id}'", false);
	}

       function ListAllFormatByRequest($format_id)
	{
		return $this->db->select("*", "formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id", 
			"formats.format_id = '{$format_id}'", false);
	}

	function ShowListAllFormat($format_id, $request_status)
	{
		return $this->db->select("*", "formats INNER JOIN branch_office ON formats.format_id = branch_office.format_id INNER JOIN request ON branch_office.branch_office_id = request.branch_office_id", 
			"formats.format_id = '{$format_id}' and request.request_status = '{$request_status}'", false);
	}

}

?>	