<?php 

class recoverPassword extends user {
	private $table = "reset_password";
	public $email;
	public $password;
	public $repeat_password;
	public $comparePassword;

	function addLink($id_user,$token,$creado)
	{
		$data = array(
			'id_user' =>$id_user,
			'token'=> $token,
			'creado' => $creado
		);
		return $this->db->insert($this->table, $data);
	}

	function showLink($token)
	{
		return $this->db->select("*", "{$this->table} LEFT JOIN user ON user.id_user={$this->table}.id_user", "{$this->table}.token='{$token}'",true);

	}
function showUserList($id_user, $limit=null)
	{
		return $this->db->select('*', $this->table, "id_user = '{$id_user}'", $limit);
	}
	function deleteToken($token)
	{
		return $this->db->delete($this->table, "token='{$token}'", true);
	}

	function updatePassword($id_user, $password)
	{
		$data  = array(
			'user_password' => $this->encrypt($password)
		);

		return $this->db->update('user', $data, "id_user='{$id_user}'");
	}

	function updatePasswordUser($password, $id_user)
	{
		$data  = array
		(
			'user_password' => trim($this->encrypt($password))
		);

		return $this->db->update('user', $data, "id_user={$id_user}");
	}
	


	function verifyEmail($email)
	{
		return (bool) $user = $this->db->select("*", "users", "user_email='{$email}'", true);
	}

	function comparePassword($password, $repeat_password)
	{
		if ($this->encrypt($repeat_password) == $this->encrypt($password)) 
		{	
			return true;
		}
		return false;
	}

function getRealIP()
{

    if (isset($_SERVER["HTTP_CLIENT_IP"]))
    {
        return $_SERVER["HTTP_CLIENT_IP"];
    }
    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    }
    elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
    {
        return $_SERVER["HTTP_X_FORWARDED"];
    }
    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
    {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    }
    elseif (isset($_SERVER["HTTP_FORWARDED"]))
    {
        return $_SERVER["HTTP_FORWARDED"];
    }
    else
    {
        return $_SERVER["REMOTE_ADDR"];
    }

}

}


?>