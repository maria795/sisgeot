<?php
class verification{
	private $table = "verification_request";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}
	
	function addVerification($data)
	{
		return $this->db->insert($this->table, $data, false);
	}

	function deleteVerification($verification_id)
	{
		return $this->db->delete($this->table, "verification_id = {$verification_id}");
	}

	function showTokenVerificationList($v_token)
	{
		return $this->db->select('*', $this->table, "v_token = '{$v_token}'");
	}

	function updateTokenVerification($data, $request_id)
	{

		return $this->db->update($this->table, $data, "v_request_id = {$request_id}");
	}

	function listVerificationBusiness($business_id)
	{
		return $this->db->select('*', 'verification_request INNER JOIN request ON request.request_id = verification_request.v_request_id', "request.business_id = '{$business_id}'");
	}

	function listVerificationRequest($request_id)
	{
		return $this->db->select('*', 'verification_request INNER JOIN request ON request.request_id = verification_request.v_request_id', "request_id = {$request_id}");
	}

	function listVerification($limit = null)
	{
		return $this->db->select('*', 'verification_request INNER JOIN request ON request.request_id = verification_request.v_request_id', $limit);
	}

		     


}
?>