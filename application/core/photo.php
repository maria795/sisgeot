<?php

class photo
{
	private $table = "photo"; 
	private $db;

	function __construct(){
		global $db;
		$this->db  = &$db;
	}

	function addPhoto($work_order_id, $photo_destination)
	{
   
          $data = array(
            'photo_destination'=> $photo_destination,
            'work_order_id'=> $work_order_id
           );
          return  $this->db->insert($this->table, $data);       
             
  } 


  function listPhoto($limit = null)
  {
    return $this->db->select('*', $this->table, $limit);
  }
  function deletePhoto($photo_id)
  {
    return $this->db->delete($this->table, "photo_id={$photo_id}");
  }
  function showPhotoListByWorkOrder($work_order_id)
  {
    return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
  }


}

?>