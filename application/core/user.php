<?php
class user{

	private $table = "user";
	protected $db;
	private $user_active;
	private $correctPassword = false;
	private $password;
	public 	$user = array();
	public $user_type;
	public $format_id;
	public $id_user;
	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function login($format_rut, $format_password)
	{
		$format_password = $this->encrypt($format_password);

			$this->user = $this->db->select('*', "user INNER JOIN formats ON formats.format_id = user.format_id",  " user.user_rut='{$format_rut}' AND user.user_password='{$format_password}'", true);

		if (empty($this->user)) return false;
		$this->initUserParameters($this->user);

		$this->correctPassword =(bool)($this->user_password === $format_password);
	}

	function initByEmail($email){
		$this->user = $this->db->select('*', "user", "user.user_email='{$email}'", true);
		$this->initUserParameters($this->user);
	}

	function verifyEmail($email){
		$this->user = $this->db->select('*', $this->table, 'user_email = "{$user_email}"');
	}

	function initUserParameters(){
		if (is_array($this->user) && !empty($this->user)){
			foreach ($this->user as $key => $value) {
				$result = $this->$key = $value;
			}
		}
	}

	function encrypt($string){
		global $salt;
		return sha1("{$string}{$salt}");
		
	}

	function isCorrectPassword(){
		return (bool) $this->correctPassword;
	}
	function isAccess(){
		return (bool) ($this->isActive() && $this->isCorrectPassword());
	}
	function isActive(){
		return (bool) $this->user_active;
	}
	function getUserId(){
		return (int) $this->id_user;
	}
	function IsFormat(){
		return (int) $this->format_id;
	}
	function getUserType(){
		return (int) $this->user_type;
	}
	function getUserBusiness(){
		return  $this->user_business;
	}
	function getUserPassword(){
		return  $this->user_password;
	}

	function addUser($user_name, $user_email, $user_rut, $user_type, $user_business, $format_id, $user_active){
	               

	$firstPassword = $this->generatePassword();
	$password = $this->encrypt($firstPassword);
	$cadena = $user_rut.$user_name.rand(1,99999).date('Y-m-d');
	$token  = sha1($cadena);

	$data = array(
		'user_name' => $user_name, 
		'user_email' => $this->validEmail($user_email),
		'user_password' => $password,
		'user_rut' => $user_rut,
		'user_type' => $user_type,
		'user_business' => $user_business, 
		'user_insert_date' => $this->getCurrentTime(),
		'format_id' => $format_id,
		'user_active' => $user_active,
		'token' => $token
		);

	    $subject = "Usted ha sido registrado en SISGEOT";	   
		$links = $_SERVER['SERVER_NAME'].'/emailVerify.php?l3ja='.$format_id."&token=".$token;
	    $body ='<div id=":153" class="a3s aXjCH m16025006a7cf3d2d"><u></u>
  	<div>
    <table align="center" bgcolor="#eff2f1" border="0" cellpadding="0" cellspacing="0" width="100%" style="
    background-color: #3e3c3c;">
  
    <tbody><tr>
      <td style="padding-bottom:24px;padding-top:16px">
      <center>
        <div style="max-width:560px;display:block;clear:both;margin:0 auto;border-collapse:collapse">
        
        <table align="center" bgcolor="" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody><tr>
          <td style="vertical-align:middle;text-align:center;padding-bottom:16px;padding-top:8px" height="28">
            <a href="sif.cl" title="SIGAOT" target="_blank"><img class="m_5163378976771115696logo CToWUd" style="margin:0 auto;display:block;height:100px" alt="" src="'.$_SERVER['SERVER_NAME'].'/public/img/logo.png" height="100"></a>
          </td>
          </tr>
        </tbody></table>
        
        </div>
        <div style="max-width:100%;display:block;clear:both;margin:0 auto">
        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top-width:1px;border-bottom-width:1px;border-top-style:solid;border-bottom-style:solid;border-right-width:0;border-left-width:0;border-top-color:#e6e6e6;border-bottom-color:#e6e6e6">
  
          <tbody><tr>
          <td style="padding-top:24px;padding-bottom:24px;padding-left:16px;padding-right:16px;text-align:left">
            <div style="max-width:560px;display:block;clear:both;margin:0 auto">
            
            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px;line-height:1.5;border-collapse:collapse;text-align:left"><tbody><tr>
    <td style="padding:16px" width="100%">
      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:16px">
        <tbody><tr>
          <td width="100%" style="line-height:1.3;padding-bottom:8px"> 
              <p style="font-size:24px;text-align:center;">APERTURA DE CUENTA DE USUARIO</p>
              <br><br>
  <strong>Hola <span style="text-transform:capitalize">'.$user_name.'</span>,</strong>
              <br> Ha sido registrado en nuestra base de datos una cuenta con su información.
              <br>
              <br>
              <strong>Si usted no solicitó obtener una cuenta en nuestro sistema, ignoré este correo electronico</strong>
              <br>
              <br>Para comenzar a disfrutar de nuestros servicios,  <strong> valide sus datos: </strong> 
              <br><br>
              <strong>Datos de su cuenta de usuario:</strong><br><br>
              <ul style="margin:0;padding:0;margin-left:24px">
                  <li style="margin:0;padding:0"><strong>RUT:</strong>'.' '.$user_rut.'</li>
                  <li style="margin:0;padding:0"><strong>CONTRASEÑA:</strong>'.' '.$firstPassword.'</li>              
                </ul>
              <br>
              <i>Usted sera redirecconado al presionar click en el siguiente boton</i>
              <br><br>
              <a href="'.$links.'" style="background: #4284ad;color:#fff;padding:12px;font-size:16px;text-decoration:none;display:block;margin:0;text-transform:uppercase;text-align:center;/* border-radius:2px; */" target="_blank">VERIFICAR CUENTA</a>
         
              <br> <span style="font-style:italic">Sistema de Gestión y Administración de Órdenes de Trabajo <br> (Tecnosec, Infraganti, Global Business)</span>
          </td>
          </td>
        </tr>
      </tbody></table>
    </td>
  </tr></tbody></table>
  </div>
  </td>
  </tr>
  </tbody></table>
  </div>
  </center>
  </td>
  </tr>';
	   $to_andress = $user_email;		
	 	
	   sent_email ($subject, $body, $to_andress);
	   return $this->db->insert("user", $data, '');
	}


		function updateUser($id_user, $user_name, $user_email, $user_rut, $user_type,  $user_business, $format_id = 0,  $firstPassword, $token){

			$data = array(
				'user_name' => $user_name, 
				'user_email' => $user_email,
				'user_rut' => $user_rut,
				'user_business' => $user_business,
				'user_password' => $firstPassword,
				'token' => $token
				);


			return $this->db->update($this->table, $data, "id_user= {$id_user}");
	    }

	    function updateAdmin($data, $id_user, $user_type){
			return $this->db->update($this->table, $data, "id_user= {$id_user} and user_type = {$user_type}");
	    }

	function showUserListByCode($id_user)
		{
			return $this->db->select(' id_user, user_name, user_email, user_rut, user_type, user_business, user_insert_date, user_last_date, format_id, user_active', $this->table,"id_user= '{$id_user}'");
		}


	function listUser($user_type)
		{
		    return $this->db->select('*', $this->table, "user_type={$user_type}");

		}

	function deleteUser($id_user)
		{
			return $this->db->delete($this->table, "id_user={$id_user}");
		}

	function deleteUserFormat($format_id)
		{
			return $this->db->delete($this->table, "format_id={$format_id}");
		}

	function setLastInit(){
		$new_last_init = $this->getCurrentTime();
		$reponse = $this->db->update($this->table, array('last_init'=>$new_user_type), "id={$this->getId()}");
		if (response) $this->last_init = $new_last_init;
		return $response;
	}

	protected function getCurrentTime(){
		return date('Y-m-d H:i:s');
	}
	
	public static function validEmail($email = '')
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    
    function generatePassword($prefix="",$special_character=false,$maxlength=8){
		$string = "0123456789abcdefghijklmnopqrstuvwxyz";
		if ($special_character) $string .= "-_.";
		$password_string_length = strlen($string) - 1;
		$password = $prefix;
		for($i=0;$i<$maxlength;$i++){
			$password .= substr($string, rand(0, $password_string_length), 1);
		}
		return $password;
	}
	function updateStatusUser($id_user, $user_active)
	{
		$data = array(
		'user_active' => $user_active
		);
		return $this->db->update($this->table, $data, "id_user={$id_user}");
	}
	function updateStatusUserFormat($token, $user_active)
	{
		$data = array(
		'user_active' => $user_active
		);
		return $this->db->update($this->table, $data, "token='{$token}'");
	}

}
?>	