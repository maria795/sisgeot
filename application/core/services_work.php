 <?php

class servicesWork{

	private $table = "services_work";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addServiceWork($work_order_id, $services_id)
	{
		$data = array(
			'work_order_id' => $work_order_id,
			'services_id' => $services_id
			);

		return $this->db->insert($this->table, $data);
	}

	function showServicesListByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}


	function deleteServicesWork($work_order_id)
	{
		return $this->db->delete($this->table, "work_order_id={$work_order_id}") ;
	}
	
	function showServicesWorkListByCode($work_order_id)
	{
		return $this->db->select("*", $this->table, "work_order_id={$work_order_id}");
	}
	
	function listServicesWork($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}



}

?>
