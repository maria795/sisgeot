<?php

class diagnostic{
	private $table = "diagnostic";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addDiagnostic($diagnostic_name)
	{
		$data = array(
			'diagnostic_name' =>  $diagnostic_name);

		return $this->db->insert($this->table, $data);
	}

	function updateDiagnostic($diagnostic_id, $diagnostic_name)
	{
		$data = array(
			'diagnostic_name' =>  $diagnostic_name
			);
		return $this->db->update($this->table, $data, "diagnostic_id={$diagnostic_id}");
	}

	function deleteDiagnostic($diagnostic_id)
	{
		return $this->db->delete($this->table, "diagnostic_id = {$diagnostic_id}");
	}

	function showDiagnosticListByCode($diagnostic_id)
	{
		return $this->db->select('*', $this->table, "diagnostic_id = {$diagnostic_id}");
	}
	
	function listDiagnostic($limit = null)
	{
		return $this->db->select('*', $this->table, $limit);
	}
}
?>