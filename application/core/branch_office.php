<?php

class branchOffice{
	private $db;
	private $table = "branch_office";

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addBranchOffice($format_id, $branch_office_code, $branch_office_nro, $branch_office_cost_center, $branch_office_gerent_name, $branch_office_gerent_phone, $branch_office_gerent_email, $branch_office_boss_name, $branch_office_boss_phone, $branch_office_boss_email)
	{

		$data = array(
		'format_id' => $format_id,
		'branch_office_code' => $branch_office_code, 
		'branch_office_nro' => $branch_office_nro, 
		'branch_office_cost_center' => $branch_office_cost_center, 
		'branch_office_gerent_name' => $branch_office_gerent_name, 
		'branch_office_gerent_phone' => $branch_office_gerent_phone, 
		'branch_office_gerent_email' => $branch_office_gerent_email, 
		'branch_office_boss_name' => $branch_office_boss_name, 
		'branch_office_boss_phone' => $branch_office_boss_phone, 
		'branch_office_boss_email' => $branch_office_boss_email
		);
		return $this->db->insert($this->table, $data);
	}
	
	function updateBranchOffice($branch_office_id, $branch_office_code, $branch_office_nro, $branch_office_cost_center, $branch_office_gerent_name, $branch_office_gerent_phone, $branch_office_gerent_email, $branch_office_boss_name, $branch_office_boss_phone, $branch_office_boss_email)
	{
		
		$data = array(
		'branch_office_code' => $branch_office_code, 
		'branch_office_nro' => $branch_office_nro, 
		'branch_office_cost_center' => $branch_office_cost_center, 
		'branch_office_gerent_name' => $branch_office_gerent_name, 
		'branch_office_gerent_phone' => $branch_office_gerent_phone, 
		'branch_office_gerent_email' => $branch_office_gerent_email, 
		'branch_office_boss_name' => $branch_office_boss_name, 
		'branch_office_boss_phone' => $branch_office_boss_phone, 
		'branch_office_boss_email' => $branch_office_boss_email
		);
		return $this->db->update($this->table, $data, "branch_office_id = {$branch_office_id}");
	}

	function showBranchOfficeListByCode($branch_office_id)
	{
		return $this->db->select('*', $this->table, "branch_office_id = {$branch_office_id}");
	}
	
	function showBranchOfficeListByFormat($format_id, $limit=null)
	{
		 return $this->db->select('*', $this->table, "format_id = {$format_id}", $limit);
	}

	function showBranchOfficeListByName($branch_office_code)
	{
		return $this->db->select('*', $this->table, "branch_office_code = '{$branch_office_code}'");
	}
	
	function listBranchOffice($limit=null)
	{
	    return $this->db->select('*', $this->table, false, $limit);
	}

	function deleteBranchOffice($branch_office_id)
	{
		return $this->db->delete($this->table, "branch_office_id = {$branch_office_id}") ;
	}
}


?>