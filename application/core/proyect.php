<?php

class proyect{

	private $table = "proyect";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addProyect($proyect_code, $proyect_name, $proyect_description, $proyect_date, $proyect_boss)
	{


		$data = array(
			'proyect_code' => $proyect_code,
			'proyect_name' => $proyect_name,
			'proyect_description' => $proyect_description,
			'proyect_date' => $proyect_date,
			'proyect_boss'  => $proyect_boss);

		return $this->db->insert($this->table, $data);
	}
	function updateProyect($proyect_id, $proyect_code, $proyect_name, $proyect_description, $proyect_date, $proyect_boss)
	{

		$data = array( 
			'proyect_code' => $proyect_code,
			'proyect_name' => $proyect_name,
			'proyect_description' => $proyect_description,
			'proyect_date' => $proyect_date,
			'proyect_boss'  => $proyect_boss);
		
		return $this->db->update($this->table, $data, "proyect_id = {$proyect_id}");
	}
	function showProyectListByCode($proyect_id)
	{
		return $this->db->select('*', $this->table, "proyect_id = {$proyect_id}");
	}
	function listProyect($limit=null)
	{
	    return $this->db->select('*', $this->table, false, $limit);
	}
	function deleteProyect($proyect_id)
	{
		return $this->db->delete($this->table, "proyect_id = {$proyect_id}") ;
	}	
}


?>