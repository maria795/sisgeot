 <?php

class technicalWork{

	private $table = "technical_work";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}

	function addTechnicalWork($work_order_id, $technical_id)
	{
		$data = array(
			'work_order_id' => $work_order_id,
			'technical_id' => $technical_id
			);

		return $this->db->insert($this->table, $data);
	}

	function showTechnicalListByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}


	function deleteTechnicalWork($work_order_id)
	{
		return $this->db->delete($this->table, "work_order_id={$work_order_id}") ;
	}
	

	function showTechnicalWorkListByCode($work_order_id)
	{
		return $this->db->select("*", $this->table, "work_order_id={$work_order_id}");
	}
	
	function listTechnicalWork($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}



}

?>
