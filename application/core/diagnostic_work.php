 <?php

class diagnosticWork{

	private $table = "diagnostic_work";
	private $db;

	function __construct(){
		global $db;
		$this->db = &$db;
	}


	function addDiagnosticWork($work_order_id, $diagnostic_id)
	{
		$data = array(
			'diagnostic_id' => $diagnostic_id,
			'work_order_id' => $work_order_id
			);

		return $this->db->insert($this->table, $data);
	}

	function deleteDiagnosticWork($work_order_id)
	{
		return $this->db->delete($this->table, "work_order_id={$work_order_id}") ;
	}
	
	function showDiagnosticListByWorkOrder($work_order_id)
	{
		return $this->db->select('*', $this->table, "work_order_id = {$work_order_id}");
	}


	function showDiagnosticWorkListByCode($work_order_id)
	{
		return $this->db->select("*", $this->table, "work_order_id={$work_order_id}");
	}
	
	function listDiagnosticWork($limit=null)
	{
		return $this->db->select("*", $this->table, false, $limit);
	}
}

?>
