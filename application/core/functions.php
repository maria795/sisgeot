<?php

function sent_email ($subject, $body, $to_andress, $file='', $from_andress='from@tecnosec.com', $reply_to='no-reply@gmail.com', $alt_body=''){

	global $USERNAME_EMAIL, $PASSWORD_EMAIL, $EMAIL_HOST, $DEBUG_EMAIL;

	require 'phpmailer/vendor/autoload.php';

	$mail = new PHPMailer\PHPMailer\PHPMailer;

	$mail->isSMTP();

	$mail->SMTPDebug = DEBUG_EMAIL;

	$mail->Host = 'smtp.gmail.com';

	$mail->Port = 587;

	$mail->SMTPSecure = 'tls';

	$mail->SMTPAuth = true;

	$mail->Username = USERNAME_EMAIL;

	$mail->Password = PASSWORD_EMAIL;

	$mail->setFrom($from_andress);

	$mail->addReplyTo($reply_to);

	$mail->addAddress($to_andress);

	$mail->Subject = $subject;

	$mail->msgHTML($body);

	$mail->AltBody = $alt_body;

	$mail->addAttachment($file);

	if ($mail->send()) return true;

	return false;
}
