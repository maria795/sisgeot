<?php
ini_set("display_errors", 1);


class MySQLiManager{

	private $link;
	private $result;
	public  $table;
	private $lastQuery = '';
 
	public function __construct($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME){
		$this->DB_HOST= $DB_HOST;
		$this->DB_USER= $DB_USER;
		$this->DB_PASS= $DB_PASS;
		$this->DB_NAME=$DB_NAME;

		$this->link = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
	}

	function __destruct(){
		$thread_id = $this->link->thread_id;
		$this->link->kill($thread_id);
		$this->link->close();
	}

	function select($attr, $table, $where = '', $one=false, $limit=null){
		$where = ($where != '' ||  $where != null) ? "WHERE ".$where : '';
		$query = "SELECT ".$attr." FROM ".$table." ".$where.";";
		if($limit) {
			$limit=(int)$limit;
			if ($limit > 0) $query .= " {$limit} ";
		}

		$this->result = $this->query($query);		
        if ($one) return (array) $this->fetch_all()[0];
        return (array) $this->fetch_all();
	}

	function num_rows (){
		return $this->result;
	}
	
function insert($table,$values,$where = '',$sanear = false){
         $columnas = null;
         $valores = null;
            
		foreach ($values as $key => $value) {

			$value = $this->link->escape_string($value);//Evitar SQL injection
			$columnas.=$key.',';
			if( $sanear == true){
			 $valores.='"'.ucwords(strtolower($value)).'",';
			}else{
				$valores.='"'.$value.'",';
			}
		}
		$columnas = substr($columnas, 0, -1);
		$valores = substr($valores, 0, -1);

		$stmt = "INSERT INTO ".$table." (".$columnas.") VALUES(".$valores.") ".$where.";";
        $result = $this->query($stmt);
        
	        if(!$result > 0) 
	        {
				$response = false;
			}
			else 
			{
				$response = true;
			}
			return $response;
	}


	function update($table,$values,$where)
	{
		$valores = '';
		foreach ($values as $key => $value) 
		{
			$value = $this->link->escape_string($value);//Evitar SQL injection
			$valores .= $key. '="'.$value.'",';
		}

		$valores = substr($valores,0,strlen($valores)-1);
		$query = "UPDATE $table SET $valores WHERE $where";

		$this->result = $this->query($query);

		if(!$this->result) {
			$response = false;
		}
		else {
			$response = true;
		}
		return $response;
	}
	
	
	function delete($table, $where, $complex = false)
	{
		$query = 'DELETE FROM '.$table.' WHERE '.$where;
		$result = $this->query($query);
		if(!$this->result > 0) {
			$response = false;
		}
		else {
			$response = true;
		}
		return $response;
	}

	function fetch_all($field=false){
		if (!empty($this->result)){
			if($this->result->num_rows > 0) {
				$data = array();
	            while($row = $this->result->fetch_assoc())
	            {
	            	if (!empty($field)){
			            if(!isset($row[$field])){
			            	throw new Exception("THE FIELD {$field} DON'T EXIST", 1);
			            }
			            $data[] = $row[$field];
			            continue;
	            	} 
		            $data[] = $row;
	            }
	            return (array) $data;
			}
		}
		return false;
	}
	



	function check($what,$table,$values,$complex = false){
		if($complex){ $where = $values; }else{
			foreach ($values as $key => $value) {
				$where = $key.'="'.$value.'"';
			}
		}
		$query = "SELECT ".$what." FROM ".$table." WHERE ".$where;
		$result = $this->query($query);
		if($result->num_rows > 0) {
			$response = true;
		}
		else {
			$response = false;
		}
		return $response;
	}

	function query ($query){
		$this->lastQuery = $query;
		return $this->link->query($query);
	}

	function getLastQuery(){
		return $this->lastQuery;
	}
}
?>