-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2019 a las 05:33:04
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisgeot`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `branch_office`
--

CREATE TABLE IF NOT EXISTS `branch_office` (
  `branch_office_id` int(5) NOT NULL,
  `format_id` int(5) NOT NULL,
  `branch_office_code` varchar(7) NOT NULL,
  `branch_office_nro` varchar(4) NOT NULL,
  `branch_office_cost_center` varchar(60) NOT NULL,
  `branch_office_gerent_name` varchar(60) NOT NULL,
  `branch_office_gerent_phone` varchar(15) NOT NULL,
  `branch_office_gerent_email` varchar(60) NOT NULL,
  `branch_office_boss_name` varchar(60) NOT NULL,
  `branch_office_boss_phone` varchar(15) NOT NULL,
  `branch_office_boss_email` varchar(60) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `business`
--

CREATE TABLE IF NOT EXISTS `business` (
  `business_id` int(4) NOT NULL,
  `business_name` varchar(100) NOT NULL,
  `business_rut` varchar(50) NOT NULL,
  `business_direction` varchar(100) NOT NULL,
  `business_phone` varchar(50) NOT NULL,
  `business_website` varchar(50) NOT NULL,
  `business_description` text NOT NULL,
  `business_logo` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `business`
--

INSERT INTO `business` (`business_id`, `business_name`, `business_rut`, `business_direction`, `business_phone`, `business_website`, `business_description`, `business_logo`) VALUES
(0, 'TECNOSEC', '76.020.371-8', 'TEMUCO,CHILE', '+569 924 90207', 'WWW.TECNOSEC.CL', 'COMERCIALIZADORA DE TECNOLOGIAS LTDA.', 'tecnosec.png'),
(1, 'INFRAGANTI', '', 'GENERAL DEL CANTO 105, OF. 1204, PROVIDENCIA', '+56 222611479', 'WWW.INFRAGANTI.CL', 'COMERCIALIZADORA DE TECNOLOGIAS LTDA.', 'infraganti.png'),
(2, 'GLOBAL BUSINESS', '0000', 'AV. EL ROSAL N 6331, MAIPU', '222 611 479', '222 611 479', 'COMERCIALIZADORA DE TECNOLOGIAS LTDA.', 'globalbusiness.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostic`
--

CREATE TABLE IF NOT EXISTS `diagnostic` (
  `diagnostic_id` int(4) NOT NULL,
  `diagnostic_name` varchar(120) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `diagnostic`
--

INSERT INTO `diagnostic` (`diagnostic_id`, `diagnostic_name`) VALUES
(4, 'ACCESORIOS INCOMPLETOS'),
(8, 'CAMARA SIN VISUALIZAR'),
(6, 'LENTE SUELTO'),
(2, 'MANIPULACIÃ³N DE TERCEROS'),
(5, 'MUESTRA GOLPE'),
(3, 'MUESTRA HUMEDAD'),
(1, 'MUESTRA SUCIEDAD'),
(9, 'SISTEMA SIN VISUALIZAR'),
(7, 'SUPERFICIE RAYADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostic_work`
--

CREATE TABLE IF NOT EXISTS `diagnostic_work` (
  `diagnostic_id` int(4) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `diagnostic_work`
--

INSERT INTO `diagnostic_work` (`diagnostic_id`, `work_order_id`) VALUES
(1, 00001),
(2, 00002),
(2, 00003);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `document_destination` varchar(150) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `document_id` int(8) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `document`
--

INSERT INTO `document` (`document_destination`, `work_order_id`, `document_id`) VALUES
('nota de venta.pdf', 00001, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formats`
--

CREATE TABLE IF NOT EXISTS `formats` (
  `format_id` int(5) NOT NULL,
  `format_name` varchar(60) NOT NULL,
  `format_business_name` varchar(60) NOT NULL,
  `format_rut` varchar(15) NOT NULL,
  `format_direction` varchar(50) NOT NULL,
  `format_comuna` varchar(40) NOT NULL,
  `format_region` varchar(40) NOT NULL,
  `format_phone` varchar(15) NOT NULL,
  `format_gerent` varchar(60) NOT NULL,
  `format_gerent_email` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `formats`
--

INSERT INTO `formats` (`format_id`, `format_name`, `format_business_name`, `format_rut`, `format_direction`, `format_comuna`, `format_region`, `format_phone`, `format_gerent`, `format_gerent_email`) VALUES
(0, 'ADMINISTRADOR', '0', 'admin', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `photo_id` int(8) NOT NULL,
  `photo_destination` varchar(150) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(4) NOT NULL,
  `product_business` enum('TECNOSEC','INFRAGANTI','GLOBAL BUSINESS') NOT NULL COMMENT '0 = ''TECNOSEC'', 1=''INFRAGANTI'', 2=''GLOBAL BUSINESS''',
  `product_description` text NOT NULL,
  `product_cost` float NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`product_id`, `product_business`, `product_description`, `product_cost`) VALUES
(1, 'TECNOSEC', 'DISCO DURO, 320GB.', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_work`
--

CREATE TABLE IF NOT EXISTS `product_work` (
  `product_id` int(4) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product_work`
--

INSERT INTO `product_work` (`product_id`, `work_order_id`) VALUES
(1, 00001),
(1, 00002),
(1, 00003);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyect`
--

CREATE TABLE IF NOT EXISTS `proyect` (
  `proyect_id` int(4) NOT NULL,
  `proyect_code` varchar(7) DEFAULT NULL,
  `proyect_name` varchar(60) DEFAULT NULL,
  `proyect_description` text,
  `proyect_date` date DEFAULT NULL,
  `proyect_boss` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proyect`
--

INSERT INTO `proyect` (`proyect_id`, `proyect_code`, `proyect_name`, `proyect_description`, `proyect_date`, `proyect_boss`) VALUES
(1, 'PB19', 'PROYECTO BRECHA', 'PROYECTO BRECHA', '2918-12-19', 'JEAN CARLOS PEREZ'),
(2, 'PT10', 'PROYECTO TOTTEM', 'TIENE COMO OBJETIVO ORGANIZAR...', '2019-10-10', 'PEPE PEREZ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receipt`
--

CREATE TABLE IF NOT EXISTS `receipt` (
  `receipt_id` int(8) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `receipt_detail_payment` enum('1','2','3','4','5') NOT NULL COMMENT '1=efectivo, 2=debito, 3=credito, 4=transferencia, 5=cheque',
  `receipt_amount` float NOT NULL,
  `receipt_issued` varchar(60) NOT NULL,
  `receipt_date` date NOT NULL,
  `receipt_observations` text NOT NULL,
  `business_id` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `request_id` int(8) NOT NULL,
  `branch_office_id` int(5) NOT NULL,
  `request_date` date NOT NULL,
  `request_department` varchar(50) NOT NULL,
  `request_medium` enum('LLAMADA TELEFONICA','EMAIL','SMS','WHATSAPP','PERSONAL') NOT NULL,
  `request_r_name` varchar(50) NOT NULL,
  `request_r_phone` varchar(15) NOT NULL,
  `request_r_email` varchar(60) NOT NULL,
  `request_status` set('0','1') NOT NULL,
  `request_app` text NOT NULL,
  `business_id` int(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `request`
--

INSERT INTO `request` (`request_id`, `branch_office_id`, `request_date`, `request_department`, `request_medium`, `request_r_name`, `request_r_phone`, `request_r_email`, `request_status`, `request_app`, `business_id`) VALUES
(3, 6, '2018-01-13', 'MANTENCION', 'EMAIL', 'JUDTH ARANEDA', '4434562', 'ANTHONYMEDINACH@GMAIL.COM', '1', 'SOLICITA VISITAR Y SOLUCIONAR REQUERIMIENTO CCTV LOCAL IRRARRAZAVAL', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reset_password`
--

CREATE TABLE IF NOT EXISTS `reset_password` (
  `reset_id` int(4) NOT NULL,
  `id_user` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `services_id` int(4) NOT NULL,
  `services_name` varchar(150) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`services_id`, `services_name`) VALUES
(1, 'CAMBIO DE PASSWORD'),
(2, 'CAMBIOS DE EQUIPO GARANTIA/ NUEVO'),
(3, 'CANALIZADO DE CABLEADO'),
(9, 'CAPACITACIÃ³N PERSONAL'),
(7, 'CONFIGURACIÃ³N CCTV'),
(8, 'ENFOQUES DE CÃ¡MARAS'),
(10, 'INSTALACIÃ³N DE COMPUTADOR'),
(11, 'INSTALACIÃ³N DE DISCO DURO'),
(4, 'MANTENCIÃ³N DE HARDWARE'),
(5, 'MANTENCIÃ³N DE SOFWARE'),
(6, 'REUBICACIÃ³N DE CÃ¡MARAS'),
(12, 'TRASLADO DE CAMARA EN SALA ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_work`
--

CREATE TABLE IF NOT EXISTS `services_work` (
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `services_id` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `services_work`
--

INSERT INTO `services_work` (`work_order_id`, `services_id`) VALUES
(00001, 8),
(00001, 7),
(00002, 3),
(00003, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `technical`
--

CREATE TABLE IF NOT EXISTS `technical` (
  `technical_id` int(4) NOT NULL,
  `technical_name` varchar(50) DEFAULT NULL,
  `technical_rut` varchar(12) DEFAULT NULL,
  `technical_position` varchar(60) DEFAULT NULL,
  `technical_phone` varchar(15) DEFAULT NULL,
  `technical_email` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `technical`
--

INSERT INTO `technical` (`technical_id`, `technical_name`, `technical_rut`, `technical_position`, `technical_phone`, `technical_email`) VALUES
(1, 'MIGUEL ANGEL', '1232323', 'GERENTE', '04121212121', 'GERENTE@GMAIL.COM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `technical_work`
--

CREATE TABLE IF NOT EXISTS `technical_work` (
  `technical_id` int(4) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `technical_work`
--

INSERT INTO `technical_work` (`technical_id`, `work_order_id`) VALUES
(1, 00001),
(1, 00002),
(1, 00003);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(4) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(60) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_rut` varchar(20) NOT NULL,
  `user_type` enum('0','1','2') NOT NULL COMMENT '0=admin, 1=personal, 2=cliente',
  `user_business` enum('0','1','2') NOT NULL COMMENT '0=tecnosec, 1=infranganti, 2=global',
  `user_insert_date` datetime NOT NULL,
  `user_last_date` datetime NOT NULL,
  `format_id` int(5) NOT NULL,
  `user_active` enum('0','1') NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `user_email`, `user_password`, `user_rut`, `user_type`, `user_business`, `user_insert_date`, `user_last_date`, `format_id`, `user_active`, `token`) VALUES
(0, 'ADMINISTRADOR', 'sif.cl17@gmail.com', 'f41d5ca7d72a6f2b7e73eda26eb9ad8e3bd7b177', 'admin', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1', ''),
(6, 'MARIA', 'MARIAJSILVAT@GMAIL.COM', '98165228fcb0a65ce54517854f87e741a7b989a4', '23677614', '1', '1', '2019-11-26 23:32:57', '0000-00-00 00:00:00', 0, '0', '43212b33beb3f7ba7423042fd6adfcca410a46dc');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `verification_request`
--

CREATE TABLE IF NOT EXISTS `verification_request` (
  `verification_id` int(11) NOT NULL,
  `v_request_id` int(11) NOT NULL,
  `v_request_status` enum('0','1') DEFAULT NULL,
  `v_request_date` date NOT NULL,
  `v_request_responsable` varchar(40) NOT NULL,
  `v_request_responsable_rut` int(11) NOT NULL,
  `v_token` varchar(150) NOT NULL,
  `v_format_id` int(8) NOT NULL,
  `business_id` int(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `verification_request`
--

INSERT INTO `verification_request` (`verification_id`, `v_request_id`, `v_request_status`, `v_request_date`, `v_request_responsable`, `v_request_responsable_rut`, `v_token`, `v_format_id`, `business_id`) VALUES
(2, 3, '1', '2018-01-13', 'RENDIC HERMANOS S.A.', 9989116, '05148b980e6adc9aaf3d9402fa1bd86814d5bf32', 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `video_id` int(8) NOT NULL,
  `video_destination` varchar(150) NOT NULL,
  `work_order_id` int(5) unsigned zerofill NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `work_order`
--

CREATE TABLE IF NOT EXISTS `work_order` (
  `work_order_id` int(5) unsigned zerofill NOT NULL,
  `request_id` int(8) NOT NULL,
  `work_order_generation` date NOT NULL,
  `proyect_id` int(4) NOT NULL,
  `work_order_date` date NOT NULL,
  `time_input` varchar(15) NOT NULL,
  `time_output` varchar(15) NOT NULL,
  `work_order_observation` text NOT NULL,
  `work_order_status` enum('EN PROCESO','FINALIZADO') NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `work_order`
--

INSERT INTO `work_order` (`work_order_id`, `request_id`, `work_order_generation`, `proyect_id`, `work_order_date`, `time_input`, `time_output`, `work_order_observation`, `work_order_status`) VALUES
(00001, 3, '2018-01-13', 2, '2018-01-15', '15:30', '16:30', '', 'FINALIZADO'),
(00002, 3, '2018-01-13', 0, '1969-12-31', '', '', '', 'EN PROCESO'),
(00003, 3, '2018-01-13', 2, '2018-02-01', '', '', '', 'EN PROCESO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `branch_office`
--
ALTER TABLE `branch_office`
  ADD PRIMARY KEY (`branch_office_id`),
  ADD UNIQUE KEY `branch_office_code` (`branch_office_code`),
  ADD KEY `format_id` (`format_id`);

--
-- Indices de la tabla `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`business_id`);

--
-- Indices de la tabla `diagnostic`
--
ALTER TABLE `diagnostic`
  ADD PRIMARY KEY (`diagnostic_id`),
  ADD UNIQUE KEY `diagnosis_name` (`diagnostic_name`);

--
-- Indices de la tabla `diagnostic_work`
--
ALTER TABLE `diagnostic_work`
  ADD KEY `work_order_id` (`work_order_id`),
  ADD KEY `diagnostic_id` (`diagnostic_id`);

--
-- Indices de la tabla `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`document_id`),
  ADD KEY `document_ibfk_1` (`work_order_id`);

--
-- Indices de la tabla `formats`
--
ALTER TABLE `formats`
  ADD PRIMARY KEY (`format_id`),
  ADD UNIQUE KEY `formats_rut` (`format_rut`),
  ADD UNIQUE KEY `format_gerent_email` (`format_gerent_email`);

--
-- Indices de la tabla `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`photo_id`),
  ADD KEY `work_order_id` (`work_order_id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indices de la tabla `product_work`
--
ALTER TABLE `product_work`
  ADD KEY `work_order_id` (`work_order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indices de la tabla `proyect`
--
ALTER TABLE `proyect`
  ADD PRIMARY KEY (`proyect_id`),
  ADD UNIQUE KEY `proyect_code` (`proyect_code`);

--
-- Indices de la tabla `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`receipt_id`),
  ADD KEY `work_order_id` (`work_order_id`);

--
-- Indices de la tabla `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `branch_office_id` (`branch_office_id`),
  ADD KEY `business_id` (`business_id`);

--
-- Indices de la tabla `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`reset_id`),
  ADD UNIQUE KEY `id_user_2` (`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`services_id`),
  ADD UNIQUE KEY `services_name` (`services_name`);

--
-- Indices de la tabla `services_work`
--
ALTER TABLE `services_work`
  ADD KEY `work_order_id` (`work_order_id`),
  ADD KEY `services_id` (`services_id`);

--
-- Indices de la tabla `technical`
--
ALTER TABLE `technical`
  ADD PRIMARY KEY (`technical_id`),
  ADD UNIQUE KEY `technical_rut` (`technical_rut`,`technical_email`);

--
-- Indices de la tabla `technical_work`
--
ALTER TABLE `technical_work`
  ADD KEY `work_order_id` (`work_order_id`),
  ADD KEY `technical_id` (`technical_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `user_email` (`user_email`,`user_rut`),
  ADD KEY `format_id` (`format_id`);

--
-- Indices de la tabla `verification_request`
--
ALTER TABLE `verification_request`
  ADD PRIMARY KEY (`verification_id`),
  ADD UNIQUE KEY `v_request_id` (`v_request_id`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`),
  ADD KEY `work_order_id` (`work_order_id`);

--
-- Indices de la tabla `work_order`
--
ALTER TABLE `work_order`
  ADD PRIMARY KEY (`work_order_id`),
  ADD KEY `request_id` (`request_id`),
  ADD KEY `proyect_id` (`proyect_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `branch_office`
--
ALTER TABLE `branch_office`
  MODIFY `branch_office_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `business`
--
ALTER TABLE `business`
  MODIFY `business_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `diagnostic`
--
ALTER TABLE `diagnostic`
  MODIFY `diagnostic_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `document`
--
ALTER TABLE `document`
  MODIFY `document_id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `formats`
--
ALTER TABLE `formats`
  MODIFY `format_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `photo`
--
ALTER TABLE `photo`
  MODIFY `photo_id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `proyect`
--
ALTER TABLE `proyect`
  MODIFY `proyect_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `receipt`
--
ALTER TABLE `receipt`
  MODIFY `receipt_id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `request`
--
ALTER TABLE `request`
  MODIFY `request_id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `reset_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `services_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `technical`
--
ALTER TABLE `technical`
  MODIFY `technical_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `verification_request`
--
ALTER TABLE `verification_request`
  MODIFY `verification_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `work_order`
--
ALTER TABLE `work_order`
  MODIFY `work_order_id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
