#### _SISGEOT_ | [SISTEMA DE GESTIÓN DE ÓRDENES DE TRABAJO]

![version](https://img.shields.io/badge/version-1.0-blue.svg?longCache=true&style=flat-square)
![license](https://img.shields.io/badge/license-MIT-green.svg?longCache=true&style=flat-square)


#### _Versión 1.0_ | [Software sin periodo de pruebas](https://gitlab.com/maria795/sisgeot/)

#### _SISGEOT_ 

El sistema de orden de trabajo, es un sistema a medida. Es creado con la finalidad de formalizar los requerimiento que han solicitado los
clientes a  la empresa por distintos medios (mensaje de texto, whatsapp, llamada telefónica otros), el objetivo es verificar el 
requerimiento vía correo electrónico, para que posteriormente no se desconozca el trabajo y proceso de cobro por el servicio prestado
y equipos instalados cuando corresponde. Posteriormente a la solicitud o adjudicación del servicio o implementación emitimos orden de trabajo.

Para mas información visite  __[https://sisgeot.herokuapp.com/](https://sisgeot.herokuapp.com/s)__ 
ó acceda a __[demo](https://sisgeot.herokuapp.com/)__.  

```
Usuario: admin
Contraseña: admin
```

### Instalación

1. Descargue los archivos
2. Extraiga los paquetes y coloquelo en su servidor web
3. Verifique cada modulos, funcione correctamente. **SISGEOT**.


### Requerimentos
* PHP >= 5.6.0
* MySQL

## Autores
[<sub>Maria Silva</sub><br>](https://gitlab.com/maria795)
[<sub>Anthony Medina</sub>](https://gitlab.com/maria795/sisgeot/)









