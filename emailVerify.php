<?php
	require"application/core/conexion.php";		

	if(!empty($token = $_GET['token']))
	{	
		$user = new user();
		$user_active = 1;
		$user->updateStatusUserFormat($token, $user_active);
		//echo $db->getLastQuery();
	}


	if (!empty($user)){
		if($user->isAccess()){
	
			header('location: application.php');
		}
	}
?>

<!doctype html>
<html lang="es" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="apple-touch-icon" sizes="57x57" href="public/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="public/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="public/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="public/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="public/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="public/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="public/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="public/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="public/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="public/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="public/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="public/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="public/icons/favicon-16x16.png">
		<link rel="manifest" href="public/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="public/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
		<link href="public/css/reset.css" rel="stylesheet" type="text/css"> 
		<link href="public/css/login-phone.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 0px) and (max-width: 767px)" >
		<link href="public/css/login-pc.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 0px) and (min-width: 768px)" >
		<link href="public/css/font-awesome.min.css" rel="stylesheet">
		<title>SISGEOT - Iniciar sesión</title>
		<body class="flexed">
	<div class="loader"></div>
	<div id="loading-div"></div>
	<header class="cd-main-header">
	</header> 
	<main class="cd-main-content flexed2">
		<img src="public/img/logo.png" id="logo">
		<div class="verifiedContent">

		<img src="public/img/done.png" id="verifyIcon" style="width: 30%; height: 100%;" ><p class="ver" style=" margin-left: 1em;">Su email ha sido verificado satisfactoriamente</p>
		</div><br><br>
		<a class="redirect" href="index.php"  target="_blank">Iniciar sesión</a>
	</main> 
	<script src="application/library/jquery.min.js"></script>
	<script src="application/library/modernizr.js"></script> 
	<script src="application/script/login.js"></script> 
</body>
	</head>

</html>